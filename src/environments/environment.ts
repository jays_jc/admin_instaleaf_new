// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config: {
    APP_NAME: 'instaLeaf-admin',
    HOST: 'http://35.183.188.110:1337/',
    CLIENT_ID: '5x7EuN09HAeBn2pYJnvvq7szgJaULh14',
    GRANT_TYPE: 'password',
    DATE_FORMAT: 'dd/MM/yyyy',
    CRON_DATE_FORMAT: 'dd MM yyyy HH:MM a', //jc's account linked key
    DATE_FORMAT_FOR_DATEPICKER: 'dd/mm/yyyy',
    ALERT_TIME: 3000,
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
