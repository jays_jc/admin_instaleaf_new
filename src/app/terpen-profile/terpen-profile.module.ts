import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { TerpenProfileRoutingModule } from './terpen-profile-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CategoryService } from './services/category.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
  	 TerpenProfileRoutingModule,
  	 CommonModule,
     NgxDatatableModule,
     NgxSpinnerModule,
     ImageUploadModule

  ],
  providers: [
    CategoryService
  ],
  declarations: [
  	ListCategoryComponent,
  	AddUpdateCategoryComponent
  ]
})
export class TerpenProfileModule { }