import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Terpen Profile'
    },
    children: [
     {
        path: '',
        component: ListCategoryComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'list',
        component: ListCategoryComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'add',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Add Terpen Profile'
        }
      },
      {
        path: 'edit/:id',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Edit Terpen Profile'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
    RouterModule,
    FormsModule,
  ]
})
export class TerpenProfileRoutingModule {}
