import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ImageResult } from 'ng2-imageupload';
import { CategoryService } from '../services/category.service';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate-category.component.scss'],
})
export class AddUpdateCategoryComponent {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;

  private _host = environment.config.HOST;
  public object = {
    name: '',
    description: '',
    image: '',
  };

  public isLoading = false;
  public isPageLoading = true;
  cities = [];
  categoryID: any;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.categoryID = _activateRouter.snapshot.params['id'];
    this.spinner.show();
    if (this.categoryID) {
      this._catgService.get(this.categoryID).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isPageLoading = false;
          if (res.success) {
            this.object = res.data.key;
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isPageLoading = false;
          // this._commanService.checkAccessToken(err);
        }
      );
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }
  }

  ngOnInit(): void {
    // this.showDangerAlert();
  }

  /*If categoryID exist then will update existing category otherwise will add new category*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.object));
    if (this.categoryID) {
      this._catgService.update(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading         = false;
          if (res.success) {
            this.toastr.success(res.data.message);
            this._cookieService.put('categoryAlert', res.data.message);
            this._router.navigate(['/terpen']);
          } else {
            // this.toastr.error(res.error.message);
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
        },
        (err) => {
          this.spinner.hide();
          this.toastr.error(err);
          // this.isLoading = false;
        }
      );
    } else {
      this._catgService.add(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading         = false;
          if (res.success) {
            this.toastr.success(res.data.message);
            this._cookieService.put('categoryAlert', res.data.message);
            this._router.navigate(['/terpen']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
        },
        (err) => {
          this.spinner.hide();
          this.toastr.error(err);
          // this.isLoading = false;
        }
      );
    }
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'product',
    };

    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(
      (res) => {

        // this.isLoading = false;
        if (res.success) {
          this.object.image = res.data.fullPath;
        } else {
          window.scrollTo(0, 0);
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
        this.myInputVariable.nativeElement.value = '';
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        this.toastr.error(err);
        // this.isLoading = false;
      }
    );
  }
}
