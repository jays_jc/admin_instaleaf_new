import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../services/profile.service';
import { Router } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';
// import { CommanService } from '../../shared/services/comman.service';

@Component({
    selector: 'app-changepassword',
    templateUrl: 'changepassword.component.html'
})
export class ChangePasswordComponent implements OnInit {

    public password = '';
    public newPassword = '';
    public confirmPassword = '';
    public errMessage = '';
    public isPageLoading = false;
    // public currentUser = { 
    //     firstName:'',
    //     lastName:'',
    //     email:''
    // };

    public user = {
        id: '',
        currentPassword: '',
        newPassword: '',
        confirmPassword: ''
    };

    constructor(private _router: Router,
        private _profileService: ProfileService,
        private _commanService: CommanService,
        private toastr: ToastrService,
        private spinner: NgxSpinnerService,
        private location: Location
    ) { }

    ngOnInit() {
        //this.isPageLoading = true;
        this.getCurrentUser();
    }

    submit() {
        this.spinner.show();
        // this.isPageLoading     = true;
        // let id = this.getCurrentUser()

        this.errMessage = '';

        this._profileService.changePassword(this.user).subscribe(res => {
            this.spinner.hide();
            // this.isPageLoading = false;
            if (res.success) {
                this.toastr.success(res.message);
                this.user.confirmPassword = '';
                this.user.newPassword = '';
                this.user.currentPassword = ''
                // this._commanService.showAlert(res.message,'alert-success');
                this.location.back();

            } else {
                this.errMessage = res.error.message;

            }
        }, err => {
            this.spinner.hide();
            // this.isPageLoading = false;

        });
    }

    getCurrentUser() {
        this.errMessage = '';
        this.spinner.show();
        // this.isPageLoading = false;
        this._profileService.getcurrentuser().subscribe(res => {
            this.spinner.hide();
            // this.currentUser  = res.id;
            this.user['id'] = res.id;
        },
            error => {
                this.spinner.hide();
            }
        );
    }
}
