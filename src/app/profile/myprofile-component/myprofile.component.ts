import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { ProfileService } from '../services/profile.service';
import { Router } from '@angular/router';
import { ProfileService } from '../services/profile.service';
import { CommanService } from 'src/app/shared/comman.service';
import { CookieService } from 'ngx-cookie';
import { NgForm } from '@angular/forms';
// import { CookieService } from 'ngx-cookie';
// import { CommanService } from '../../shared/services/comman.service';
// import { ModalDirective } from 'ng2-bootstrap/modal/modal.component';

@Component({
  selector: 'app-myprofile',
  templateUrl: 'myprofile.component.html',
  styleUrls: ['./myprofile.component.scss'],
})
export class MyprofileComponent implements OnInit {

  passwordType: boolean;
  passwordType1: boolean;
  passwordType2: boolean;
  public errMessage = '';
  public successMessage = '';
  public isPageLoading = false;
  public isLoading: boolean = false;
  public currentUser: any = {};
  public user = { id: '', currentPassword: '', newPassword: '', confirmPassword: '' };
  @ViewChild("closeModal", { static: true }) closeModal: ElementRef;
  @ViewChild('changepasswordForm', {static:false}) changepasswordForm: NgForm;
  // @ViewChild('cpasswordModal',{static:true}) public cpasswordModal: ModalDirective;

  constructor(
    private _router: Router,
    private _profileService: ProfileService,
    private _commanService: CommanService,
    private _cookieService: CookieService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    //this.isPageLoading = true;
    this.getCurrentUser();
  }

  togglePassword() {
    this.passwordType = !this.passwordType;
  }
  togglePasswords() {
    this.passwordType1 = !this.passwordType1;
  }
  togglePassword1() {
    this.passwordType2 = !this.passwordType2;
  }
  submit() {
    this.spinner.show();
    // this.isPageLoading     = true;
    // this.isLoading     = true;
    this.errMessage = '';

    this._profileService.changePassword(this.user).subscribe(res => {
     
      // this.isPageLoading = false;
      if (res.success) {
        this.toastr.success(res.message);
        this.closeModal.nativeElement.click();
        this.user.confirmPassword = '';
        this.user.currentPassword = '';
        this.user.newPassword = '';
        // this.isLoading     = false;
        // this._commanService.showAlert(res.message,'alert-success');
        // this.cpasswordModal.hide();             
        // this.isPageLoading = false;
        //this._commanService.back();

      } else {
        this.errMessage = res.error.message;
        // this.isLoading     = false;

      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      // this.isPageLoading = false;

    });
  }

  getCurrentUser() {
    this.spinner.show();
    this.errMessage = '';
    // this.isPageLoading = false;
    this._profileService.getcurrentuser().subscribe(res => {
      this.spinner.hide();
      this.currentUser = res
      this.user['id'] = this.currentUser.id;
    },
      error => {
        this.spinner.hide();
      }
    );
  }

  clear(){
    this.changepasswordForm.resetForm();
  }

}