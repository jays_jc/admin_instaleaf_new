import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CommanService } from 'src/app/shared/comman.service';

@Injectable()

export class ProfileService {
    private _host = environment.config.HOST;

    constructor(
        private _http: HttpClient,
        private _commanService: CommanService
    ) { }

    getcurrentuser() {
        return this._http.get(this._host + 'users/current').pipe(
            map((response: any) => {
                return response;
            }),
            catchError(this.handleError)
        );
        // let headers = this._commanService.getAuthorizationHeader();
        // return this._http.get(this._host+'/users/current', { headers: headers }).map((res:Response) => res.json())
    }

    changePassword(userData) {
        return this._http.put(this._host + 'changepassword', userData).pipe(
            map((response: any) => {
                return response;
            }),
            catchError(this.handleError)
        );
        // let headers = this._commanService.getAuthorizationHeader();
        // return this._http.put(this._host +'/changepassword', userData, { headers: headers }).map((res:Response) => res.json())
    }



    handleError(error: HttpErrorResponse) {
        console.log(error);
        if (error.error.code == 401) {
            return throwError('');
        } else if (error.error.code == 404) {
            return throwError(error.error.message);
        }
        return throwError('Something bad happened; please try again later.');
    }
}