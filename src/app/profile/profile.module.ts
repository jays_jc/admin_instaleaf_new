import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from './changepassword-component/changepassword.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { MyprofileComponent } from './myprofile-component/myprofile.component';
import { ProfileService } from './services/profile.service';
import { EqualValidator } from '../profile/equal-validator.directive';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  imports: [
     ProfileRoutingModule,
     CommonModule,
     FormsModule,
     NgxSpinnerModule    
  ],
  providers: [
    ProfileService,
  ],
  declarations: [
    ChangePasswordComponent,
    MyprofileComponent,
    EqualValidator    
  ]
})
export class ProfileModule { }