import { CookieService } from 'ngx-cookie';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { CommanService } from './../../shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewsService } from '../services/news.service';
import { CategoryService } from 'src/app/category/services/category.service';
@Component({
  selector: 'app-list-component',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string; date: string; } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
    date: new Date().getTime().toString(),
  };
  categoryList: any = [];
  public response: any;
  public addEditDelete: boolean = false;
  public categories = [];
  public categoryType = "news";
  totalItems: any;


  public constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _newsService: NewsService,
    private _commanService: CommanService,
    private toastr: ToastrService,
    private _categoryService: CategoryService,
    private _cookieService: CookieService
  ) {
    let actions = this._commanService.getActions();
    if (actions["type"] == 'SA' || actions['news']['addEditDelete']) this.addEditDelete = true;


  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();
    this.getCategories(this.categoryType)
  }

  /* Function use to remove News with news id*/
  removeNews(newsID) {
    if (confirm("Do you want to delete?")) {
      this.spinner.show();
      // this.isLoading = true;
      this._newsService.delete(newsID).subscribe(res => {
        this.response = res;
        this.spinner.hide();
        // this.isLoading = false;  
        // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
        // this.itemsTotal = this.itemsTotal - 1;

        // if( ! (this.itemsTotal >= start) ){
        //    this.activePage = this.activePage -1
        //    if(this.activePage == 0) this.activePage = 1;
        // }
        this.toastr.success('News Deleted  successfully.');
        /* reload page. */
        this.getCategory();
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }


  /*Get all Users */
  getCategory() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._newsService
      .getAllNews(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].newsData
          this.removeHTML();
          this.categoryList = response['data'].newsData.map((data) => {
            return {
              id: data.id,
              image: data.image,
              title: data.title,
              subtitle: data.subtitle,
              createdAt: data.createdAt,
              totalOrders: data.totalOrders,
              categoryId: data.categoryId
            };
          });
          // this.removeHTML();
          // this.isLoading = false;
          // this.spinner.hide();
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  getCategories(categoryType) {
    this._categoryService.getCategoryByType(categoryType).subscribe(res => {
      if (res.success) {
        this.categories = res.data.newscat;
        
      }
    }, err => {
      this.isLoading = false;
    });
  }

  changeCategory(obj) {
    console.log("object is", obj)
    if (obj.categoryId) {
      this.spinner.show();
      // this.isLoading = true;
      this._categoryService.updateNewsCategory(obj).subscribe(res => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.toastr.success('Updated Successfully.');
          // this._commanService.showAlert("Updated Successfully.",'alert-success');
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error('There is Some Problem please try after some time.');
        // this._commanService.showAlert('There is Some Problem please try after some time.','alert-danger');
        // this.isLoading = false;
      })
    }

  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/news/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  viewNews(newsID) {
    let route = '/news/list/' + newsID;
    this._router.navigate([route]);
  }

  editNews(ID) {
    let route = '/news/edit/' + ID;
    this._router.navigate([route]);
  }
  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  removeHTML() {
    if (this.categoryList) {
      for (var j = 0; j < this.categoryList.length; ++j) {
        //if(this.categoryList[j].subtitle && this.categoryList[j].subtitle.length > 60) this.categoryList[j].subtitle =  this.categoryList[j].subtitle.substring(0,60) + ' ...';
        if (this.categoryList[j].subtitle) this.categoryList[j].subtitle = this.categoryList[j].subtitle.replace(/<\/?[^>]+(>|$)/g, "");
        if (this.categoryList[j].subtitle) this.categoryList[j].subtitle = this.categoryList[j].subtitle.replace(/&.*;/g, '');
        if (this.categoryList[j].subtitle && this.categoryList[j].subtitle.length > 500) this.categoryList[j].subtitle = this.categoryList[j].subtitle.substring(0, 500) + ' ...';

        if (this.categoryList[j].title) this.categoryList[j].title = this.categoryList[j].title.replace(/<\/?[^>]+(>|$)/g, "");
        if (this.categoryList[j].title) this.categoryList[j].title = this.categoryList[j].title.replace(/&.*;/g, '');
        if (this.categoryList[j].title && this.categoryList[j].title.length > 500) this.categoryList[j].title = this.categoryList[j].title.substring(0, 500) + ' ...';

        if (this.categoryList[j].description) this.categoryList[j].description = this.categoryList[j].description.replace(/<\/?[^>]+(>|$)/g, "");
        if (this.categoryList[j].description) this.categoryList[j].description = this.categoryList[j].description.replace(/&.*;/g, '');
        if (this.categoryList[j].description && this.categoryList[j].description.length > 500) this.categoryList[j].description = this.categoryList[j].description.substring(0, 500) + ' ...';
      }
    }

  }

}
