import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsService } from '../services/news.service';
import { CommanService } from 'src/app/shared/comman.service';


@Component({
  selector: 'app-view-component',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})

export class ViewComponent implements OnInit {


  private _host = environment.config.HOST;

  public newsID = '';
  public news = {};
  public isLoading = true;
  public addEditDelete: boolean = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _newsService: NewsService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {

    let actions = this._commanService.getActions();
    if (actions["type"] == 'SA' || actions['news']['addEditDelete']) this.addEditDelete = true;
    this.spinner.show();
    this.newsID = _route.snapshot.params['id'];
    this._newsService.get(this.newsID).subscribe(res => {
      this.spinner.hide();
      // this.isLoading = false;
      if (res.success) {
        this.news = res.data.news[0];
      } else {
        // this._commanService.checkAccessToken(res.error);
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });

  }

  ngOnInit(): void { }

  editNews(newsid) {
    let route = '/news/edit/' + newsid;
    this._router.navigate([route]);
  }







}
