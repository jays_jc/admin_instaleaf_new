import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { truncateSync } from 'fs';
// import { NewsService } from 'src/app/subscriptions/services/news.service';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';
import { ImageResult } from 'ng2-imageupload';
import { NewsService } from '../services/news.service';
import { CKEditorModule } from 'ng2-ckeditor';

@Component({
  templateUrl: './addupdate.component.html',
  styleUrls: ['./addupdate.component.scss']
})
export class AddupdateComponent implements OnInit {

  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  private _host = environment.config.HOST;

  public news = {
    title: '',
    subtitle: '',
    description: '',
    image: '',
    meta_name: '',
    meta_desc: ''
  };

  public isLoading = false;
  public isPageLoading = true;
  public descriptionError = false;
  public titleError = false;
  public subtitleError = false;
  public errMessage = '';
  public newsID: any;

  public option: Object = {
    placeholderText: 'Edit your content here',
    charCounterCount: false
  }

  constructor(private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _newsService: NewsService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private CKEditorModule: CKEditorModule,
    private toastr: ToastrService) {

    this.newsID = _activateRouter.snapshot.params['id'];
    this.spinner.show();
    if (this.newsID) {
      this._newsService.get(this.newsID).subscribe(res => {
        this.spinner.hide();
        // this.isPageLoading = false;
        if (res.success) {
          this.news = res.data.news[0];
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isPageLoading = false;
      });
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }
  }

  ngOnInit(): void { }

  /*If newsID exist then will update existing news otherwise will add new news*/
  save() {
    //this.news.title = this.news.title.trim();
    //if(!this.news.title) return;
    if (!this.news.title) {
      this.titleError = true;
      return;
    }
    if (!this.news.subtitle) {
      this.subtitleError = true;
      return;
    }
    if (!this.news.description) {
      this.descriptionError = true;
      return;
    }
    this.spinner.show();
    // this.isLoading = true;
    if (this.newsID) {
      this._newsService.update(this.news).subscribe(res => {
        this.spinner.hide();
        // this.isLoading = false;
        this.toastr.success('News Updated successfully.');
        this._router.navigate(['/news/list']);
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      })
    } else {
      this._newsService.add(this.news).subscribe(res => {
        this.spinner.hide();
        // this.isLoading = false;
        console.log("res", res)
        if (res.success) {
          this.toastr.success('News Updated successfully.');
          this._router.navigate(['/news/list']);
        } else {
          window.scrollTo(0, 0);
          this.errMessage = res.error.message;
          this.toastr.error(res.error.message);
          // this.showAlert();
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'news'
    }
    
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(res => {
      
      // this.isLoading = false;
      if (res.success) {
        this.news.image = res.data.fullPath;
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message);
        // // this._flashMessagesService.show( res.error.message, {
        // //     classes: ['alert', 'alert-danger'],
        // //     timeout: 3000,
        // });
      }
      this.myInputVariable.nativeElement.value = "";
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  removeImage(image) {
    this.news.image = '';
  }

  // showAlert(): void {

  //         this._flashMessagesService.show( this.errMessage, {
  //             classes: ['alert', 'alert-danger'],
  //             timeout: 3000,
  //         });
  // }
}
