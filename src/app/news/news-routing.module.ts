import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListComponent } from './list-component/list.component';
import { AddupdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'News'
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'List News'
        }
      },
      {
        path: 'list',
        component: ListComponent,
        data: {
          title: 'List News'
        }
      },
      {
        path: 'add',
        component: AddupdateComponent,
        data: {
          title: 'Add News'
        }
      },
      {
        path: 'list/:id',
        component: ViewComponent,
        data: {
          title: 'View News'
        }
      },

      {
        path: 'edit/:id',
        component: AddupdateComponent,
        data: {
          title: 'Edit News'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
  	RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
  	RouterModule,
    FormsModule,
  ]
})
export class NewsRoutingModule { }
