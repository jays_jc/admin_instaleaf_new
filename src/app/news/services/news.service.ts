import { CookieService } from 'ngx-cookie';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class NewsService {


  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _CommanService: CommanService,
    private _cookieService: CookieService
  ) {

  }

  /*Use to fetch all Inputs*/
  getAllNews(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'news', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to get news with crop id*/
  get(newsID) {
    return this._http.get(this._host + 'news/' + newsID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/news/'+ newsID, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to add new news*/
  add(news) {
    return this._http.post(this._host + 'news', news).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // console.log("news in service",news)
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.post(this._host +'/news', news, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to update news*/
  update(news) {
    return this._http.put(this._host + 'news/' + news.id, news).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/news/'+ news.id, news, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to Delete news with news id */
  delete(newsID) {
    return this._http.delete(this._host + 'news/' + newsID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.delete(this._host +'/news/'+ newsID,  { headers: headers }).map((res:Response) => res.json());
  }

  getUserID() {
    let userData = this._cookieService.getObject('userData');
    if (userData) {
      return userData['id'];
    }
  }



  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }

}
