import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AddupdateComponent } from './addupdate-component/addupdate.component';
import { NewsRoutingModule } from './news-routing.module';
import { ListComponent } from './list-component/list.component';
import { ViewComponent } from './view-component/view.component';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NewsService } from './services/news.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CategoryService } from '../category/services/category.service';
import { CKEditorModule } from 'ng2-ckeditor';
import { ImageUploadModule } from 'ng2-imageupload';

@NgModule({
	imports: [
		CommonModule,
		NewsRoutingModule,
		FormsModule,
		NgxDatatableModule,
		NgxSpinnerModule,
		CKEditorModule,
		ImageUploadModule
	],
	exports: [
		RouterModule,
		FormsModule
	],
	providers: [NewsService, CategoryService],
	declarations: [
		ListComponent,
		AddupdateComponent,
		ViewComponent,
	],
	entryComponents: [
	],
})
export class NewsModule { }
