import { CookieService } from 'ngx-cookie';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private _cookieService: CookieService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  logout() {
    this._cookieService.removeAll();
    this.router.navigate(['/']);
  }

}
