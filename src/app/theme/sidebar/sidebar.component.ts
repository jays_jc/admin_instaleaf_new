import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() active: string;
  // tooglebutton=false;
  @Input() tooglebutton: any;
  // @Output() togglebutton = new EventEmitter<any>();
  @Output() Layout = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    console.log(this.tooglebutton, "this jashd")
  }

  changeSidebar() {
    this.tooglebutton = !this.tooglebutton;
    this.Layout.emit(this.tooglebutton);

  }

}
