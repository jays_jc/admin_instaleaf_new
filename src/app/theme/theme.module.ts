import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThemeRoutingModule } from './theme-routing.module';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [HeaderComponent, SidebarComponent, FooterComponent, LayoutComponent],
  imports: [
    CommonModule,
    ThemeRoutingModule,
    NgxSpinnerModule
  ]
})
export class ThemeModule { }
