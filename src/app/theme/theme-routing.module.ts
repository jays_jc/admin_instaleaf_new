import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthModule } from '../auth/auth.module';
import { DashboardModule } from '../dashboard/dashboard.module';
import { BannersModule } from '../banners/banners.module';
import { OrdersModule } from '../orders/orders.module';
import { ReserveOrderModule } from '../reserve-order/reserve-orders.module';
import { DispensaryModule } from '../dispensary/dispensary.module';
import { CategoryModule } from '../category/category.module';
import { ProductCategoryModule } from '../product-category/product-category.module';
import { PosCategoryModule } from '../pos-category/pos-category.module';
import { BrandModule } from '../products/brand.module';
import { MarketplaceModule } from '../marketplace/marketplace.module';
import { AffiliateModule } from '../affiliate/affiliate.module';
import { PlanManagementModule } from '../plan-management/plan-management.module';
import { SubscriptionsModule } from '../subscriptions/subscriptions.module';
import { RewardManagemnetModule } from '../reward-managemnet/reward-managemnet.module';
import { TagsModule } from '../tags/tags.module';
import { ProducerModule } from '../producer/producer.module';
import { TerpenProfileModule } from '../terpen-profile/terpen-profile.module';
import { UsersModule } from '../users/users.module';
import { SuppliersModule } from '../suppliers/suppliers.module';
import { DriversModule } from '../drivers/drivers.module';
import { AutomationModule } from '../automation/automation.module';
import { NewsModule } from '../news/news.module';
import { CityModule } from '../city/city.module';
import { StoreIntegrationModule } from '../store-integration/store-integration.module';
import { PosManagementModule } from '../pos-management/pos-management.module';
import { SettingsModule } from '../settings/settings.module';
import { AuthGuard } from '../shared/auth.guard';
import { ProfileModule } from '../profile/profile.module';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    // loadChildren: '../auth/auth.module#AuthModule',
    loadChildren: () => AuthModule,
    // component: LayoutComponent,
    // canActivate: [ActiveRouteGuard]
  },
  {

    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        // loadChildren: './dashboard/dashboard.module#DashboardModule',
        loadChildren: () => DashboardModule,
      },
      {
        path: 'banners',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => BannersModule,
      },
      {
        path: 'order',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => OrdersModule,
      },
      {
        path: 'reserve-order',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => ReserveOrderModule,
      },
      {
        path: 'dispensary',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => DispensaryModule,
      },
      {
        path: 'category',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => CategoryModule,
      },
      {
        path: 'product-category',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => ProductCategoryModule,
      }, {
        path: 'pos-category',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => PosCategoryModule,
      }, {
        path: 'product',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => BrandModule,
      }, {
        path: 'market',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => MarketplaceModule,
      }, {
        path: 'affiliate',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => AffiliateModule,
      }, {
        path: 'plan-management',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => PlanManagementModule,
      }, {
        path: 'subscrptions',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => SubscriptionsModule,
      }, {
        path: 'reward',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => RewardManagemnetModule,
      }, {
        path: 'tags',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => TagsModule,
      }, {
        path: 'producer',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => ProducerModule,
      }, {
        path: 'terpen',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => TerpenProfileModule,
      }, {
        path: 'user',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => UsersModule,
      }, {
        path: 'supplier',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => SuppliersModule,
      }, {
        path: 'drivers',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => DriversModule,
      }, {
        path: 'automation',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => AutomationModule,
      }, {
        path: 'news',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => NewsModule,
      }, {
        path: 'city',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => CityModule,
      }, {
        path: 'store',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => StoreIntegrationModule,
      }, {
        path: 'pos-management',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => PosManagementModule,
      }, {
        path: 'setting',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => SettingsModule,
      },
      {
        path: 'profile',
        // loadChildren: './banners/banners.module#BannersModule',
        loadChildren: () => ProfileModule,
      },
      //   {
      //     path: 'profile',
      //     loadChildren: './profile/profile.module#ProfileModule',
      //   },
      //   {
      //     path: 'items',
      //     loadChildren: './dispensary/dispensary.module#DispensaryModule',
      //   },
      //   {
      //     path: 'category',
      //     loadChildren: './category/category.module#CategoryModule',
      //   },
      //   {
      //     path: 'product-category',
      //     loadChildren: './product-category/category.module#CategoryModule',
      //   },
      //   {
      //     path: 'poscategory',
      //     loadChildren: './pos-category/poscategory.module#PosCategoryModule',
      //   },
      //   {
      //     path: 'products',
      //     loadChildren: './products/brand.module#BrandModule',
      //   },
      //   {
      //     path: 'automation',
      //     loadChildren: './automation/automation.module#AutomationModule',
      //   },
      //   {
      //     path: 'scheduler',
      //     loadChildren: './scheduler/scheduler.module#SchedulerModule',
      //   },
      //   {
      //     path: 'news',
      //     loadChildren: './news/news.module#NewsModule',
      //   },
      //   {
      //     path: 'city',
      //     loadChildren: './city/city.module#CityModule',
      //   },
      //   {
      //     path: 'users',
      //     loadChildren: './users/users.module#UsersModule',
      //   },
      //   {
      //     path: 'drivers',
      //     loadChildren: './drivers/drivers.module#DriversModule',
      //   },
      //   {
      //     path: 'settings',
      //     loadChildren: './settings/settings.module#SettingsModule',
      //   },
      //   {
      //     path: 'roles',
      //     loadChildren: './roles/roles.module#RolesModule',
      //   },
      //   {
      //     path: 'admin-users',
      //     loadChildren: './admin-users/admin-users.module#AdminUsersModule',
      //   },
      //   {
      //     path: 'tags',
      //     loadChildren: './tags/tags.module#TagsModule',
      //   },
      //   {
      //     path: 'producer',
      //     loadChildren: './producer/producer.module#ProducerModule',
      //   },
      //   {
      //     path: 'terpen-profile',
      //     loadChildren:
      //       './terpen-profile/terpen-profile.module#TerpenProfileModule',
      //   },
      //   {
      //     path: 'reward-management',
      //     loadChildren:
      //       './reward-managemnet/reward-managemnet.module#RewardManagemnetModule',
      //   },
      //   {
      //     path: 'store_integration',
      //     loadChildren:
      //       './store-integration/store-integration.module#StoreIntegrationModule',
      //   },
      //   {
      //     path: 'subscriptions',
      //     loadChildren:
      //       './subscriptions/subscriptions.module#SubscriptionsManagemnetModule',
      //   },
      //   {
      //     path: 'plan-management',
      //     loadChildren:
      //       './plan-management/plan-management.module#PlanManagementModule',
      //   },
      //   {
      //     path: 'pos_management',
      //     loadChildren:
      //       './pos-management/pos-management.module#PosManagementModule',
      //   },
      //   {
      //     path: 'marketplace',
      //     loadChildren: './marketplace/marketplace.module#MarketplaceModule',
      //   },
      //   {
      //     path: 'orders',
      //     loadChildren: './orders/orders.module#OrdersModule',
      //   },
      //   {
      //     path: 'affiliate',
      //     loadChildren: './affiliate/affiliate.module#AffiliateModule',
      //   },
      //   {
      //     path: 'reserve_orders',
      //     loadChildren:
      //       './reserve-orders/reserve-orders.module#ReserveOrdersModule',
      //   },
      //   // {
      //   //   path: 'academy',
      //   //   loadChildren: './academy/academy.module#AcademyModule'
      //   // },
      //   // {
      //   //   path: 'suppliers',
      //   //   loadChildren: './suppliers/suppliers.module#SuppliersModule',
      //   // },
      //   // {
      //   //   path: 'banners',
      //   //   loadChildren: './banners/banners.module#BannersModule',
      //   // },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThemeRoutingModule { }
