import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ListComponentComponent } from './list-component/list-component.component';
import { AddupdateComponent } from './addupdate/addupdate.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Banner Management',
    },
    children: [
      {
        path: '',
        component: ListComponentComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'list',
        component: ListComponentComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'add',
        component: AddupdateComponent,
        data: {
          title: 'Add Banner',
        },
      },
      {
        path: 'edit',
        component: AddupdateComponent,
        data: {
          title: 'Edit Banner',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BannersRoutingModule {}
