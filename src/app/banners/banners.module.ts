import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NG2DataTableModule } from "angular2-datatable-pagination";
// import { BannersService } from './services/banners.service';
// import { SharedModule } from '../shared/shared.module';
//import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BannersRoutingModule } from './banners-routing.module';
import { ListComponentComponent } from './list-component/list-component.component';
import { AddupdateComponent } from './addupdate/addupdate.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BannersService } from './services/banners.service';
import { ImageUploadModule } from 'ng2-imageupload';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  imports: [
    BannersRoutingModule,
    CommonModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    ImageUploadModule
  ],
  declarations: [ListComponentComponent, AddupdateComponent],
  providers: [BannersService],
})
export class BannersModule {}
