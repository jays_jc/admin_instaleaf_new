import { ToastrService } from 'ngx-toastr';
import { CommanService } from 'src/app/shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { BannersService } from '../services/banners.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { BannersService } from '../services/banners.service';
import { environment } from 'src/environments/environment';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-list-component',
  templateUrl: './list-component.component.html',
  styleUrls: ['./list-component.component.scss'],
})
export class ListComponentComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
  };
  bannerList: any = [];
  totalItems: any;
  response: any;

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private bannerService: BannersService,
    private _activateRouter: ActivatedRoute,
    private _commanService: CommanService,
    private toastr: ToastrService,
  ) { }
  ngOnInit(): void {
    // this.getBanners();
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getBanners();
  }

  // view(userID) {
  //   let route = '/products/view/' + userID;
  //   this.router.navigate([route]);
  // }

  edit(ID) {
    let route = '/banners/edit/' + ID;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete?")) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.deleteRecord(ID, 'banners').subscribe(res => {
        if (res.success) {
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1;
          //    if(this.activePage == 0) this.activePage = 1;
          // }
          /* reload page data */
          this.getBanners();
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
        } else {

          this.toastr.error(res.error.message);
          // this.isLoading = false;
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }

  getBanners() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.bannerService
      .getAllBanners(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.bannerList = [];
          // this.spinner.hide();
          // this.isLoading = false;
        } else {
          console.log(response['data']);
          this.totalItems = response.data.total;
          this.bannerList = response['data'].data.map((data) => {
            return {
              id: data.id,
              name: data.name,
              image: data.image,
              bannerType: data.bannerType,
              bannerSize: data.bannerSize,
              status: data.status,
            };
          });
          // this.isLoading = false;
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  changeStatus(data, ID) {
    let status = '';
    let message = 'Do you want to activate this banner ?';
    if (data == 'active') {
      status = "deactive";
      message = 'Do you want to deactivate this banner ?';
    } else {
      status = "active";
    }

    if (confirm(message)) {
      this.spinner.show();
      this._commanService.changeStatus(ID,'banners',status).subscribe(res => {
        if(res.success) {
            this.response  = res;
           this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
            /* reload page. */
            this.getBanners();
        } else {
          this.toastr.error(res.error.message);
            // this.isLoading    = false;
            // this._commanService.showAlert(res.error.message,'alert-danger');
            // this._commanService.checkAccessToken(res.error);
        }
        this.spinner.hide();
    },err => {
      this.spinner.hide();
        // this.isLoading = false;
    }); 
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/products/list/' + page;
    this.getBanners();
    // this.router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getBanners();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getBanners();
  }
}
