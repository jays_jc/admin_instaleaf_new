import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BannersService } from '../services/banners.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
// import { CookieService } from 'ngx-cookie';
// import { CommanService } from '../../shared/services/comman.service';
// import { FlashMessagesService } from 'ngx-flash-messages';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { environment } from 'src/environments/environment';
// import tsConstants = require('./../../tsconstant'); 
import { NgxSpinnerService } from 'ngx-spinner';

// import tsMessages  = require('../../tsmessage');

@Component({
  selector: 'app-addupdate',
  templateUrl: './addupdate.component.html',
  styleUrls: ['./addupdate.component.scss'],
})
export class AddupdateComponent {
  private _host = environment.config.HOST;
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  @ViewChild('myFileInput',{static: false}) myFileInput;
  public bannerForm: FormGroup;
  submitted = false;
  _bannerObservable: any;
  banner: any = [];
  bannerId: any;
  bannerAddForm: boolean = true;
  headerTitle: string;
  bannerImagePath: any = '';
  public banner_types = [
    'Store',
    'News',
    'Products',
    'Item Detail',
    'MobileStore',
  ];
  public banner_size = [
    '1140px X 350px',
    '250px X 470px',
    '400px X 150px',
    '260px X 329px',
    '250px X 129px',
  ];
  constructor(
    private formBuilder: FormBuilder,
    // private spinner: NgxSpinnerService,
    private _bannerService: BannersService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
  ) {
    this.createForm();
  }

  createForm() {
    this.bannerForm = this.formBuilder.group({
      name: ['', Validators.required],
      bannerType: ['', Validators.required],
      bannerSize: ['', Validators.required],
      url: ['', Validators.required],
      image: ['']
    });
  }

  get f() {
    return this.bannerForm.controls;
  }

  ngOnInit(): void {
    // this.getClients();
    this.route.queryParams.subscribe((params) => {
      this.bannerId = params['id'];
      if (params['view'] == 'true' && this.bannerId) {
        this.headerTitle = 'View Banner';
        this.getBannerData();
        // this.addAbbrevationForm.patchValue({
        //   clientName: params['clientName'],
        //   abbreviation: params['abbreviation'],
        // })
      } else if (this.bannerId) {
        this.headerTitle = 'Update Banner';
        this.bannerAddForm = false;
        this.getBannerData();
        // this.addAbbrevationForm.patchValue({
        //   clientName: params['clientName'],
        //   abbreviation: params['abbreviation'],
        // })
      } else {
        this.headerTitle = 'Add Banner';
        this.bannerAddForm = true;
      }
    });
  }

  getBannerData() {
    this._bannerService.get(this.bannerId).subscribe(
      (res) => {
        // this.isPageLoading = false;
        if (res.success) {
          this.banner = res.data;
          this.bannerForm.patchValue(this.banner);
          this.bannerImagePath = res.data.image;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {
        // this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    if (this.bannerAddForm) {
      if (!this.bannerForm.invalid) {
        this.spinner.show();
        this._bannerObservable = this._bannerService
          .post(this.bannerForm.value, 'banner')
          .subscribe(
            (res) => {
              if (res.success) {
                this.toastr.success(res.data.message);
                this.router.navigate(['/banners/list']);
              } else {
                this.toastr.error(res.error.message);
              }
              this.spinner.hide();
            },
            (error) => {
              this.spinner.hide();
              this.toastr.error(error);
            }
          );
      }
    } else {
      if (!this.bannerForm.invalid) {
        var data = {
          name: this.bannerForm.value.name,
          image: this.bannerImagePath,
          bannerSize: this.bannerForm.value.bannerSize,
          bannerType: this.bannerForm.value.bannerType,
        };
        this.spinner.show();
        this._bannerObservable = this._bannerService
          .update(data, this.bannerId)
          .subscribe(
            (res) => {
              if (res.success) {
                this.toastr.success(res.data.message);
                this.router.navigate(['/banners/list']);
              } else {
                this.toastr.error(res.error.message);
              }
              this.spinner.hide();
            },
            (error) => {
              this.spinner.hide();
              this.toastr.error(error);
            }
          );
      }
    }
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'banners',
    };
    this.spinner.show();

    // this.isLoading = true;
    this._bannerService.post(object, 'upload').subscribe(
      (res) => {
        // this.isLoading = false;
        if (res.success) {
          this.bannerImagePath = res.data.fullPath;
          this.bannerForm.patchValue({
            image: this.bannerImagePath
          })
          this.myFileInput.nativeElement.value = '';
        } else {
          window.scrollTo(0, 0);
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }

        this.spinner.hide();
        // this.myInputVariable.nativeElement.value = '';
      },
      (err) => {
        this.spinner.hide();
        this.toastr.error(err);
        // this.isLoading = false;
      }
    );
  }

  removeImage(image) {
    this.bannerImagePath = '';
  }

  ngOnDestroy(): void {
    if (this._bannerObservable) {
      this._bannerObservable.unsubscribe();
    }
  }
}
