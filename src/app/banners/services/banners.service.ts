import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { CommanService } from 'src/app/shared/comman.service';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

// import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
// import { CommanService } from '../../shared/services/comman.service';
// import tsConstants = require('./../../tsconstant');

@Injectable()
export class BannersService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService
  ) { }

  /*Use to fetch all Inputs*/
  getAllBanners(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'allbanner', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to add new Input*/
  // add(banner) {

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.post(this._host +'/banner', banner, { headers: headers }).map((res:Response) => res.json())
  // }

  // /*Use to get category with id*/
  get(id) {
    return this._http.get(this._host + 'banner/' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  post(body, path) {
    return this._http.post(this._host + path, body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  update(context, id) {
    return this._http.put(this._host + `banner/` + id, context).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  // /*Use to update category*/
  // update(category) {

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.put(this._host +'/banner/'+ category.id, category, { headers: headers }).map((res:Response) => res.json())
  // }

  // getBannerByType(type) {
  //     let headers = this._commanService.getAuthorizationHeader();
  //     let url = this._host + '/bannerbytype/'+ type;
  //     return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  // }

  // updateNewsCategory(newsinfo) {
  //     let obj = {
  //         id:newsinfo.id,
  //         categoryId:newsinfo.categoryId
  //     }
  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.put(this._host +'/addnewscategory', newsinfo, { headers: headers }).map((res:Response) => res.json())
  // }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
