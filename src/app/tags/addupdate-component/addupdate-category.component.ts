import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, ViewChild } from '@angular/core';
// import { CategoryService } from '../services/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';
import { ImageResult } from 'ng2-imageupload';
// import { CookieService } from 'ngx-cookie';
// import { CommanService } from '../../shared/services/comman.service';
// import { FlashMessagesService } from 'ngx-flash-messages';
// import tsMessages  = require('../../tsmessage');
// import tsConstants = require('./../../tsconstant');
// import { ImageResult, ResizeOptions } from 'ng2-imageupload';

@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate-category.component.scss']
})
export class AddUpdateCategoryComponent {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;

  public category = {
    "name": "",
    "tagname": "",
    "image": ""
  };

  private _host = environment.config.HOST;
  public isLoading = false;
  public isPageLoading = true;
  public categoryID: any;
  public oBj = { vname: '' };
  public response: any;
  public type;
  public errMessage = '';
  public ParentCategories = ['Effects', 'Consumption', 'Activities', 'Medical', 'Negatives'];




  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.spinner.show();
    this.categoryID = this._activateRouter.snapshot.params['id'];

    if (this.categoryID) {
      this._catgService.get(this.categoryID).subscribe(res => {
        this.spinner.hide();
        // this.isPageLoading = false;
        if (res.success) {
          this.category = res.data[0];
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      });
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }


  }

  ngOnInit(): void {

    // this.showDangerAlert();
  }

  /*If categoryID exist then will update existing category otherwise will add new category*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.category));
    if (this.categoryID) {
      this._catgService.update(data).subscribe(res => {
        this.spinner.hide();
        // this.isLoading         = false;
        if (res.success) {
          this.response = res;
          this.toastr.success('Tags updated successfully!');
          this._cookieService.put('categoryAlert', res.data.message);
          this._router.navigate(['/tags/list']);
        } else {
          this._cookieService.put('categoryExistAlert', res.error.message);
          this.toastr.error(res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      })
    } else {
      this._catgService.add(data).subscribe(res => {
        this.spinner.hide();
        // this.isLoading         = false;
        if (res.success) {
          this.response = res;
          this.toastr.success('Tags added successfully!');
          this._cookieService.put('categoryAlert', res.data.message);
          this._router.navigate(['/tags/list']);
        } else {
          this._cookieService.put('categoryExistAlert', res.error.message);
          this.toastr.error(res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });

    }
  }


  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'product'
    }
    
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(res => {
     
      // this.isLoading = false;
      if (res.success) {
        this.category["image"] = res.data.fullPath;
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
      this.myInputVariable.nativeElement.value = "";
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }


}
