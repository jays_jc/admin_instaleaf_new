import { CookieService } from 'ngx-cookie';
import { CommanService } from './../../shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { CategoryService } from '../services/category.service';
@Component({
  selector: 'app-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
})
export class ListCategoryComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
  };
  categoryList: any = [];
  public addEditDelete: boolean = false;
  response: any;
  totalItems: any;

  public constructor(
    private _router: Router,
    private CategoryService: CategoryService,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService,
    private _cookieService: CookieService
  ) {
    let actions = this._commanService.getActions();
    if (actions["type"] == 'SA' || actions['category']['addEditDelete']) this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();
  }
  /*Get all Users */
  getCategory() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.CategoryService.getAllCategory(
      this.filters
    ).subscribe((response) => {
      // console.log('data', response.data);
      if (!response.success) {
        this.categoryList = [];
        // this.isLoading = false;
        // this.spinner.hide();
      } else {
        this.totalItems = response.data.total;
        this.categoryList = response['data'].category.map((data) => {
          return {
            id: data.id,
            name: data.name,
            tagname: data.tagname,
            createdAt: data.createdAt,
          };
        });
        // this.isLoading = false;
        // this.spinner.hide();
      }
      this.spinner.hide();
    },
      error => {
        this.spinner.hide();
      }
    );
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/tags/list/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, {
      page: 1,
      search: this.filters.search,
    });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  view(ID) {
    let route = '/tags/list/' + ID
    this._router.navigate([route]);
  }

  edit(ID) {
    let route = '/tags/edit/' + ID;
    this._router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete?")) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.deleteRecord(ID, 'productreviewsubcategory').subscribe(res => {
        this.response = res;
        this.spinner.hide();
        // this.isLoading = false;  
        // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
        // this.itemsTotal = this.itemsTotal - 1;

        // if( ! (this.itemsTotal >= start) ){
        //    this.activePage = this.activePage -1
        // }
        this._cookieService.put('categoryAlert', 'Deleted successfully.');
        /* reload page. */
        this.getCategory();
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }
}
