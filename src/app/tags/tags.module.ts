import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CategoryService } from './services/category.service';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { ViewCategoryComponent } from './view-component/view-category.component';
import { TagsRoutingModule } from './tags-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
  	TagsRoutingModule,
  	 CommonModule,  
     NgxDatatableModule,
     NgxSpinnerModule,
     ImageUploadModule

  ],
  exports: [
		RouterModule,
	],
  providers: [
    CategoryService
  ],
  declarations: [
  	ListCategoryComponent,
  	AddUpdateCategoryComponent,
  	ViewCategoryComponent
  ]
})
export class TagsModule { }