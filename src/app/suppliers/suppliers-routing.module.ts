import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';

import { ListComponentComponent } from './list-component/list-component.component';
import { AddupdateComponentComponent } from './addupdate-component/addupdate-component.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Suppliers'
    },
    children: [
     {
        path: '',
        component: ListComponentComponent,
        data: {
          title: 'Suppliers List'
        }
      },
      {
        path: 'list',
        component: ListComponentComponent,
        data: {
          title: 'Suppliers List'
        }
      },
      {
        path: 'add',
        component: AddupdateComponentComponent,
        data: {
          title: 'Add Supplier'
        }
      },
      {
        path: 'edit/:id',
        component: AddupdateComponentComponent,
        data: {
          title: 'Edit Supplier'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
    RouterModule,
    FormsModule,
  ]
})
export class SuppliersRoutingModule { }