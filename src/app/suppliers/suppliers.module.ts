import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuppliersRoutingModule } from './suppliers-routing.module';
import { AddupdateComponentComponent } from './addupdate-component/addupdate-component.component';
import { ListComponentComponent } from './list-component/list-component.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SuppliersService } from './services/suppliers.service';
@NgModule({
  imports: [
    CommonModule,
    SuppliersRoutingModule,
    CommonModule,
    NgxDatatableModule,
    NgxSpinnerModule
  ],
  providers: [
    SuppliersService
  ],
  declarations: [AddupdateComponentComponent, ListComponentComponent]
})
export class SuppliersModule { }
