import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddupdateComponentComponent } from './addupdate-component.component';

describe('AddupdateComponentComponent', () => {
  let component: AddupdateComponentComponent;
  let fixture: ComponentFixture<AddupdateComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddupdateComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddupdateComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
