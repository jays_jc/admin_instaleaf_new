import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SuppliersService } from '../services/suppliers.service';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  selector: 'app-addupdate-component',
  templateUrl: './addupdate-component.component.html',
  styleUrls: ['./addupdate-component.component.scss']
})
export class AddupdateComponentComponent implements OnInit {

  public subscriber = {
    name: '',
    instaleaf_commission: 0.0,
    supplier_commission: 0.0,
    email: '',
    account_number: '',
    bank_name: '',
    accountholder_name: '',
    mobile: '',
    //   country:'',
    routing_number: '',
    first_name: '',
    last_name: '',
    dobDate: '',
    ssn: '',
  };

  public isLoading = false;
  public isPageLoading = true;
  subscriberID: any;
  date: Date;


  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _supplierService: SuppliersService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
  ) {

    this.subscriberID = _activateRouter.snapshot.params['id'];
    this.spinner.show();
    if (this.subscriberID) {
      this._supplierService.get(this.subscriberID).subscribe(res => {
        this.spinner.hide();
        // this.isPageLoading = false;
        if (res.success) {
          this.subscriber = res.data;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      });
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }
  }

  ngOnInit() {
    // this.date = new Date();
  }

  /*If subscriberID exist then will update existing subscriber otherwise will add new subscriber*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.subscriber));
    if (this.subscriberID) {
      this._supplierService.update(data).subscribe(res => {
        // this.isLoading         = false;
        if (res.success) {
          this.updateBank();
          // this._cookieService.put('supplierAlert', res.data.message);
          // this._router.navigate(['/suppliers/list']);
        } else {
          this.toastr.error(res.error.message);
          this._cookieService.put('supplierExistAlert', res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      })
    } else {
      this._supplierService.add(data).subscribe(res => {
        // this.isLoading         = false;
        if (res.success) {
          // console.log(res);
          this.bankAdd(res.data.data.id);
          // this._cookieService.put('supplierAlert', res.data.message);
          // this._router.navigate(['/suppliers/list']);
        } else {
          this.toastr.error(res.error.message);
          this._cookieService.put('supplierExistAlert', res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });

    }
  }

  bankAdd(id) {

    var days = new Date(this.subscriber.dobDate).getDate();
    var month = new Date(this.subscriber.dobDate).getMonth() + 1;
    var year = new Date(this.subscriber.dobDate).getFullYear();
    this.subscriber['dob'] = {
      "day": days,
      "month": month,
      "year": year
    }
    this.subscriber['id'] = id;

    let data = JSON.parse(JSON.stringify(this.subscriber));
    this.spinner.show();
    this._supplierService.addBank(data).subscribe(res => {
      this.spinner.hide();
      // this.isLoading         = false;
      if (res.success) {
        this.toastr.success('Supplier added Successfully!');
        this._cookieService.put('supplierAlert', 'Supplier added Successfully!');
        this._router.navigate(['/suppliers/list']);
      } else {
        this.toastr.error(res.error.message);
        this._cookieService.put('supplierExistAlert', res.error.message);
        // this.showDangerAlert();
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  updateBank() {
    this.spinner.show();
    let data = JSON.parse(JSON.stringify(this.subscriber));
    this._supplierService.updateBank(data).subscribe(res => {
      this.spinner.hide();
      // this.isLoading         = false;
      if (res.success) {
        this.toastr.success('Supplier updated Successfully!');
        this._cookieService.put('supplierAlert', 'Supplier updated successfully!');
        this._router.navigate(['/supplier']);
      } else {
        this.toastr.error(res.error.message);
        this._cookieService.put('supplierExistAlert', res.error.message);
        // this.showDangerAlert();
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }



}
