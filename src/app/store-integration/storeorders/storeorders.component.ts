import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { StoreIntegrationService } from '../services/store-integration.service';
import { CommanService } from 'src/app/shared/comman.service';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
    selector: 'app-storeorders',
    templateUrl: './storeorders.component.html',
    styleUrls: ['./storeorders.component.scss']
})
export class StoreordersComponent implements OnInit {

    columns = [];
    ColumnMode = ColumnMode;
    page = 0;
    filters: { page: number; count: number; search: string; sortBy: string; } = {
        page: 0,
        count: 5,
        search: '',
        sortBy: 'createdAt desc'
    };
    reservedOrderList: any = [];
    public response: any;
    public addEditDelete: boolean = false;

    public roles = 'DRIVER';
    public exportData = [];
    private _subscriberData: any;
    totalItems: any;

    public constructor(
        private _router: Router,
        private _storeIntegrationService: StoreIntegrationService,
        private _commanService: CommanService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService,
        private _activateRouter: ActivatedRoute
    ) {

        let actions = this._commanService.getActions();
        if (actions["type"] == 'SA' || actions['users']['addEditDelete']) this.addEditDelete = true;
    }

    ngOnInit(): void {

        this._router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0)
        });

        this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
        // this.page = this.filters.page;
        Object.assign(this.filters, { page: this.filters.page });
        this.getReservedOrders();
    }

    public toInt(num: string) {
        return +num;
    }

    public sortByWordLength = (a: any) => {
        return a.city.length;
    }

    view(orderId) {
        let route = '/store/orderdetail/' + orderId;
        this._router.navigate([route]);
    }

    // changeStatus(user) {

    //     if(confirm("Do you want to Mark as deliver ?")) {
    //         this.isLoading = true;
    //         user['order_status'] = "Delivered"
    //         this._driverService.update(user).subscribe(res => {
    //             if(res.success) {
    //                 this.response  = res;
    //                 let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
    //                 this.itemsTotal = this.itemsTotal - 1;

    //                 if( ! (this.itemsTotal >= start) ){
    //                    this.activePage = this.activePage -1
    //                 }
    //                 this._commanService.showAlert(res.data.message,'alert-success');
    //                 /* reload page. */
    //                 this.getReservedOrders();
    //             } else {
    //                 this.isLoading    = false;
    //                 this._commanService.showAlert(res.error.message,'alert-danger');
    //                 this._commanService.checkAccessToken(res.error);
    //             }
    //         },err => {
    //             this.isLoading = false;
    //         });             
    //     }
    // }

    // sendInvoice(order) {
    //     if(confirm("Do you want to send invoice to supplier ?")) {
    //         this.isLoading = true;
    //         //user['isInvoice'] = "Sent"
    //         this._driverService.sendingInvoice(order).subscribe(res => {
    //             if(res.success) {
    //                 this.response  = res;
    //                 this._commanService.showAlert(res.data.message,'alert-success');
    //                 /* reload page. */
    //                 this.getReservedOrders();
    //             } else {
    //                 this.isLoading    = false;
    //                 this._commanService.showAlert(res.error.message,'alert-danger');
    //                 this._commanService.checkAccessToken(res.error);
    //             }
    //         },err => {
    //             this.isLoading = false;
    //         });             
    //     }
    // }

    /*Get all Users */
    getReservedOrders(): void {
        // this.isLoading = true;
        this.spinner.show();
        this._subscriberData = this._storeIntegrationService
            .getAllReservedOrders(this.filters)
            .subscribe((response) => {
                // console.log('data', response.data);
                if (!response.success) {

                    this.reservedOrderList = [];
                    // this.isLoading = false;
                    // this.spinner.hide();
                } else {
                    this.totalItems = response.data.total;
                    this.reservedOrderList = response['data'].data.map((data) => {
                        return {
                            id: data.id,
                            order_number: data.order_number,
                            dispensary: data.dispensary.name,
                            user: data.addedBy,
                            price: data.totalprice,
                            mobile: data.mobile,
                            status: data.order_status,
                            createdAt: data.createdAt
                        };
                    });
                    // this.isLoading = false;

                }
                this.spinner.hide();
            },
                error => {
                    this.spinner.hide();
                }
            );
    }

    setPage(e) {
        // console.log(e);
        this.page = e.offset + 1;
        Object.assign(this.filters, { page: this.page });
        // let route = '/store/' + page;
        this.getReservedOrders();
        // this._router.navigate([route]);
    }

    searchValue() {
        this.page = 0;
        Object.assign(this.filters, { page: 1, search: this.filters.search });
        this.getReservedOrders();
    }

    clearValue() {
        this.page = 0;
        this.filters.search = '';
        Object.assign(this.filters, { page: 1, search: this.filters.search });
        this.getReservedOrders();
    }
}