import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { StoreIntegrationService } from '../services/store-integration.service';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  selector: 'app-integration',
  templateUrl: './integration.component.html',
  styleUrls: ['./integration.component.scss']
})
export class IntegrationComponent implements OnInit {


  public object = {
    dispensary_id: '',
    auth_key: '',
    auth_token: '',
    api_url: '',
    company_id: '',
    location_id: '',
    pos_name: '',
  };
  public PosList = [];
  public DispanceryList = [];
  public isLoading = false;
  public isPageLoading: boolean = true;
  constructor(
    private integrationSer: StoreIntegrationService,
    private _router: Router,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.integrationSer.getAllPos().subscribe((res: any) => {
      this.PosList = res.data;
    },
      error => {
        this.spinner.hide();
      }
    );
    this.integrationSer.getAllDispancery().subscribe((res: any) => {
      var newData = [];
      var data = res.data;
      data.forEach((x) => {
        if (x.businessType == 'dispensary') {
          newData.push(x);
        }
      });
      this.DispanceryList = newData;
    });
    this.spinner.hide();
    // this.isPageLoading = false;
  }

  save() {
    this.spinner.show();
    // this.isPageLoading = true;
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.object));
    this.integrationSer.getApiData(data).subscribe(
      (res: any) => {
        console.log(res);
        if (res.success) {
          setTimeout(() => {
            this.toastr.success('Store data created successfully!')
            // this._commanService.showAlert(
            //   'Store data create successfully!',
            //   'alert-success'
            // );
            // this._commanService.back();
          }, 500);
          this.spinner.hide();
          // this.isPageLoading = false;
          this.checkfuncation();
        } else {
          this.spinner.hide();
          this.toastr.error(res.error.message);
          // this.isPageLoading = false;
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
      },
      (error) => {
        this.spinner.hide();
        this.toastr.error(error);
        // this.isPageLoading = false;
        // this._commanService.showAlert(error, 'alert-danger');
      }
    );
  }

  checkfuncation() {
    let route = '/store_integration/list';
    this._router.navigate([route]);
  }
}
