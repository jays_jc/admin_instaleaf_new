import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { StoreIntegrationService } from '../services/store-integration.service';

import { CommanService } from './../../shared/comman.service';
import { CookieService } from 'ngx-cookie';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-component',
  templateUrl: './list-component.component.html',
  styleUrls: ['./list-component.component.scss']
})
export class ListComponentComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string; date: string; } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
    date: new Date().getTime().toString(),
  };
  categoryList: any = [];
  public response: any;
  public addEditDelete: boolean = false;

  private _path;
  totalItems: any;
  public constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private StoreIntegrationService: StoreIntegrationService,
    private _commanService: CommanService,
  ) {

    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();

  }

  route(cropID, path) {
    let route = '/store/' + path + '/' + cropID;
    this._router.navigate([route]);
  }

  remove(ID) {
  }
  /*Get all Users */
  getCategory() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.StoreIntegrationService
      .getAllStore(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].data.map((data) => {
            return {
              id: data.id,
              name: data.name,
              api_url: data.api_url,
              auth_key: data.auth_key,
              auth_token: data.auth_token,
              location_id: data.location_id,
              company_id: data.company_id,
              pos_name: data.pos_name,
              updatedByCron: data.updatedByCron,
              dispensary_id: data.dispensary_id
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }
  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/store/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  edit(ID) {
    let route = '/store/edit/' + ID;
    this._router.navigate([route]);
  }
  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

}
