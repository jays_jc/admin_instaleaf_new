import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntegrationComponent } from './integration/integration.component';
import { ListComponentComponent } from './list-component/list-component.component';
import { EditComponentComponent } from './edit-component/edit-component.component';
import { StoreordersComponent } from './storeorders/storeorders.component';
import { OrderdetailComponent } from './orderdetail/orderdetail.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'All Store'
    },
    children: [
      {
        path: '',
        component: ListComponentComponent,
        data: {
          title: 'All Store'
        }
      },
      {
        path: 'add-store',
        component: IntegrationComponent,
        data: {
          title: 'Add Store'
        }
      },
      {
        path: 'edit-store/:id',
        component: EditComponentComponent,
        data: {
          title: 'Edit Store'
        }
      },
      {
        path: 'storeorders/:id',
        component: StoreordersComponent,
        // canActivate: [ChildRouteGuard],
        data: {
          title: 'Store Orders'
        }
      },
      {
        path: 'orderdetail/:id',
        component: OrderdetailComponent,
        // canActivate: [ChildRouteGuard],
        data: {
          title: 'Store Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class storeIntegrationRoutingModule { }
