import { OrderdetailComponent } from './orderdetail/orderdetail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntegrationComponent } from './integration/integration.component';
import { storeIntegrationRoutingModule } from './store-integration-routing.module';
import { FormsModule } from '@angular/forms';
import { StoreIntegrationService } from './services/store-integration.service';
import { ListComponentComponent } from './list-component/list-component.component';
import { EditComponentComponent } from './edit-component/edit-component.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { StoreordersComponent } from './storeorders/storeorders.component';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    storeIntegrationRoutingModule,
    NgxDatatableModule,
    NgxSpinnerModule,
    ImageUploadModule
  ],
  providers: [
    StoreIntegrationService
  ],
  declarations: [
    IntegrationComponent,
    ListComponentComponent,
    EditComponentComponent,
    StoreordersComponent,
    OrderdetailComponent
  ]
})
export class StoreIntegrationModule { }
