import { TestBed, inject } from '@angular/core/testing';

import { StoreIntegrationService } from './store-integration.service';

describe('StoreIntegrationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoreIntegrationService]
    });
  });

  it('should be created', inject([StoreIntegrationService], (service: StoreIntegrationService) => {
    expect(service).toBeTruthy();
  }));
});
