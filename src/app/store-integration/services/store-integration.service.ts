import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class StoreIntegrationService {

  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _CommanService: CommanService
  ) {

  }
  /*Use to fetch all Inputs*/
  getAllStore(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'all_store', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  getApiData(obj) {
    return this._http.post(this._host + 'storeInfo', obj).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let url = this._host+'/storeInfo?api_key='+obj.key+'&external_key='+obj.external_key+'&url='+obj.api_url+'&company_id='+obj.company_id+'&location_id='+obj.location_id+'&name='+obj.store_name+"&pos_name="+obj.pos_name
    // let headers = this._commanService.getAuthorizationHeader();
    // // return this.http.post(url, { headers: headers }).map((res:Response) => res.json()) 
    //  return this.http.post(this._host +'/storeInfo', obj, { headers: headers }).map((res:Response) => res.json()) 
  }

  get(orderId) {
    return this._http.get(this._host + 'reserveorders/' + orderId).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this.http.get(this._host +'/reserveorders/'+ orderId, { headers: headers }).map((res:Response) => res.json())
  }


  editStoreInfo(obj) {
    return this._http.post(this._host + 'updateStore', obj).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this.http.post(this._host +'/updateStore', obj, { headers: headers }).map((res:Response) => res.json()) 
  }

  getAllPos() {
    return this._http.get(this._host + 'getPosList').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let url = this._host +'/getPosList';
    //   let headers = this._commanService.getAuthorizationHeader();
    //   return this.http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  getAllDispancery() {
    return this._http.get(this._host + 'getnormaldispensary').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let url = this._host +'/getnormaldispensary';
    //   let headers = this._commanService.getAuthorizationHeader();
    //   return this.http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  getAllReservedOrders(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'allreservedorderslist', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let date =  new Date().getTime().toString();
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host +'/allreservedorderslist?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&search='+search;
    // return this.http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
