import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { StoreIntegrationService } from '../services/store-integration.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  selector: 'app-edit-component',
  templateUrl: './edit-component.component.html',
  styleUrls: ['./edit-component.component.scss']
})
export class EditComponentComponent implements OnInit {

  ID = '';
  public rowsOnPage = 5;
  public sortBy = "createdAt";
  public sortOrder = "desc";
  public activePage = 1;
  public itemsTotal = 0;
  public searchTerm = '';
  public city = '';
  public sortTrem = '';
  public itemsOnPage;
  public selectedFilter = '';
  public object = {
    dispensary_id: '',
    key: '',
    external_key: '',
    api_url: '',
    company_id: '',
    location_id: '',
    pos_name: ''
  };
  public selectedDispencey = '';
  public showDispencey = false;
  public PosList = [];
  public DispanceryList = [];
  public ok = "ok";
  public dispencry_id;
  public dispensaryName;
  public name;
  public PosName;
  filters: { id: string; } = {

    id: '',
  };
  constructor(
    private _router: Router,
    private integrationSer: StoreIntegrationService,
    private _activateRouter: ActivatedRoute,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.ID = _activateRouter.snapshot.params['id'];
    this.filters.id = this.ID;

    this.integrationSer.getAllStore(this.filters).subscribe((res: any) => {
      this.object = res.data[0];
      console.log("res.data[0]", res.data[0])
      //this.PosName = res.data[0].pos_name;
      //this.dispencry_id = res.data[0].dispensary_id;
      this.integrationSer.getAllDispancery().subscribe((res: any) => {
        var data = res.data;
        data.forEach(x => {
          if (x.businessType == 'dispensary') {
            if (x.id == this.dispencry_id) {
              this.dispensaryName = x.name;
            }
          }
        });
      })
    })
  }

  ngOnInit() {
    this.spinner.show();
    this.integrationSer.getAllPos().subscribe((res: any) => {
      // console.log("this is all pos",res.data);
      this.PosList = res.data;
    });
    this.integrationSer.getAllDispancery().subscribe((res: any) => {
      //  console.log("tis is all dispancry",res.data);
      var newData = [];
      var data = res.data;
      data.forEach(x => {
        if (x.businessType == 'dispensary') {
          newData.push(x);
        }
      });
      this.DispanceryList = newData;
      //  console.log("this is dis",this.DispanceryList);
    })
    this.spinner.hide();
  }

  update() {
    //this.object.store_name = this.dispensaryName;
    this.integrationSer.editStoreInfo(this.object).subscribe((res: any) => {
      if (res.success) {
        setTimeout(() => {
          this.toastr.success('Store data updated successfully!');
          // this._commanService.showAlert("Store data update successfully!",'alert-success');
          this.location.back();
          this.checkfuncation();
        }, 500);
        // let route = '/store_integration/list'
        // this._router.navigate([route]);    
      }
    })
  }
  checkfuncation() {
    let route = '/store/list'
    this._router.navigate([route]);
  }


}
