import { TestBed, inject } from '@angular/core/testing';

import { ReserveOrdersService } from './reserve-orders.service';

describe('StoreIntegrationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReserveOrdersService]
    });
  });

  it('should be created', inject([ReserveOrdersService], (service: ReserveOrdersService) => {
    expect(service).toBeTruthy();
  }));
});
