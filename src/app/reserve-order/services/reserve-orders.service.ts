import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
// import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { throwError } from 'rxjs';
import {
  HttpErrorResponse,
  HttpClient,
  HttpParams,
} from '@angular/common/http';
import { CommanService } from 'src/app/shared/comman.service';
import { map, catchError } from 'rxjs/operators';
// import tsConstants = require('./../../tsconstant');
// import { CommanService } from '../../shared/services/comman.service';

@Injectable()
export class ReserveOrdersService {
  private _host = environment.config.HOST;

  constructor(
    private http: HttpClient,
    private _commanService: CommanService
  ) {}

  getReservedOrders(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this.http
      .get(this._host + 'allreservedorderslist', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  get(id) {
    return this.http.get(this._host + 'reserveorders/' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  // getApiData(obj){
  //     let url = this._host+'/storeInfo?api_key='+obj.key+'&external_key='+obj.external_key+'&url='+obj.api_url+'&company_id='+obj.company_id+'&location_id='+obj.location_id+'&name='+obj.store_name+"&pos_name="+obj.pos_name
  //     let headers = this._commanService.getAuthorizationHeader();
  //     // return this.http.post(url, { headers: headers }).map((res:Response) => res.json())
  //      return this.http.post(this._host +'/storeInfo', obj, { headers: headers }).map((res:Response) => res.json())
  // }

  // get(orderId) {
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this.http.get(this._host +'/reserveorders/'+ orderId, { headers: headers }).map((res:Response) => res.json())
  // }

  // getAllStore(id:any,rowsOnPage, activePage, sortTrem , search = ''){
  //   var url;
  //     if(id == ''){
  //       let date =  new Date().getTime().toString();
  //     url = this._host+'/all_store?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&search='+search;
  //     }else{
  //       url = this._host+'/all_store?id='+id;
  //     }
  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this.http.get(url, { headers: headers }).map((res:Response) => res.json())
  // }

  // editStoreInfo(obj){
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this.http.post(this._host +'/updateStore', obj, { headers: headers }).map((res:Response) => res.json())
  // }

  // getAllPos() {
  // let url = this._host +'/getPosList';
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this.http.get(url, { headers: headers }).map((res:Response) => res.json())
  // }

  // getAllDispancery() {
  //   let url = this._host +'/getnormaldispensary';
  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this.http.get(url, { headers: headers }).map((res:Response) => res.json())
  //   }

  // getAllReservedOrders(rowsOnPage, activePage, sortTrem, search = '',) {
  //     let date =  new Date().getTime().toString();
  //     let headers = this._commanService.getAuthorizationHeader();
  //     let url = this._host +'/allreservedorderslist?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&search='+search;
  //     return this.http.get(url, { headers: headers }).map((res:Response) => res.json())
  // }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
