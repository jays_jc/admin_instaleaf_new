import { environment } from './../../../environments/environment';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { DriverService } from '../services/driver.service';
import { ReserveOrdersService } from '../services/reserve-orders.service';
import { CommanService } from 'src/app/shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  templateUrl: 'view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent {
  private _host = environment.config.HOST;

  public gst;
  public delivery_charge;
  public userID = '';
  public order: any = {
    shippingDetail: {},
    billingDetail: {},
  };
  public isLoading = false;
  public isPageLoading = true;
  public addEditDelete = false;
  public response: any;
  public groupByObject: any = [];

  constructor(
    private _route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _router: Router,
    private _reserveOrdersService: ReserveOrdersService,
    private _commanService: CommanService,

  ) {
    this._route.queryParams.subscribe((params) => {
      this.userID = params['id'];
      this.getDetail();
    });

    // this._commanService.getSettings().subscribe( res => {
    //     if(res.success) {
    //         this.gst = res.data[0].gst;
    //         this.delivery_charge = res.data[0].delivery_charge;
    //     } else {
    //         this._commanService.checkAccessToken(res.error);
    //     }
    // }, err => {
    //     this.isPageLoading = false;
    //     this._commanService.checkAccessToken(err);
    // });

    // let actions = this._commanService.getActions();
    // if(actions["type"] == 'SA' || actions['users']['addEditDelete']) this.addEditDelete = true;
  }

  getDetail() {
    this.spinner.show();
    this._reserveOrdersService.get(this.userID).subscribe(
      (res) => {
        if (res.success) {
          this.order = res.data;
          console.log('order detil', res.data);
          //  this.groupByObject= this.groupBy(this.order['order_detail'])
          //  this.groupByObject = Object.keys( this.groupByObject).map(key =>  this.groupByObject[key])
          this.isPageLoading = false;
          this.spinner.hide();
        } else {
          //    this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {
        this.spinner.hide();
        this.isPageLoading = false;
      }
    );
  }

  groupBy(arr) {
    let group = arr.reduce((r, a) => {
      r[a.product_id.supplier_id] = [...(r[a.product_id.supplier_id] || []), a];
      return r;
    }, {});

    return group;
  }
}
