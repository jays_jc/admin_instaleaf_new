import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { NG2DataTableModule } from "angular2-datatable-pagination";
// import { DriverService } from './services/driver.service';
// import { SharedModule } from '../shared/shared.module';
// import { CustomFormsModule } from 'ng2-validation';
// import { FlashMessagesModule } from 'ngx-flash-messages';
// import { OrdersRoutingModule } from './reserve-orders-routing.module';
// import {ReserveOrderModule} from './reserveorders-routing.module';
// import { NgxSpinnerService } from 'ngx-spinner';
import { ListComponent } from './list-component/list.component';
import { ViewComponent } from './view-component/view.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReserveOrderRoutingModule } from './reserve-orders-routing.module';
import { ReserveOrdersService } from './services/reserve-orders.service';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  imports: [
    CommonModule,
    ReserveOrderRoutingModule,
    NgxSpinnerModule,
    // NgxSpinnerService,
    //  NG2DataTableModule,
    //  CustomFormsModule,
    //  FlashMessagesModule,
    //  SharedModule,
    //  ImageUploadModule
    NgxDatatableModule,
  ],
  providers: [ReserveOrdersService],
  declarations: [ListComponent, ViewComponent],
})
export class ReserveOrderModule {}
