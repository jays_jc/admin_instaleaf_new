import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CommanService } from '../shared/comman.service';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService
  ) {}

  get(data) {
    // let headers = this._commanService.getAuthorizationHeader();
    return this._http.get(this._host + `dashboard?type=` + data).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // return this._http
    //   .get(this._host + '/dashboard?type=' + data)
    //   .map((res: Response) => res.json());
  }

  // getDashboard(data) {
  //   // let headers = this._commanService.getAuthorizationHeader();
  //   return this._http
  //     .get(this._host + '/dashboard', { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('Session expired . Please login again!');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    } else if (error.error.code == 301) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
