import { CommanService } from './../shared/comman.service';
import { DailyDataComponent } from './daily-data/daily-data.component';
import { WeeklyDataComponent } from './weekly-data/weekly-data.component';
import { YearlyDataComponent } from './yearly-data/yearly-data.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
// import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MonthlyDataComponent } from './monthly-data/monthly-data.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    DashboardComponent,
    MonthlyDataComponent,
    YearlyDataComponent,
    WeeklyDataComponent,
    DailyDataComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule
  ],
  providers: [CommanService],
})
export class DashboardModule { }
