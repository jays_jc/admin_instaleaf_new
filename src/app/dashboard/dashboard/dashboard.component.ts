import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public isChartLoading: boolean = true;
  public data = [];
  filterValue = 'monthly';
  public isPageLoading: boolean = false;

  constructor(private _router: Router, private _route: ActivatedRoute, private spinner: NgxSpinnerService) {
    
  }

  ngOnInit() {
    this.spinner.show();
    // this.spinner.hide();
  }

  selectFilter() { }

  // logout() {
  //   this._cookieService.removeAll();
  //   this._router.navigate(['/login']);
  // }

  // export(url) {
  //   this._commanService.downloadExportCSV('/' + url).subscribe(
  //     (res: any) => {
  //       const blob = new Blob([res], {
  //         type: 'application/octet-stream',
  //       });
  //       var link = document.createElement('a');
  //       link.href = window.URL.createObjectURL(blob);
  //       link.download = url + Date.now() + '.xlsx';
  //       link.click();
  //     },
  //     (error) => {
  //       console.log(error);
  //       // this.toastr.error(error);
  //     }
  //   );
  // }
}
