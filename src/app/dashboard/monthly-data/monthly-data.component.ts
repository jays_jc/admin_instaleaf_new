import { Component, OnInit, Input } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';
import { CommanService } from 'src/app/shared/comman.service';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-monthly-data',
  templateUrl: './monthly-data.component.html',
  styleUrls: ['./monthly-data.component.scss'],
})
export class MonthlyDataComponent implements OnInit {
  @Input() filterValue;
  public isChartLoading: boolean = true;
  public data = [];
  public isProgressStart: boolean = false;
  // filterValue = 'monthly';
  public lineChartType: string = 'line';
  public barChartType: string = 'bar';
  // lineChart
  // public lineChartData:Array<any> = [
  //     {data: [10,8,9,4,8,2,10,5,10,8,9,4,8,2,10,5], label: 'Doctor'},
  //     {data: [6,8,9,4,8,2,10,8,7,8,9,4,8,2,2,5], label: 'Dispensary'},
  //     {data: [2,1,9,8,8,2,7,8,2,7,8,2,7,8,2,7,8,7], label: 'User'}
  // ];
  //   public barChartData: Array<any> = [
  //     { data: [], fill: false, label: 'Doctor' },
  //     { data: [], fill: false, label: 'Dispensary' },
  //     { data: [], fill: false, label: 'Brand' },
  //     { data: [], fill: false, label: 'User' },
  //     { data: [], fill: false, label: 'Super Admin' },
  //     { data: [], fill: false, label: 'Driver' },
  //   ];
  public lineChartData: Array<any> = [
    { data: [], fill: false, label: 'Doctor' },
    { data: [], fill: false, label: 'Dispensary' },
    { data: [], fill: false, label: 'Brand' },
    { data: [], fill: false, label: 'User' },
    { data: [], fill: false, label: 'Super Admin' },
    { data: [], fill: false, label: 'Driver' },
  ];
  public isPageLoading: boolean = true;
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        { id: 'y-axis-1', type: 'linear', position: 'left' },
      ] /*ticks: {min: 0, max:100}*/,
    },
  };

  public favTab = true;
  public reviewTab = true;
  public salesTab = true;
  public reserveTab = true;
  public appTab = true;
  public userTab = true;

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  public barChartLabels: Array<any> = [];
  public barChartLegend = true;
  public barChartData: Array<any> = [
    { data: [], label: 'Web Users' },
    { data: [], label: 'App Users' },
  ];

  public barChartDownloadLabels = [];
  //   public barChartLegend = true;
  public barChartDownlodData = [
    { data: [], label: 'Android Downloads' },
    { data: [], label: 'IOS Downloads' },
  ];

  public pieChartLabels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
  public pieChartData = [120, 150, 180, 90];
  public pieChartType = 'pie';
  dataPointsAppUsers: any = [];
  dataPointsWebUsers: any = [];
  webUsers: any = [];
  appUsers: any = [];
  dateFormate = 'y';
  pipe = new DatePipe('en-US');
  dataPointsDownloads: any = [];
  androidDownloads: any = [];
  iosDownloads: any = [];
  public lineChartFavouriteData: Array<any> = [
    { data: [], label: 'Favourite Stores' },
    { data: [], label: 'Favourite Products' },
  ];
  public lineChartFavouriteLabels: Array<any> = [];
  totalFavoriteStore: any = [];
  totalProductfavorites: any = [];
  //   dataPointFavourites: any = [];
  public lineChartReviewData: Array<any> = [
    { data: [], label: 'Product Reviews' },
    { data: [], label: 'Store Reviews' },
  ];
  public lineChartReviewLabels: Array<any> = [];
  productReviews: any = [];
  storeReviews: any = [];
  //   public lineChartSalesData: Array<any> = [{ data: [], label: 'Sales' }];
  //   public lineChartSalesLabels: Array<any> = [];
  //   sales: any = [];
  public lineChartReservedData: Array<any> = [
    { data: [], label: 'Reserved Orders' },
  ];
  public lineChartReservedLabels: Array<any> = [];
  reservedOrder: any = [];
  //   dataPointReviews: any = [];
  public lineChartReservedColors = [
    {
      // grey
      backgroundColor: 'rgba(0,255,0,0.3)',
      borderColor: 'green',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)',
    },
  ];
  normalProductSale: any = [];
  reservedProductSale: any = [];
  subscriptionSale: any = [];
  public lineChartSalesData: Array<any> = [
    { data: [], label: 'Normal Products Sale' },
    { data: [], label: 'Reserved Products Sale' },
    { data: [], label: 'Subscription Sale' },
  ];
  public lineChartSalesLabels: Array<any> = [];
  public showData: boolean = false;

  //   @ViewChild(BaseChartDirective) public chart: BaseChartDirective;
  constructor(
    private _dashboardService: DashboardService,
    // private _fullLayoutService: FullLayoutService,
    private _commanService: CommanService,
    // private _cookieService: CookieService,
    private _router: Router,
    private _route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    // this._route.queryParams.subscribe((params) => {
    //   if (params.type) {
    //     this.filterValue = params.type;
    //   }
    // });
    // console.log(this.filterValue);
  }

  ngOnInit() {
    // console.log(this.filter);
    // this.filterValue = this.filter;
    //   this.getMyProgress();
    this.getDashBoardData();
  }

  getMyProgress() {
    this._dashboardService.get(this.filterValue).subscribe(
      (res) => {
        this.isPageLoading = false;
        if (res.success) {
          this.data = res.data;
          // this.data.reverse();
          this.data.forEach((obj) => {
            this.isProgressStart = true;
            //     let date = new Date(obj.createdAt);
            //     let month = date.getMonth()+1;
            //     let label = month + '/' + date.getDate() + '/' + date.getFullYear();
            this.lineChartLabels.push(obj._id);
            obj.roles.forEach((record) => {
              if (record.role == 'DR') {
                this.lineChartData[0].data.push(record.count);
              } else if (record.role == 'D') {
                this.lineChartData[1].data.push(record.count);
              } else if (record.role == 'B') {
                this.lineChartData[2].data.push(record.count);
              } else if (record.role == 'U') {
                this.lineChartData[3].data.push(record.count);
              } else if (record.role == 'SA') {
                this.lineChartData[4].data.push(record.count);
              } else if (record.role == 'DRIVER') {
                this.lineChartData[5].data.push(record.count);
              }
            });
          });
          this.isChartLoading = false;
        } else {
          this.isChartLoading = false;
        }
      },
      (err) => {
        this.isPageLoading = false;
        this.isChartLoading = false;
      }
    );
  }

  chartClicked(event) { }

  getDashBoardData() {
    this.spinner.show();
    // this._commanService.loader("show");
    this._dashboardService.get(this.filterValue).subscribe((res: any) => {
      console.log(res);
      this.isPageLoading = false;
      if (res.error && res.error.code == 401) {
        // this._commanService.showAlert(res.error.message, 'alert-danger');
        this.showData = false;
        this.logout();
      } else {
        this.showData = true;
        //     this._commanService.loader("hide");
        //     (this.event = res.totalEvent),
        this.totalProductfavorites = res.totalProductfavorites.response;
        // this.totalProductfavorites = res.totalProductfavorites.totalresults;
        // this.totalFavoriteStore = res.totalFavoriteStore.totalresults;

        // this.normalProductSale = res.totalNormalProductSales.totalresults;
        // this.reservedProductSale = res.totalReservedProductSale.totalresults;
        this.subscriptionSale = res.totalSubscriptionSales.response;
        //       (this.follow = res.totalFollow.totalresults),
        this.reservedOrder = res.totalReserveOrder.totalresults;
        this.androidDownloads = res.totalAppDownloads.response;
        // this.iosDownloads = res.totalIOSAppDownloads.totalresults;
        this.webUsers = res.totalWebUsers.response;
        // this.appUsers = res.totalAppUsers.totalresults;
        this.productReviews = res.totalProductReviews.response;
        // this.storeReviews = res.totalStoreReview.totalresults;
        //     this.soldOutProducts = res.totalSoldOut;
        //     let catSaleProduct = res.totalProductCat.totalEvents;
        //     this.dataTable = res.totalViewProductReview.totalEvents;

        //     let websiteviews = res.totalViews.totalEvents;
        //     let catCommissionSaleProduct = res.totalProductCatComission.totalEvents;

        //     websiteviews.forEach((element) => {
        //       if (element.id.type == "home_page") this.homeView = element.sum;

        //       if (element.id.type == "blog_page") this.blogsView = element.sum;

        //       if (element.id.type == "shop_page") this.shopView = element.sum;

        //       if (element.id.type == "provider_detail_page")
        //         this.detailView = element.sum;
        //     });
        // this.barChartData = [
        //   { data: [], label: 'Web Users' },
        //   { data: [], label: 'App Users' },
        // ];
        // this.barChartLabels = [];
        // //   this.barChartDownlodData = [
        // //     { data: [], label: 'App Downloads' },
        // //     // { data: [], label: 'App Users' },
        // //   ];
        // this.lineChartReviewData = [
        //   { data: [], label: 'Product Reviews' },
        //   { data: [], label: 'Store Reviews' },
        // ];

        // this.lineChartFavouriteData = [
        //   { data: [], label: 'Favourite Stores' },
        //   { data: [], label: 'Favourite Products' },
        // ];
        // this.lineChartFavouriteLabels = [];
        // this.lineChartSalesData = [
        //   { data: [], label: 'Normal Products Sale' },
        //   { data: [], label: 'Reserved Products Sale' },
        //   { data: [], label: 'Subscription Sale' },
        // ];

        // //   this.barChartDownloadLabels = [];
        // //   this.dataPointsDownloads = [];
        // //   this.dataPointsAppUsers = [];
        // //   this.dataPointsWebUsers = [];
        // //   console.log(this.barChartLabels);
        // //   this.dataPointFavourites = [];
        // //   this.dataPointReviews = [];
        // //   this.lineChartReviewData = [{ data: [], label: 'Reviews' }];
        // this.lineChartReviewLabels = [];
        // //   this.lineChartSalesData = [{ data: [], label: 'Sales' }];
        // this.lineChartSalesLabels = [];
        // this.lineChartReservedData = [{ data: [], label: 'Reserved Order' }];
        // this.lineChartReservedLabels = [];
        //     this.dataPointsTicketPayment = [];
        //     this.dataPointsBlogs = [];
        //     this.dataPointsFollow = [];
        //     this.dataPointsProducts = [];
        //     this.dataPointsReviews = [];
        //     this.dataPointsCatProduct = [];
        //   this.barChartLabels.length = 0;
        this.webUsers.forEach((element) => {
          if (element.totalWebUser) {
            this.barChartData[0].data.push(element.totalWebUser);
          }
          if (element.totalAppUsers) {
            this.barChartData[1].data.push(element.totalAppUsers);
          }
          if (
            !this.barChartLabels.includes(
              this.getMonth(element.month) + ' ' + element.year
            )
          ) {
            this.barChartLabels.push(
              this.getMonth(element.month) + ' ' + element.year
            );
          }
          //   let date = element.id.day
          //     ? element.id.date_year +
          //       '/' +
          //       element.id.date_month +
          //       '/' +
          //       element.id.day
          //     : element.id.date_month
          //     ? element.id.date_year + '/' + element.id.date_month + '/01'
          //     : element.id.date_year + '/01/01';
          //   this.dateFormate = element.id.day
          //     ? 'MMM d , y'
          //     : element.id.date_month
          //     ? 'MMM, y'
          //     : 'y';
          //   if (element.id.date_year)
          //     this.barChartData[0].data.push(
          //       // label: this.pipe.transform(date, this.dateFormate),
          //       element.sum
          //     );

          //   this.barChartLabels.push(this.pipe.transform(date, this.dateFormate));
          // });
          // console.log(this.barChartLabels);
          // console.log(this.webUsers);
          // this.appUsers.forEach((element) => {
          //   let date = element.id.day
          //     ? element.id.date_year +
          //       '/' +
          //       element.id.date_month +
          //       '/' +
          //       element.id.day
          //     : element.id.date_month
          //     ? element.id.date_year + '/' + element.id.date_month + '/01'
          //     : element.id.date_year + '/01/01';
          //   if (element.id.date_year)
          //     this.barChartData[1].data.push(
          //       // label: this.pipe.transform(date, this.dateFormate),
          //       element.sum
          //     );
          //   if (
          //     !this.barChartLabels.includes(
          //       this.pipe.transform(date, this.dateFormate)
          //     )
          //   ) {
          //     this.barChartLabels.push(
          //       this.pipe.transform(date, this.dateFormate)
          //     );
          //   }
        });
        // console.log(this.barChartData);
        // console.log(this.barChartLabels);
        this.isPageLoading = false;
        this.totalProductfavorites.forEach((element) => {
          if (element.totalFavProduct) {
            this.lineChartFavouriteData[1].data.push(element.totalFavProduct);
          }
          if (element.totalFavStores) {
            this.lineChartFavouriteData[0].data.push(element.totalFavStores);
          }
          if (
            !this.lineChartFavouriteLabels.includes(
              this.getMonth(element.month) + ' ' + element.year
            )
          ) {
            this.lineChartFavouriteLabels.push(
              this.getMonth(element.month) + ' ' + element.year
            );
          }
          // let date = element.id.day
          //   ? element.id.date_year +
          //     '/' +
          //     element.id.date_month +
          //     '/' +
          //     element.id.day
          //   : element.id.date_month
          //   ? element.id.date_year + '/' + element.id.date_month + '/01'
          //   : element.id.date_year + '/01/01';
          // if (element.id.date_year)
          //   this.lineChartFavouriteData[1].data.push(
          //     // label: this.pipe.transform(date, this.dateFormate),
          //     element.sum
          //   );
          // if (
          //   !this.lineChartFavouriteLabels.includes(
          //     this.pipe.transform(date, this.dateFormate)
          //   )
          // ) {
          //   this.lineChartFavouriteLabels.push(
          //     this.pipe.transform(date, this.dateFormate)
          //   );
          // }
          // this.lineChartFavouriteLabels.sort((a, b) => a - b);
          // console.log(this.lineChartFavouriteLabels);
        });
        // this.totalFavoriteStore.forEach((element) => {
        //   let date = element.id.day
        //     ? element.id.date_year +
        //       '/' +
        //       element.id.date_month +
        //       '/' +
        //       element.id.day
        //     : element.id.date_month
        //     ? element.id.date_year + '/' + element.id.date_month + '/01'
        //     : element.id.date_year + '/01/01';
        //   if (element.id.date_year)
        //     this.lineChartFavouriteData[0].data.push(
        //       // label: this.pipe.transform(date, this.dateFormate),
        //       element.sum
        //     );
        //   if (
        //     !this.lineChartFavouriteLabels.includes(
        //       this.pipe.transform(date, this.dateFormate)
        //     )
        //   ) {
        //     this.lineChartFavouriteLabels.push(
        //       this.pipe.transform(date, this.dateFormate)
        //     );
        //   }

        //   // console.log(this.lineChartFavouriteLabels);
        // });

        this.androidDownloads.forEach((element) => {
          if (element.totalAndriodDownload) {
            this.barChartDownlodData[0].data.push(element.totalAndriodDownload);
          }
          if (element.totalIosDownload) {
            this.barChartDownlodData[1].data.push(element.totalIosDownload);
          }
          if (
            !this.barChartDownloadLabels.includes(
              this.getMonth(element.month) + ' ' + element.year
            )
          ) {
            this.barChartDownloadLabels.push(
              this.getMonth(element.month) + ' ' + element.year
            );
          }
          // let date = element.id.day
          //   ? element.id.date_year +
          //     '/' +
          //     element.id.date_month +
          //     '/' +
          //     element.id.day
          //   : element.id.date_month
          //   ? element.id.date_year + '/' + element.id.date_month + '/01'
          //   : element.id.date_year + '/01/01';
          // if (element.id.date_year)
          //   this.barChartDownlodData[0].data.push(
          //     // label: this.pipe.transform(date, this.dateFormate),
          //     element.sum
          //   );
          // this.barChartDownloadLabels.push(
          //   this.pipe.transform(date, this.dateFormate)
          // );
        });
        // this.iosDownloads.forEach((element) => {
        //   let date = element.id.day
        //     ? element.id.date_year +
        //       '/' +
        //       element.id.date_month +
        //       '/' +
        //       element.id.day
        //     : element.id.date_month
        //     ? element.id.date_year + '/' + element.id.date_month + '/01'
        //     : element.id.date_year + '/01/01';
        //   if (element.id.date_year)
        //     this.barChartDownlodData[1].data.push(
        //       // label: this.pipe.transform(date, this.dateFormate),
        //       element.sum
        //     );
        //   if (
        //     !this.barChartDownloadLabels.includes(
        //       this.pipe.transform(date, this.dateFormate)
        //     )
        //   ) {
        //     this.barChartDownloadLabels.push(
        //       this.pipe.transform(date, this.dateFormate)
        //     );
        //   }
        // });

        // this.normalProductSale.forEach((element) => {
        //   let date = element.id.day
        //     ? element.id.date_year +
        //       '/' +
        //       element.id.date_month +
        //       '/' +
        //       element.id.day
        //     : element.id.date_month
        //     ? element.id.date_year + '/' + element.id.date_month + '/01'
        //     : element.id.date_year + '/01/01';
        //   if (element.id.date_year)
        //     this.lineChartSalesData[0].data.push(
        //       // label: this.pipe.transform(date, this.dateFormate),
        //       element.sum
        //     );
        //   this.lineChartSalesLabels.push(
        //     this.pipe.transform(date, this.dateFormate)
        //   );
        // });
        // this.reservedProductSale.forEach((element) => {
        //   let date = element.id.day
        //     ? element.id.date_year +
        //       '/' +
        //       element.id.date_month +
        //       '/' +
        //       element.id.day
        //     : element.id.date_month
        //     ? element.id.date_year + '/' + element.id.date_month + '/01'
        //     : element.id.date_year + '/01/01';
        //   if (element.id.date_year)
        //     this.lineChartSalesData[1].data.push(
        //       // label: this.pipe.transform(date, this.dateFormate),
        //       element.sum
        //     );
        //   if (
        //     !this.lineChartSalesLabels.includes(
        //       this.pipe.transform(date, this.dateFormate)
        //     )
        //   ) {
        //     this.lineChartSalesLabels.push(
        //       this.pipe.transform(date, this.dateFormate)
        //     );
        //   }
        // });

        this.subscriptionSale.forEach((element) => {
          if (element.totalNormalProductSale) {
            this.lineChartSalesData[0].data.push(
              element.totalNormalProductSale
            );
          }
          if (element.totalReserverProductSale) {
            this.lineChartSalesData[1].data.push(
              element.totalReserverProductSale
            );
          }
          if (element.totalSubscriptionSale) {
            this.lineChartSalesData[2].data.push(element.totalSubscriptionSale);
          }
          if (
            !this.lineChartSalesLabels.includes(
              this.getMonth(element.month) + ' ' + element.year
            )
          ) {
            this.lineChartSalesLabels.push(
              this.getMonth(element.month) + ' ' + element.year
              // this.getMonth(element.month) + ' ' + element.year
            );
          }
          // let date = element.id.day
          //   ? element.id.date_year +
          //     '/' +
          //     element.id.date_month +
          //     '/' +
          //     element.id.day
          //   : element.id.date_month
          //   ? element.id.date_year + '/' + element.id.date_month + '/01'
          //   : element.id.date_year + '/01/01';
          // if (element.id.date_year)
          //   this.lineChartSalesData[2].data.push(
          //     // label: this.pipe.transform(date, this.dateFormate),
          //     element.sum
          //   );
          // if (
          //   !this.lineChartSalesLabels.includes(
          //     this.pipe.transform(date, this.dateFormate)
          //   )
          // ) {
          //   this.lineChartSalesLabels.push(
          //     this.pipe.transform(date, this.dateFormate)
          //   );
          // }
        });
        //     // console.log(this.dataPointsBlogs,'-----------------')

        this.reservedOrder.forEach((element) => {
          // element.id.day
          //   ? element.id.date_year +
          //     '/' +
          //     element.id.date_month +
          //     '/' +
          //     element.id.day
          //   : element.id.date_month
          //   ? element.id.date_year + '/' + element.id.date_month + '/01'
          //   : element.id.date_year + '/01/01';
          if (element.id.date_year)
            this.lineChartReservedData[0].data.push(
              // label: this.pipe.transform(date, this.dateFormate),
              element.sum
            );
          if (
            !this.lineChartReservedLabels.includes(
              this.getMonth(element.month) + ' ' + element.year
            )
          ) {
            this.lineChartReservedLabels.push(
              this.getMonth(element.month) + ' ' + element.year
            );
          }
        });

        //     this.product.forEach((element) => {
        //       let date = element.id.day
        //         ? element.id.date_year +
        //           "/" +
        //           element.id.date_month +
        //           "/" +
        //           element.id.day
        //         : element.id.date_month
        //         ? element.id.date_year + "/" + element.id.date_month + "/01"
        //         : element.id.date_year + "/01/01";
        //       if (element.id.date_year)
        //         this.dataPointsProducts.push({
        //           label: this.pipe.transform(date, this.dateFormate),
        //           y: element.sum,
        //         });
        //     });

        this.productReviews.forEach((element) => {
          if (element.totalProductReviews) {
            this.lineChartReviewData[0].data.push(element.totalProductReviews);
          }
          if (element.totalStoresReview) {
            this.lineChartReviewData[1].data.push(element.totalStoresReview);
          }
          if (
            !this.lineChartReviewLabels.includes(
              this.getMonth(element.month) + ' ' + element.year
            )
          ) {
            this.lineChartReviewLabels.push(
              this.getMonth(element.month) + ' ' + element.year
              // this.getMonth(element.month) + ' ' + element.year
            );
          }
          // let date = element.id.day
          //   ? element.id.date_year +
          //     '/' +
          //     element.id.date_month +
          //     '/' +
          //     element.id.day
          //   : element.id.date_month
          //   ? element.id.date_year + '/' + element.id.date_month + '/01'
          //   : element.id.date_year + '/01/01';
          // if (element.id.date_year)
          //   this.lineChartReviewData[0].data.push(
          //     // label: this.pipe.transform(date, this.dateFormate),
          //     element.sum
          //   );
          // this.lineChartReviewLabels.push(
          //   this.pipe.transform(date, this.dateFormate)
          // );
        });
        // this.storeReviews.forEach((element) => {
        //   let date = element.id.day
        //     ? element.id.date_year +
        //       '/' +
        //       element.id.date_month +
        //       '/' +
        //       element.id.day
        //     : element.id.date_month
        //     ? element.id.date_year + '/' + element.id.date_month + '/01'
        //     : element.id.date_year + '/01/01';
        //   if (element.id.date_year)
        //     this.lineChartReviewData[1].data.push(
        //       // label: this.pipe.transform(date, this.dateFormate),
        //       element.sum
        //     );
        //   if (
        //     !this.lineChartReviewLabels.includes(
        //       this.pipe.transform(date, this.dateFormate)
        //     )
        //   ) {
        //     this.lineChartReviewLabels.push(
        //       this.pipe.transform(date, this.dateFormate)
        //     );
        //   }
        // });

        //     catSaleProduct.forEach((element) => {
        //       let date = element.id.day
        //         ? element.id.date_year +
        //           "/" +
        //           element.id.date_month +
        //           "/" +
        //           element.id.day
        //         : element.id.date_month
        //         ? element.id.date_year + "/" + element.id.date_month + "/01"
        //         : element.id.date_year + "/01/01";
        //       if (element.id.date_year) {
        //         let name = element.categoryData.filter(
        //           (res) => res._id == element.category
        //         );
        //         // let concat_name =
        //         // console.log(name,'-----------------')
        //         if (name.length > 0)
        //           this.dataPointsCatProduct.push({
        //             label:
        //               name[0].name +
        //               " (" +
        //               this.pipe.transform(date, this.dateFormate) +
        //               ")",
        //             y: element.sum,
        //           });
        //       }
        //     });

        //     catCommissionSaleProduct.forEach((element) => {
        //       let date = element.id.day
        //         ? element.id.date_year +
        //           "/" +
        //           element.id.date_month +
        //           "/" +
        //           element.id.day
        //         : element.id.date_month
        //         ? element.id.date_year + "/" + element.id.date_month + "/01"
        //         : element.id.date_year + "/01/01";
        //       if (element.id.date_year) {
        //         let name = element.categoryData.filter(
        //           (res) => res._id == element.category
        //         );
        //         // let concat_name =
        //         this.dataPointsCommCatProduct.push({
        //           label:
        //             name[0].name +
        //             " (" +
        //             this.pipe.transform(date, this.dateFormate) +
        //             ")",
        //           y: element.sum,
        //         });
        //       }
        //     });

        //     this.showDashboardData();
        //   }
        // this._commanService.loader("hide");
        // this.refresh_chart();
        // setTimeout(() => {
        //   this.chart.chart.update();
        // }, 100);
        // this.chart.ngOnChanges({} as SimpleChanges);
      }
      this.spinner.hide();
    },
      error => {
        this.spinner.hide()
      }
    );
  }

  //   refresh_chart() {
  //     setTimeout(() => {
  //       console.log(this.chart);
  //       //   this.chart.chart.update();
  //       //   console.log(this.chart);
  //       //   this.chart.forEach((child) => {
  //       //     child.chart.update();
  //       //   });
  //       //   console.log(this.lineChartFavouriteData);
  //       //   console.log(this.lineChartFavouriteLabels);
  //       //   if (this.chart && this.chart.chart && this.chart.chart.config) {
  //       //     console.log(this.chart.chart);
  //       //     this.chart.chart.config.data.labels = this.lineChartFavouriteLabels;
  //       //     this.chart.chart.config.data.datasets = this.lineChartFavouriteData;
  //       //     this.chart.chart.update();
  //       //   }
  //     });
  //   }

  // selectFilter() {
  //   // this.ngOnInit();
  //   // this.getDashBoardData();
  //   this.changeData();
  // }

  // ngOnChanges(changes: SimpleChanges): void {
  //   //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
  //   //Add '${implements OnChanges}' to the class.
  //   console.log(changes);
  // }
  // changeData() {
  //   console.log('called');
  //   let currentUrl = this._router.url;
  //   this._router
  //     .navigateByUrl('/dashboard', {
  //       queryParams: { type: this.filterValue },
  //       skipLocationChange: true,
  //     })
  //     .then(() => {
  //       this._router.navigate(['/dashboard'], {
  //         queryParams: { type: this.filterValue },
  //         skipLocationChange: true,
  //       });
  //     });
  //   // this._router.navigate(['/dashboard'], {
  //   //   relativeTo: this._route,
  //   //   queryParams: {
  //   //     type: this.filterValue,
  //   //   },
  //   //   //   queryParamsHandling: "merge",
  //   //   // preserve the existing query params in the route
  //   //   skipLocationChange: false,
  //   //   // do not trigger navigation
  //   // });
  //   // setTimeout(() => {
  //   //   let path = '/dashboard?type=' + this.filterValue;
  //   //   this._router.navigate([path]);
  //   // }, 100);
  // }

  logout() {
    // this._cookieService.removeAll();
    // this._router.navigate(['/login']);
  }

  export(url) {
    this._commanService.downloadExportCSV(url).subscribe(
      res => {
        const blob = new Blob([res], { type: 'application/octet-stream' });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = url+Date.now()+".xlsx";
        link.click();
        // const blob = new Blob([], {
        //   type: 'text/csv',
        // });
        // var link = document.createElement('a');
        // link.href = window.URL.createObjectURL(blob);
        // link.download = url + Date.now() + '.xlsx';
        // link.click();
      },
      (error) => {
        console.log(error);
        // this.toastr.error(error);
      }
    );
  }

  getMonth(value) {
    const monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    return monthNames[value - 1];
  }
}
