import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearlyDataComponent } from './yearly-data.component';

describe('YearlyDataComponent', () => {
  let component: YearlyDataComponent;
  let fixture: ComponentFixture<YearlyDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearlyDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearlyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
