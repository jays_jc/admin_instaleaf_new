import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { CommanService } from 'src/app/shared/comman.service';
import { ImageResult } from 'ng2-imageupload';

@Component({
  templateUrl: 'addupdate-user.component.html',
  styleUrls: ['./addupdate-user.component.scss']
})
export class AddUpdateUserComponent {
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  private _host = environment.config.HOST;

  public user = {
    status: 'active',
    username1: '',
    email: '',
    address: '',
    mobile: '',
    city: '',
    pincode: '',
    Type: "U",
    roles: "U",
    isVerified: "Y",
    domain: 'web'

  };
  public isLoading = false;
  public isPageLoading = true;
  public userID: any;

  //public city = '';
  public cities = [];

  public errMessage = '';

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _userService: UserService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private location: Location
    // private _dialogService: DialogService
  ) {
    this.userID = _activateRouter.snapshot.params['id'];
    this.spinner.show();
    if (this.userID) {
      this._userService.get(this.userID).subscribe(res => {
        if (res.success) {
          this.user = res.data;
          if (!this.user["email"]) this.user["email"] = this.user["username"];
          this.spinner.hide();
          // this.isPageLoading = false;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isPageLoading = false;
      });
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }

    this.allCities()

    /*setTimeout(()=>{
        this._userService.getStates().subscribe( res => { 
            this.states = res.data;   
            if( this.userID ) this.setDistrict();
        },err => {});
    },1500); */
  }

  /*If useID exist then will update existing user otherwise will add new user*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    if (this.userID) {
      this.user["username"] = this.user["email"];
      this._userService.update(this.user).subscribe(res => {
        if (res.success) {
          this.spinner.hide();
          this.toastr.success('Record Updated successfully.');
          // this.isLoading = false;
          // this._commanService.showAlert(tsMessages.RECORD_UPDATED_SUCCESSFULLY,'alert-success');
          this.location.back();
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      })
    } else {
      this.user["username"] = this.user["email"];
      this._userService.add(this.user).subscribe(res => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
          // this._router.navigate(['/user/list']);
          this.location.back();
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }

  trim(key) {
    if (this.user[key] && this.user[key][0] == ' ') this.user[key] = this.user[key].trim();
  }

  // // Use to View Image Prompt
  // viewImage(imageUrl) {
  //     this._dialogService.addDialog(ViewUserImageComponent, {
  //       imageUrl:imageUrl
  //     }).subscribe((res)=>{ });
  // }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'users'
    }
    
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(res => {
      
      // this.isLoading = false;
      if (res.success) {
        this.user['image'] = res.data.fullPath;
      } else {
        this.toastr.error(res.error.message);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
      this.myInputVariable.nativeElement.value = "";
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  allCities() {
    this._commanService.allCities().subscribe(res => {
      if (res.success) {
        let data = res.data.data
        for (var i = 0; i < data.length; ++i) {
          // code...
          this.cities.push(data[i].name);
        }
      } else {
        this.toastr.error(res.error);
        // this._commanService.checkAccessToken(res.error);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }
}
