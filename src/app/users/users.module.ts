import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListUserComponent } from './list-component/list-user.component';
import { AddUpdateUserComponent } from './addupdate-component/addupdate-user.component';
import { ViewUserComponent } from './view-component/view-user.component';
import { UsersRoutingModule } from './users-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
    imports: [
      	UsersRoutingModule,
      	CommonModule,
        NgxDatatableModule,
        NgxSpinnerModule,
        ImageUploadModule
    ],
    declarations: [
        ListUserComponent,
        AddUpdateUserComponent,
        ViewUserComponent,
    ],

})
export class UsersModule { }