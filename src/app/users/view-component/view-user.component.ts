import { environment } from './../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
    templateUrl: 'view-user.component.html',
    styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent {
    private _host = environment.config.HOST;
    public ID = '';
    public object = {};
    public addEditDelete: boolean = false;
    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _commanService: CommanService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService,
        private _userService: UserService,
        ) { 
            let actions = this._commanService.getActions();
            if (actions['type'] == 'SA' || actions['crops']['addEditDelete'])
              this.addEditDelete = true;
        
            this.ID = _route.snapshot.params['id'];
            this.getDetail();

    }
    getDetail() {
        this.spinner.show();
        this._userService.get(this.ID).subscribe(
          (res) => {
            this.spinner.hide();
            // this.isPageLoading = false;
            if (res.success) {
              this.object = res.data;
            } else {
              // this._commanService.checkAccessToken(res.error);
            }
          },
          (err) => {
            this.spinner.hide();
            // this.isPageLoading = false;
          }
        );
      }

    route(ID, path) {
        let route = '/user/' + path + '/' + ID;
        this._router.navigate([route]);
      }

}
