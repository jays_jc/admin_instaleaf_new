import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../services/user.service';
import { CommanService } from './../../shared/comman.service';
@Component({
  selector: 'app-users',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string; roles: string } = {
    page: 0,
    count: 25,
    search: '',
    sortBy: 'createdAt desc',
    roles: ''
  };
  categoryList: any = [];
  private _path;
  response: any;
  public addEditDelete: boolean = false;
  public userroles = '';
  totalItems: any;
  public constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private _userService: UserService,
    private _commanService: CommanService,
  ) {
    let actions = this._commanService.getActions();
    if (actions["type"] == 'SA' || actions['users']['addEditDelete']) this.addEditDelete = true;

  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();

  }
  /*Get all Users */
  getCategory() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._userService
      .getAllCategory(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].users.map((data) => {
            return {
              id: data.id,
              username1: data.username1,
              email: data.email ? data.email : data.username,
              mobile: data.mobile ? data.mobile : '-',
              city: data.city ? data.city : '-',
              totalReviews: data.totalReviews,
              totalFavourite: data.totalFavourite,
              status: data.status,
              date_verified: data.date_verified,
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  setPage(e) {
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/user/list/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  route(cropID, path) {
    let route = '/user/' + path + '/' + cropID;
    this._router.navigate([route]);
  }

  viewUser(userID) {
    let route = '/user/list/' + userID;
    this._router.navigate([route]);
  }

  editUser(userID) {
    let route = '/user/edit/' + userID;
    this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, {
      page: 1,
      search: this.filters.search,
      roles: this.filters.roles
    });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  changeStatus(user) {
    let status = '';
    let message = 'Do you want to Activate User?';
    if (user.status == 'active') {
      status = "deactive";
      message = 'Do you want to Deactivate User?';
    } else {
      status = "active";
    }

    if (confirm(message)) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.changeStatus(user.id, 'users', status).subscribe(res => {
        if (res.success) {
          this.response = res;
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1
          // }
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
          /* reload page. */
          this.getCategory();
        } else {
          this.spinner.hide();
          this.toastr.error(res.error.message);
          // this.isLoading    = false;
          // this._commanService.showAlert(res.error.message,'alert-danger');
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }



}
