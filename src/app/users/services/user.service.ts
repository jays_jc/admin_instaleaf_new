import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators/map';
import { throwError } from 'rxjs';
@Injectable()
export class UserService {

  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _CommanService: CommanService
  ) {

  }

  /*Use to fetch all Inputs*/
  getAllCategory(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'user', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );

  }

  /*Use to add new user*/
  add(user) {
    return this._http.post(this._host + 'user', user).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );

    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.post(this._host +'/user', user, { headers: headers }).map((res:Response) => res.json())
  }


  /*Use to update user detail with there ID*/
  update(user) {
    return this._http.put(this._host + 'user/' + user.id, user).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/user/'+ user.id, user, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to fetch all States*/
  getStates() {
    return this._http.get(this._host + 'states?sort=stateName').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/states?sort=stateName', { headers: headers }).map((res:Response) => res.json())
  }


  get(ID) {
    return this._http.get(this._host + 'user/' + ID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/productdetailview/'+ ID, { headers: headers }).map((res:Response) => res.json())
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }



}
