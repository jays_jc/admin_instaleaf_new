import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListUserComponent } from './list-component/list-user.component';
import { AddUpdateUserComponent } from './addupdate-component/addupdate-user.component';
import { ViewUserComponent } from './view-component/view-user.component';
import { UserService } from './services/user.service';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Users'
    },
    children: [
      {
        path: '',
        component: ListUserComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'list',
        component: ListUserComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'add',
        component: AddUpdateUserComponent,
        data: {
          title: 'Add User'
        }
      },
      {
        path: 'list/:id',
        component: ViewUserComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'edit/:id',
        component: AddUpdateUserComponent,
        data: {
          title: 'Edit User'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  providers: [
    UserService
  ],
  exports: [
    RouterModule,
    FormsModule,
  ]
})
export class UsersRoutingModule {}
