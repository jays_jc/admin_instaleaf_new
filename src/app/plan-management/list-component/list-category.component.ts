import { CommanService } from 'src/app/shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
})
export class ListCategoryComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
  };
  categoryList: any = [];
  public addEditDelete: boolean = false;
  totalItems: any;
  public constructor(
    private _router: Router,
    private _categoryService: CategoryService,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();
  }
  /*Get all Users */
  getCategory(): void {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._categoryService
      .getAllCategory(this.filters)
      .subscribe(
        (response) => {
          // console.log('data', response.data);
          if (!response.success) {
            this.categoryList = [];
            // this.isLoading = false;
          } else {
            this.totalItems = response.data.total;
            this.categoryList = response['data'].category.map((data) => {
              return {
                id: data.id,
                name: data.name,
                createdAt: data.createdAt,
              };
            });
            // this.isLoading = false;
            this.spinner.hide();
          }
        },
        (error) => {
          this.spinner.hide();
        }
      );
  }

  edit(ID) {
    let route = '/plan-management/edit/' + ID;
    this._router.navigate([route]);
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/plan-management/list/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }
}
