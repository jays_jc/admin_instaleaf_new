import { Component, ChangeDetectorRef } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';
import { CookieService } from 'ngx-cookie';
import { ToastrService } from 'ngx-toastr';

import { NgxSpinnerService } from 'ngx-spinner';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate-category.component.scss'],
})
export class AddUpdateCategoryComponent {
  public category = {
    name: '',
  };

  public isLoading = false;
  public isPageLoading = true;
  public categoryID: any;
  public response: any;
  public type;
  public errMessage = '';
  public ParentCategories = [];

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    private toastr: ToastrService,
    // private _flashMessagesService: FlashMessagesService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private changeDetectorRef: ChangeDetectorRef,
    private spinner: NgxSpinnerService
  ) {
    this.categoryID = _activateRouter.snapshot.params['id'];

    if (this.categoryID) {
      this._catgService.get(this.categoryID).subscribe(
        (res) => {
          this.isPageLoading = false;
          if (res.success) {
            this.category = res.data[0];
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.isPageLoading = false;
          // this._commanService.checkAccessToken(err);
        }
      );
    } else {
      this.isPageLoading = false;
    }
  }

  ngOnInit(): void {
    // this.showDangerAlert();
  }

  /*If categoryID exist then will update existing category otherwise will add new category*/
  save() {
    this.spinner.show();
    let data = JSON.parse(JSON.stringify(this.category));
    if (this.categoryID) {
      this._catgService.update(data).subscribe(
        (res) => {
          if (res.success) {
            this.response = res;
            this._cookieService.put('categoryAlert', res.data.message);
            this._router.navigate(['/plan-management/list']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    } else {
      this._catgService.add(data).subscribe(
        (res) => {
          // this.isLoading = false;
          if (res.success) {
            this.response = res;
            this._cookieService.put('categoryAlert', res.message);
            this.toastr.success(res.message);
            this._router.navigate(['/plan-management/list']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  // showDangerAlert(): void {

  //     let alertMessage = this._cookieService.get('categoryExistAlert');
  //     if( alertMessage ) {
  //         this._flashMessagesService.show( alertMessage, {
  //             classes: ['alert', 'alert-danger'],
  //             timeout: 3000,
  //         });
  //         this._cookieService.remove('categoryExistAlert');
  //     }
  // }

  trim(key) {
    if (this.category[key] && this.category[key][0] == ' ')
      this.category[key] = this.category[key].trim();
  }
}
