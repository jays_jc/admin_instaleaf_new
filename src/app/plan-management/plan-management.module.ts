import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryService } from './services/category.service';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { PlanManagementRoutingModule } from './plan-management-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  imports: [
  	PlanManagementRoutingModule,
  	 CommonModule,
     NgxDatatableModule,
     NgxSpinnerModule
  ],
  providers: [
    CategoryService
  ],
  declarations: [
  	ListCategoryComponent,
  	AddUpdateCategoryComponent
  ]
})
export class PlanManagementModule { }