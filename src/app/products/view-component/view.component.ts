import { environment } from './../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrandService } from '../services/brand.service';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: 'view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent {
  private _host = environment.config.HOST;
  // private _dateFormat    = tsConstants.DATE_FORMAT;

  public ID = '';
  public object = {};
  // public isPageLoading   = true;
  // public isLoading       = false;
  public addEditDelete: boolean = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _brandService: BrandService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['crops']['addEditDelete'])
      this.addEditDelete = true;

    this.ID = _route.snapshot.params['id'];
    this.getDetail();
  }

  getDetail() {
    this.spinner.show();
    this._brandService.get(this.ID).subscribe(
      (res) => {  
        // this.isPageLoading = false;
        if (res.success) {
          this.object = res.data;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        // this.isPageLoading = false;
      }
    );
  }

  route(ID, path) {
    let route = '/product/' + path + '/' + ID;
    this._router.navigate([route]);
  }
}
