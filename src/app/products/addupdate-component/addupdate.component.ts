import { environment } from './../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ImageResult } from 'ng2-imageupload';
import { ThrowStmt } from '@angular/compiler';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';
import { ToastrService } from 'ngx-toastr';
import { BrandService } from '../services/brand.service';
import { Location } from '@angular/common';
// import { BrandService } from '../services/brand.service';

@Component({
  templateUrl: 'addupdate.component.html',
  styleUrls: ['./addupdate.component.scss'],
})
export class AddUpdateComponent {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  @ViewChild('myInput1', { static: true })
  myInputVariable1: any;
  private _host = environment.config.HOST;

  public object = {
    name: '',
    city: '',
    //cities:[],
    category_id: '',
    thc: null,
    thcrange: null,
    cbd: null,
    cbdrange: null,
    cbg: null,
    cbgrange: null,
    cbn: null,
    cbnrange: null,
    cba: null,
    cbarange: null,
    user_visits: 1,
    image: '',
    detail: '',
    isFeatured: false,
    // isForDelivery:false,
    product_category: [],
    product_subcategory: [],
    terpene_profile: [],
    second_name: '',
    producer: '',
    type: '',
    reward_point: null,
    meta_name: '',
    meta_desc: '',
    thc_type: '',
    cbd_type: '',
    lineage: '',
  };

  public terpene_profile = [];

  /*    public city = '';*/
  public errMessage: any;
  public cattype = '';

  public isLoading = false;
  public isPageLoading = true;
  public sellers = [];
  public categories = [];
  public rewardcategories = [];
  public poscustomcategories = [];
  public othercategories = [];
  public cities = [];
  public prodCategories = ['Recreational', 'Medical', 'Conditions'];
  public terpeneProfile = [
    'A-PINENE',
    'HUMULENE',
    'BETA CARYOPHYLLENE',
    'MYRCENE',
    'LIMONENE',
  ];
  public subCategories = [];
  public subcategoryIDs = [];
  public productSubcat = '';
  public ID: any;
  ProducerArray = [];
  Recreational = [];
  Medical = [];
  Conditions = [];
  newLabelTMC = 'THC Range %';
  newLabelMCB = 'CBD Range %';
  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _brandService: BrandService,
    private _commanService: CommanService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private location: Location
  ) {
    this.ID = _activateRouter.snapshot.params['id'];
    if (this.ID) {
      this._brandService.get(this.ID).subscribe(
        (res) => {
          // console.log("data of product is",res)
          if (res.success) {
            this.object = res.data;
            this.object.name = res.data.name;
            this.object.city = res.data.city;
            this.object.cba = res.data.cba;
            this.object.cbarange = res.data.cbarange;
            this.object.cbd = res.data.cbd;
            this.object.cbd_type = res.data.cbd_type;
            this.object.cbg = res.data.cbg;
            this.object.cbgrange = res.data.cbgrange;
            this.object.cbn = res.data.cbn;
            this.object.cbnrange = res.data.cbnrange;
            // this.terpene_profile = res.data.terpene_profile;
            this.object.terpene_profile = res.data.terpene_profile;
            // console.log(this.object.terpene_profile);
            // console.log("data of product is",res.data)
            this.object.category_id = res.data.category_id.id;
            if (res.data.producer) this.object.producer = res.data.producer.id;
            // this.subcategoryIDs        = this.object.product_subcategory
            this.subcategoryIDs = this.object.product_subcategory;
            // this.object.product_subcategory    = res.data.product_subcategory.id;
            // this.fetchSubCategories(this.object['product_category'],false)
            // this.selectTerpeneProfile();
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
          this.spinner.hide();
          // this.isPageLoading = false;
        },
        (err) => {
          this.spinner.hide();
          // this.isPageLoading = false;
        }
      );
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }

    this.allCities();
    this.allCategories();
    this.fetchProducerArray();
    this.otherCategory();
    this.productCategory();
    this.posCustomCategory();

    this.prodCategories.forEach((value) => {
      this.getAllTags(value);
    });
    this.fetchTerpeneProfile();
  }

  changeLabel(e: string) {
    let baseId;
    this.categories.forEach((element) => {
      if (element.name == 'Edible') {
        baseId = element.id;
      }
    });

    if (baseId == e) {
      this.newLabelTMC = 'THC Range in Mg';
      this.newLabelMCB = 'CBD Range in Mg';
      this.object.thc_type = 'Mg';
      this.object.cbd_type = 'Mg';
    } else {
      this.newLabelTMC = 'THC Range in %';
      this.newLabelMCB = 'CBD Range in %';
      this.object.thc_type = '%';
      this.object.cbd_type = '%';
    }
  }

  getAllTags(value) {
    this._brandService.getSubCat(value).subscribe(
      (res) => {
        if (res.success) {
          this[value] = res.data;
        }
      },
      (err) => {}
    );
  }

  fetchTerpeneProfile() {
    this._brandService.getTerpenProfile().subscribe(
      (res) => {
        if (res.success) {
          // console.log("here",res.data)
          this.terpene_profile = res.data || [];
          this.terpene_profile.forEach((element) => {
            element['value'] = '';
          });
          //   console.log("terpene print",this.terpene_profile)
          //   console.log(this.object.terpene_profile);
          //   console.log(this.terpene_profile);
          for (var i = 0; i < this.object.terpene_profile.length; i++) {
            for (var j = 0; j < this.terpene_profile.length; j++) {
              if (
                this.object.terpene_profile[i].id == this.terpene_profile[j].id
              ) {
                this.terpene_profile[j]['showInput'] = true;
                this.terpene_profile[j]['value'] = this.object.terpene_profile[
                  i
                ].value;
                // this.terpene_profile[j]['value'] = this.terpene_profile[j].value;
                this.object.terpene_profile.splice(
                  i,
                  1,
                  this.terpene_profile[j].name
                );
              }
            }
          }
          //   console.log(this.terpene_profile);
          //   this.selectTerpeneProfile();
        }
      },
      (err) => {}
    );
  }

  getValue(e) {
    // console.log(e);
    for (var i = 0; i < e.length; i++) {
      if (e.length > 1) {
        for (var j = 0; j < this.terpene_profile.length; j++) {
          if (e[i] == this.terpene_profile[j].name) {
            this.terpene_profile[j]['showInput'] = true;
          }
        }
      } else {
        for (var j = 0; j < this.terpene_profile.length; j++) {
          if (e[i] == this.terpene_profile[j].name) {
            this.terpene_profile[j]['showInput'] = true;
          } else {
            this.terpene_profile[j]['showInput'] = false;
          }
        }
      }
    }
    // console.log(this.terpene_profile);
  }

  fetchProducerArray() {
    this._brandService.getProducer().subscribe(
      (res) => {
        if (res.success) {
          this.ProducerArray = res.data;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
        this.spinner.hide();
        // this.isPageLoading = false;
      },
      (err) => {
        this.spinner.hide();
        // this.isPageLoading = false;
      }
    );
  }

  selectTerpeneProfile() {
    if (
      !this.object.terpene_profile ||
      this.object.terpene_profile == undefined
    ) {
      this.object.terpene_profile = [];
    } else {
      // console.log('object ..',this.object.terpene_profile);
      // console.log(this.terpene_profile);
      // for (var i = 0; i < this.object.terpene_profile.length; i++) {
      //     for (var j = 0; j < this.terpene_profile.length; j++) {
      //         if(this.object.terpene_profile[i].id == this.terpene_profile[j].id) {
      //             this.terpene_profile[j]['showInput'] = true;
      //             // this.terpene_profile[j]['value'] = this.terpene_profile[j].value;
      //             this.object.terpene_profile.splice(i,1,this.terpene_profile[j].name);
      //         }
      //     }
      // }
      // console.log(this.terpene_profile);
    }
  }

  /*If id exist then will update existing brand otherwise will add new brand*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.object));
    data.terpene_profile = [];
    // console.log(this.terpene_profile);
    for (var i = 0; i < this.object['terpene_profile'].length; i++) {
      for (var j = 0; j < this.terpene_profile.length; j++) {
        if (this.object['terpene_profile'][i] == this.terpene_profile[j].name) {
          var object = {
            id: this.terpene_profile[j].id,
            value: this.terpene_profile[j].value,
            name: this.terpene_profile[j].name,
          };
          data.terpene_profile.push(object);
        }
      }
    }
    data.product_subcategory = this.subcategoryIDs;
    // console.log(data);
    if (this.object.type != 'reward' && this.subcategoryIDs.length == 0) {
      this.errMessage = 'Subcategory is Required.';
      this.spinner.hide();
      // this.isLoading = false;
    } else {
      if (this.ID) {
        this._brandService.update(data).subscribe(
          (res) => {
            this.spinner.hide();
            // this.isLoading = false;
            if (res.success) {
              this.toastr.success(res.data.message);
              // this._commanService.showAlert(res.data.message, 'alert-success');
              // this._commanService.back();
            } else {
              this.toastr.error(res.error.message);
              // this._commanService.checkAccessToken(res.error);
              // this._commanService.showAlert(res.error.message, 'alert-danger');
            }
          },
          (err) => {
            this.toastr.error(
              'There is Some Problem please try after some time.'
            );
            // this._commanService.showAlert(
            //   'There is Some Problem please try after some time.',
            //   'alert-danger'
            // );
            // this.isLoading = false;
            this.spinner.hide();
          }
        );
      } else {
        this._brandService.add(data).subscribe(
          (res) => {
            this.spinner.hide();
            // this.isLoading = false;
            if (res.success) {
              this.toastr.success(res.data.message);
              // this._commanService.showAlert(res.data.message, 'alert-success');
              // this._commanService.back();
            } else {
              this.toastr.error(res.error.message);
              // this._commanService.checkAccessToken(res.error);
              // this._commanService.showAlert(res.error.message, 'alert-danger');
            }
          },
          (err) => {
            this.spinner.hide();
            // this.isLoading = false;
          }
        );
      }
    }
  }

  validateNumber(e: any) {
    let input = String.fromCharCode(e.charCode);
    const reg = /^\d*(?:[.]\d{1,2})?$/;

    if (!reg.test(input)) {
      e.preventDefault();
    }
  }

  allCities() {
    this._commanService.allCities().subscribe(
      (res) => {
        if (res.success) {
          let data = res.data.data;
          for (var i = 0; i < data.length; ++i) {
            this.cities.push(data[i].name);
          }
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  allCategories() {
    this._commanService.getAllCategories().subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.rewardcategories = res.data.category;
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  productCategory() {
    this._commanService.getAllCategoriesByType('product').subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.categories = res.data.newscat;
          console.log('cat', this.categories);
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  posCustomCategory() {
    this._commanService.getAllPosCustom().subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.poscustomcategories = res.data.data;
          // console.log("cat",this.categories)
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  otherCategory() {
    this._commanService.getAllCategoriesByType('other').subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.othercategories = res.data.newscat;
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  trim(key) {
    if (this.object[key] && this.object[key][0] == ' ')
      this.object[key] = this.object[key].trim();
  }

  addEelement(id) {
    // if(!this.productSubcat){
    //     this.errMessage = 'Subcategory is Required.';
    //     return false;
    // }
    if (id != '' || id != undefined) {
      let index = this.subcategoryIDs.indexOf(id);
      if (index >= 0) {
        this.subcategoryIDs.splice(index, 1);
        this.toastr.error('Already exist.');
        // this.errMessage = tsMessages.ALREADY_EXISTS;
        // return false;
      } else {
        this.subcategoryIDs.push(id);
        // this.productSubcat = "";
        // this.errMessage = "" ;
        // return true;
      }
    }
  }

  catName(id) {
    let name = '';
    this.subCategories.forEach((obj) => {
      if (id == obj.id) name = obj.subcategoryname;
    });
    return name;
  }

  removeEelement(index) {
    this.subcategoryIDs.splice(index, 1);
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'product',
    };
   
    // this.isLoading = true;
    this.spinner.show();
    this._commanService.uploadImage(object).subscribe(
      (res) => {
        
        // this.isLoading = false;
        if (res.success) {
          this.object.image = res.data.fullPath;
        } else {
          window.scrollTo(0, 0);
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
        this.myInputVariable.nativeElement.value = '';
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  fetchSubCategories(value, key) {
    let name = value;
    this._brandService.getSubCat(name).subscribe(
      (res) => {
        this.isLoading = false;
        if (res.success) {
          this.subCategories = res.data;
          if (key) {
            // this.object.product_subcategory = '';
            this.subcategoryIDs = [];
          }
        }
      },
      (err) => {}
    );
  }

  removeImage(image) {
    this.object.image = '';
  }

  back() {
    this.location.back();
  }
}
