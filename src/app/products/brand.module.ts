import { NgxSpinnerModule } from 'ngx-spinner';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list-component/list.component';
import { EditRatingComponent } from './editrating-component/editrating.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';
import { BrandRoutingModule } from './brand-routing.module';
import { BrandService } from './services/brand.service';
import { FormsModule } from '@angular/forms';
import { RatingComponent } from './rating-component/rating.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
    BrandRoutingModule,
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    NgxSpinnerModule,
    ImageUploadModule
  ],
  providers: [BrandService],
  declarations: [
    ListComponent,
    AddUpdateComponent,
    EditRatingComponent,
    ViewComponent,
    RatingComponent,
    BulkUploadComponent,
  ],
  //Don't forget add component to entryComponents section
  entryComponents: [],
})
export class BrandModule {}
