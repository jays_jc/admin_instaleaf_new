import { environment } from './../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild } from '@angular/core';
// import { BrandService } from '../services/brand.service';
// import { CommanService } from '../../shared/services/comman.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BrandService } from '../services/brand.service';
import { CommanService } from 'src/app/shared/comman.service';
import { Location } from '@angular/common';

// import tsConstants = require('./../../tsconstant');

@Component({
  selector: 'app-editrating',
  templateUrl: './editrating.component.html',
  styleUrls: ['./editrating.component.scss'],
})
export class EditRatingComponent implements OnInit {
  private _host = environment.config.HOST;
  public object = {
    rating: '',
    details: '',
  };

  public isLoading: boolean = false;
  public ID: any;
  public isPageLoading: boolean = false;
  public prodCategories = ['Effects', 'Medical', 'Negatives'];
  Effects = [];
  Medical = [];
  Negatives = [];
  subcategoryIDs = [];
  ratingArray = [1, 2, 3, 4, 5];

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _brandService: BrandService,
    private _commanService: CommanService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private location: Location
  ) {
    this.ID = _activateRouter.snapshot.params['id'];
  }

  ngOnInit() {
    this.fetch();
    this.prodCategories.forEach((value) => {
      this.getAllTags(value);
    });
  }

  fetch() {
    this.spinner.show();
    // this.isLoading = true;
    this._brandService.getReview(this.ID).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.object = res.data.key;
          this.subcategoryIDs = res.data.key.tags;
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  getAllTags(value) {
    this._brandService.productReviewTag(value).subscribe(
      (res) => {
        if (res.success) {
          this[value] = res.data;
        }
      },
      (err) => {}
    );
  }

  /*If id exist then will update existing reviews and rating*/
  save() {
    this.spinner.show();
    // this.isLoading         = true;
    let data = JSON.parse(JSON.stringify(this.object));
    this._brandService.editReviews(this.ID, data).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
          this.location.back();
        } else {
          this.toastr.error(res.error);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  addEelement(id) {
    let index = this.subcategoryIDs.indexOf(id);
    if (index >= 0) {
      this.subcategoryIDs.splice(index, 1);
    } else {
      this.subcategoryIDs.push(id);
    }
  }
}
