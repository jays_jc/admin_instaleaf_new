import { CookieService } from 'ngx-cookie';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommanService } from 'src/app/shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { BrandService } from '../services/brand.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    product: string;
    id: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      product: '',
      id: '',
    };
  ratingList: any = [];
  public addEditDelete: boolean = true;
  public response: any;
  private _path: any;
  private _subscriberData: any;
  totalItems: any;

  public constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private brandService: BrandService,
    private _cookieService: CookieService
  ) {
    this._path = this._route.snapshot['_urlSegment'].segments[0].path;

    let actions = this._commanService.getActions();

    this.filters.id = _route.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.filters.page = this._route.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getRatings();
  }

  getRatings() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.brandService
      .getAllReviewList(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.ratingList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.ratingList = response['data'].products.map((data) => {
            return {
              id: data.id,
              user: data.addedby,
              rating: data.rating ? data.rating : '-',
              detail: data.detail ? data.detail : '-',
              date: data.createdAt,
            };
          });
          // this.isLoading = false;
          // this.spinner.hide();
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  route(cropID, path) {
    let route = '/product/' + path + '/' + cropID;
    this._router.navigate([route]);
  }

  viewUser(userID, path) {
    let route = path + '/' + userID;
    this._router.navigate([route]);
  }

  remove(ID) {
    if (confirm('Do you want to delete?')) {
      // this.isLoading = true;
      this.spinner.show();
      this.brandService.delete(ID).subscribe(
        (res) => {
          this.response = res;
          // this.isLoading = false;
          this.spinner.hide();
          // let start = this.activePage * this.rowsOnPage - this.rowsOnPage + 1;
          // this.itemsTotal = this.itemsTotal - 1;
          // if (!(this.itemsTotal >= start)) {
          //   this.activePage = this.activePage - 1;
          // }
          this._cookieService.put('categoryAlert', res.result.sucess.Message);
          /* reload page. */
          this.getRatings();
        },
        (err) => {
          // this.isLoading = false;
          this.spinner.hide();
        }
      );
    }
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/product/rating/' + page;
    this.getRatings();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, {
      page: 1,
      search: this.filters.search,
    });
    this.getRatings();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getRatings();
  }

  revoke(ID) {
    if (confirm('Do you want to Revoke Reward ?')) {
      this.spinner.show();
      // this.isLoading = true;
      this.brandService.RevokeReviews(ID).subscribe(
        (res) => {
          if (res.success) {
            this.spinner.hide();
            // this.isLoading = false;
            // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
            // this.itemsTotal = this.itemsTotal - 1;

            // if( ! (this.itemsTotal >= start) ){
            //    this.activePage = this.activePage -1;
            //    if(this.activePage == 0) this.activePage = 1;
            // }
            // /* reload page data */
            this.getRatings();
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
          } else {
            this.spinner.hide();
            this.toastr.error(res.error.message);
            // this.isLoading = false;
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }
}
