import { CookieService } from 'ngx-cookie';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommanService } from './../../shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { BrandService } from '../services/brand.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-crops',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    product: string;
    city: string;
    producer: string;
    type: string;
  } = {
      page: 0,
      count: 25,
      search: '',
      sortBy: 'createdAt desc',
      product: '',
      city: '',
      producer: '',
      type: 'other',
    };
  productList: any = [];
  public addEditDelete: boolean = true;
  public response: any;
  private _path;
  public cities: any = [];
  public producers: any = [];
  totalItems: any;

  constructor(
    private _commanService: CommanService,
    private _activateRouter: ActivatedRoute,
    private _router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private brandService: BrandService,
    private _cookieService: CookieService
  ) {
    this._path = this._activateRouter.snapshot['_urlSegment'].segments[0].path;

    let actions = this._commanService.getActions();
    // this.spinner.show();
    // this.isPageLoading = true;

    this.allCities();
    this.allProducers();
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getProducts();
  }

  allCities() {
    this._commanService.allCities().subscribe(
      (res) => {
        if (res.success) {
          let data = res.data.data;
          for (var i = 0; i < data.length; ++i) {
            // code...
            this.cities.push(data[i].name);
          }
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  allProducers() {
    this._commanService.allProducers().subscribe(
      (res) => {
        if (res.success) {
          //this.producers = res.data.producers
          // above line commmented because old api producerlist is not working anymore
          this.producers = res.data;
          // for (var i = 0; i < data.length; ++i) {
          //     // code...
          // this.producers.push(data[i].name);
          // }
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getProducts() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.brandService
      .getAllProducts(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.productList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.productList = response['data'].products.map((data) => {
            return {
              id: data.id,
              name: data.name,
              producer: data.producerinfo ? data.producerinfo.name : '-',
              type: data.category ? data.category : '-',
              category: data.product_category
                ? this.categoryParent(data.product_category)
                : '-',
              subCategory: data.product_subcategoryname
                ? data.product_subcategoryname
                : '-',
              city: data.city ? data.city : '-',
              userVisits: data.user_visits ? data.user_visits : '-',
              reviews: data.reviews ? data.reviews : '-',
              rating: data.rating ? data.rating : '-',
              date: data.createdAt,
            };
          });
          // this.isLoading = false;
          // this.spinner.hide();
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  route(cropID, path) {
    let route = '/product/' + path + '/' + cropID;
    this._router.navigate([route]);
  }

  remove(ID) {
    if (confirm('Do you want to delete?')) {
      // this.isLoading = true;
      this.spinner.show();
      this.brandService.delete(ID).subscribe(
        (res) => {
          this.response = res;
          // this.isLoading = false;
          this.spinner.hide();
          // let start = this.activePage * this.rowsOnPage - this.rowsOnPage + 1;
          // this.itemsTotal = this.itemsTotal - 1;
          // if (!(this.itemsTotal >= start)) {
          //   this.activePage = this.activePage - 1;
          // }
          this._cookieService.put('categoryAlert', res.result.sucess.Message);
          /* reload page. */
          this.getProducts();
        },
        (err) => {
          // this.isLoading = false;
          this.spinner.hide();
        }
      );
    }
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/product/list/' + page;
    this.getProducts();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, {
      page: 1,
      search: this.filters.search,
      city: this.filters.city,
      producer: this.filters.producer,
    });
    this.getProducts();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getProducts();
  }

  categoryParent(array) {
    let newArray = [];
    if (array.indexOf('Recreational') >= 0) newArray.push('Recreational');
    if (array.indexOf('Medical') >= 0) newArray.push('Medical');
    if (array.indexOf('Conditions') >= 0) newArray.push('Conditions');
    return newArray;
  }
}
