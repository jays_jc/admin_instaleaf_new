import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-forgot-passowrd',
  templateUrl: './forgot-passowrd.component.html',
  styleUrls: ['./forgot-passowrd.component.scss'],
})
export class ForgotPassowrdComponent implements OnInit {
  public forgotForm: FormGroup;
  submitted = false;
  _forgotObservable: any;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService,
  ) {
    this.createForm();
  }

  createForm() {
    this.forgotForm = this.formBuilder.group({
      username1: ['', [Validators.required]],
    });
  }
  get f() {
    return this.forgotForm.controls;
  }

  ngOnInit() {}

  onSubmit() {
    this.submitted = true;
    if (!this.forgotForm.invalid) {
      this.spinner.show();
      this._forgotObservable = this.authService
        .forgotPasswordEmail(this.forgotForm.value)
        .subscribe(
          (res) => {
            if (res.success) {
              this.toastr.success(res.data.message);
              // this.successMessage = res.data.message;
            } else {
              this.toastr.error(res.error.message);
              // this.errMessage     = res.error.message;
            }

            this.spinner.hide();
          },
          (error) => {
            this.spinner.hide();
            this.toastr.error('No such user exist.');
          }
        );
    }
  }

  ngOnDestroy(): void {
    if (this._forgotObservable) {
      this._forgotObservable.unsubscribe();
    }
  }
}
