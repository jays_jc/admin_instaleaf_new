import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import {
  HttpErrorResponse,
  HttpParams,
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _host = environment.config.HOST;

  constructor(private httpClient: HttpClient) {}

  login(context) {
    let param = this.getParams(context);
    for (let key of Object.keys(context)) {
      param = param.set(key, context[key]);
    }
    return this.httpClient.post(this._host + `oauth/token`, param).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  forgotPasswordEmail(formData): Observable<any> {
    return this.httpClient.post(this._host + `forgotpassword`, formData).pipe(
      map((response: any) => {
        console.log(response);
        return response;
      }),
      catchError(this.handleError)
    );
  }

  logout() {}

  getParams(parameters) {
    let params = new HttpParams();
    Object.keys(parameters).map((key) => {
      params = params.set(key, parameters[key]);
    });
    return params;
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('Session expired . Please login again!');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    } else if (error.error.code == 301) {
      return throwError(error.error.message);
    }
    else if (error.status == 403) {
      return throwError('Wrong credentials!');
    }
    return throwError('Something bad happened; please try again later.');
  }
}
