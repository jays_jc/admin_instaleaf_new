import { CookieService } from 'ngx-cookie';
import { environment } from './../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public adminLoginForm: FormGroup;
  submitted = false;
  public rememberMe: boolean = true;
  _loginObservable: any;
  passwordType: boolean;
  private _session = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private _activateRouter: ActivatedRoute,
    private _cookieService: CookieService
  ) {
    // this._session = _activateRouter.snapshot.params['data'];
    // if (this._session) {
    //   this.toastr.error('Your session has expired. Please login again.');
    //   // this.errMessage = tsMessages.YOUR_SESSION_HAS_EXPIRED_PLEASE_LOGIN_AGAIN;
    // }
    this.createForm();
  }

  ngOnInit() {
    if (localStorage.getItem('remember') && (localStorage.getItem('rememberPassword'))) {
      this.adminLoginForm.patchValue({
        username: localStorage.getItem('remember'),
        password: localStorage.getItem('rememberPassword')
      });
    }
  }

  markRemember() {
    this.rememberMe = !this.rememberMe;
  }

  createForm() {
    this.adminLoginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      // remember: [''],
      grant_type: environment.config.GRANT_TYPE,
      client_id: environment.config.CLIENT_ID,
    });
  }

  get f() {
    return this.adminLoginForm.controls;
  }

  togglePassword() {
    this.passwordType = !this.passwordType;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.adminLoginForm.invalid) {
      this.spinner.show();
      this._loginObservable = this.authService
        .login(this.adminLoginForm.value)
        .subscribe(
          (res) => {
            console.log(res);
            let token = res.access_token;
            this.setValue(res);

            let actions;
            if (res.role_id && res.role_id['permission']) {
              actions = res.role_id['permission'];
              actions['type'] = res.roles;
              this.routeNavigate(token, actions);
            } else if (res.roles == 'SA') {
              actions = {
                type: res.roles,
              };
              this.routeNavigate(token, actions);
            } else {
              this.toastr.error(
                'You are not authorized please contact market admin.'
              );
              // this.isPageLoading = false;
              // this.errMessage = tsMessages.YOU_ARE_NOT_AUTHORIZED_PLEASE_CONTACT_MARKET_ADMIN;
            }
            // if (res.code == 200) {
            //   this.router.navigate(['/dashboard']);
            //   this.toastr.success('Logged In Successfully!');
            // } else {
            //   this.toastr.error(res.error.message, 'Error');
            // }

            if (this.rememberMe == true) {
              localStorage.setItem(
                'remember',
                this.adminLoginForm.value.username,
              );
              localStorage.setItem(
                'rememberPassword',
                this.adminLoginForm.value.password,
              );
            } else {
              localStorage.removeItem('remember');
              localStorage.removeItem('rememberPassword');
            }

            this.spinner.hide();
          },
          (error) => {
            this.spinner.hide();
            this.toastr.error(error, 'Error');
          }
        );
    }
  }

  setValue(res) {
    let mObj = {
      markets: [],
      selectedMarket: {
        name: 'All',
        pincode: [],
      },
    };
    this._cookieService.putObject('markets', mObj);
  }

  routeNavigate(token, actions) {
    /* Setup Cookie */
    // localStorage.setItem('token', token);
    this._cookieService.put('token', token);
    // localStorage.setItem('actions', actions);
    this._cookieService.putObject('actions', actions);
    if (this.rememberMe) {
      localStorage.setItem('remember', this.adminLoginForm.value.username);
      localStorage.setItem(
        'rememberPassword',
        this.adminLoginForm.value.password,
      );
    } else {
      localStorage.removeItem('remember');
      localStorage.removeItem('rememberPassword');
    }
    if (!this._session) {
      let route = '/dashboard';
      this.router.navigate([route]);
    } else {
      // this._commanService.back();
    }
  }

  ngOnDestroy(): void {
    if (this._loginObservable) {
      this._loginObservable.unsubscribe();
    }
  }
}
