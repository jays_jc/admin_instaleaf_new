import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommanService } from './../../shared/comman.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AutomationService } from '../services/automation.service';
declare let jsPDF;

@Component({
  selector: 'automation',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string; } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
  };
  categoryList: any = [];
  public response: any;
  public addEditDelete: boolean = false;
  totalItems: any;

  public constructor(
    private _router: Router,
    private _automationService: AutomationService,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService,
  ) {

    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();

  }
  remove(ID) {
  }
  /*Get all Users */
  getCategory() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._automationService
      .getAllUsers(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].data.map((data) => {
            return {
              id: data.id,
              business_type: data.business_type,
              subject: data.subject,
              desc: data.desc,
              createdAt: data.createdAt,
            };
          });
          // this.isLoading = false;
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }
  setPage(e) {
    // console.log(e);
    this.page = e.offset;
    Object.assign(this.filters, { page: this.page });
    // let route = '/automation/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  view(ID) {
    let route = '/automation/list/' + ID
    this._router.navigate([route]);
  }

  edit(ID) {
    let route = '/automation/edit/' + ID;
    this._router.navigate([route]);
  }
  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

}