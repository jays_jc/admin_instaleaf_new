import { NgxSpinnerModule } from 'ngx-spinner';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutomationService } from './services/automation.service';
import { ListComponent } from './list-component/list.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';
import { AutomationRoutingModule } from './automation-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
@NgModule({
  imports: [
    AutomationRoutingModule,
    CommonModule,
    NgxDatatableModule,
    NgxSpinnerModule
  ],
  providers: [
    AutomationService
  ],
  declarations: [
    ListComponent,
    AddUpdateComponent,
    ViewComponent
  ]
})
export class AutomationModule { }