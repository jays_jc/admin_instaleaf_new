import { environment } from './../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AutomationService } from '../services/automation.service';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: 'addupdate.component.html',
  styleUrls: ['./addupdate.component.scss']
})

export class AddUpdateComponent {

  private _host = environment.config.HOST;

  public object = {
    subject: '',
    desc: '',
    business_type: ''

  };

  public isLoading = false;
  public isPageLoading = false;
  public ID: any;
  public business_types = ['Brand', 'Doctor', 'Dispensary', 'User'];

  constructor(private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _automationService: AutomationService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {

    this.ID = _activateRouter.snapshot.params['id'];
  }

  /*If id exist then will update existing dispensary otherwise will add new dispensary*/
  save() {
    this.spinner.hide();
    // this.isLoading         = true;
    let data = JSON.parse(JSON.stringify(this.object));

    this._automationService.add(data).subscribe(res => {
      this.spinner.hide();
      // this.isLoading = false;
      if (res.success) {
        this.toastr.success(res.data.message);
        // this._commanService.showAlert(res.data.message,'alert-success');
        let route = '/automation/list/';
        this._router.navigate([route]);
      } else {
        this.toastr.error(res.error.message);
        // this._commanService.checkAccessToken(res.error);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  trim(key) {
    if (this.object[key] && this.object[key][0] == ' ') this.object[key] = this.object[key].trim();
  }

  // onDateChanged(event: IMyDateModel): void {
  //     // date selected
  // }
}
