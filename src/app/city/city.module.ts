import { NgxSpinnerModule } from 'ngx-spinner';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCityComponent } from './list-component/list-city.component';
import { CityRoutingModule } from './city-routing.module';
import { AddupdateCityComponent } from './addupdate-component/addupdate-city.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CityService } from './services/city.service';
@NgModule({
  imports: [
    CommonModule,
    CityRoutingModule,
    NgxDatatableModule,
    NgxSpinnerModule
  ],
  providers: [
    CityService
  ],
  declarations: [
    ListCityComponent,
    AddupdateCityComponent

  ]
})
export class CityModule { }
