import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable()
export class CityService {

  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _CommanService: CommanService
  ) {

  }

  /*Use to fetch all Inputs*/
  getAllCity(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'getallcities', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to add new City*/
  add(city) {
    return this._http.post(this._host + 'city', city).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.post(this._host +'/city', city, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to get city with id*/
  get(ID) {
    return this._http.get(this._host + 'city/' + ID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/city/'+ ID, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to update city*/
  update(city) {
    return this._http.put(this._host + 'city/' + city.id, city).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // console.log("city",this._host)
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/city/'+ city.id, city, { headers: headers }).map((res:Response) => res.json())
  }


  /*Use to Delete city with city id */
  delete(Id) {
    return this._http.delete(this._host + 'city/' + Id,).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.delete(this._host +'/city/'+ Id,  { headers: headers }).map((res:Response) => res.json());
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }


}  
