import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListCityComponent } from './list-component/list-city.component';
import { AddupdateCityComponent } from './addupdate-component/addupdate-city.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'City'
    },
    children: [
     {
        path: '',
        component: ListCityComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'list',
        component: ListCityComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'add',
        component: AddupdateCityComponent,
        data: {
          title: 'Add City'
        }
      },
      {
        path: 'edit/:id',
        component: AddupdateCityComponent,
        data: {
          title: 'Edit City'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
    RouterModule,
    FormsModule,
  ]
})
export class CityRoutingModule { }
