import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CityService } from '../services/city.service';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: './addupdate-city.component.html',
  styleUrls: ['./addupdate.component.scss']
})
export class AddupdateCityComponent implements OnInit {


  public city = {
    name: '',
    province: '',
    isForDelivery: ''
  };

  public isLoading = false;
  public isPageLoading = true;
  public cityID: any;
  public response: any;
  public type;
  public errMessage = '';
  public province = [];

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _cityService: CityService,
    private spinner: NgxSpinnerService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private toastr: ToastrService) {

    this.cityID = _activateRouter.snapshot.params['id'];
    this.spinner.show();
    if (this.cityID) {
      this._cityService.get(this.cityID).subscribe(res => {
        this.spinner.hide();
        // this.isPageLoading = false;
        if (res.success) {
          this.city = res.data.city;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      });
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }


  }

  ngOnInit(): void {

    // this.showDangerAlert();
    this.allProvinces()
  }

  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.city));
    if (this.cityID) {
      console.log("fsdf", this.cityID)
      this._cityService.update(data).subscribe(res => {
        this.spinner.hide();
        // this.isLoading         = false;
        if (res.success) {
          this.response = res.data.city;
          this.toastr.success('City updated successfully')

          //this._cookieService.put('cityAlert', res.data.message);
          this._router.navigate(['/city/list']);
        } else {
          //this.toastr.error(res.error.message);

         // this._cookieService.put('cityExistAlert', res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      })
    } else {
      this._cityService.add(data).subscribe(res => {
        this.spinner.hide();
        // this.isLoading         = false;e
        if (res.success) {
          this.response = res.data.city;
          this.toastr.success('City updated successfully')

         // this._cookieService.put('cityAlert', res.data.message);
          this._router.navigate(['/city/list']);
        } else {
          this.toastr.error(res.error.message);
        //  this._cookieService.put('cityExistAlert', res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });

    }
  }

  allProvinces() {
    this._commanService.allProvinces().subscribe(res => {
      this.spinner.hide();
      this.isLoading = false;
      if (res.success) {
        this.province = res.data.city;
      }
    }, err => {
      this.spinner.hide();
      this.isLoading = false;
    })
  }



  trim(key) {
    if (this.city[key] && this.city[key][0] == ' ') this.city[key] = this.city[key].trim();
  }



}
