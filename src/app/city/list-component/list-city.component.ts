import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { CommanService } from './../../shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CityService } from '../services/city.service';

@Component({
  selector: 'app-city',
  templateUrl: './list-city.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListCityComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string; date: string; } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
    date: new Date().getTime().toString(),
  };
  categoryList: any = [];
  public response: any;
  public isPageLoading: boolean = true;
  public addEditDelete: boolean = false;
  totalItems: any;


  public constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService,
    private _cityService: CityService,
    private toastr: ToastrService
  ) {
    let actions = this._commanService.getActions();
    if (actions["type"] == 'SA' || actions['city']['addEditDelete']) this.addEditDelete = true;

  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();

  }

  remove(ID) {
    if (confirm("Do you want to delete?")) {
      this.spinner.show();
      // this.isLoading = true;
      this._cityService.delete(ID).subscribe(res => {
        if (res.success) {
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1;
          //    if(this.activePage == 0) this.activePage = 1;
          // }
          /* reload page data */
          this.getCategory();
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
        } else {
          this.spinner.hide();
          this.toastr.error(res.error.message);
          // this.isLoading = false;
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }

  /*Get all Users */
  getCategory() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._cityService
      .getAllCity(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].city.map((data) => {
            return {
              id: data.id,
              name: data.name,
              province: data.province,
              createdAt: data.createdAt,
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }
  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/city/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  edit(ID) {
    let route = '/city/edit/' + ID;
    this._router.navigate([route]);
  }
  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }
}