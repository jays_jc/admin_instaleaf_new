import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './list-component/list.component';
import { ViewComponent } from './view-component/view.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Orders',
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'list',
        component: ListComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'view',
        component: ViewComponent,
        data: {
          title: 'View',
        },
      },
      {
        path: 'view/:id',
        component: ViewComponent,
        data: {
          title: 'View',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), FormsModule],
  exports: [RouterModule, FormsModule],
})
export class PosCategoryRoutingModule {}
