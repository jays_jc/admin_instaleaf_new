import { NgxSpinnerService } from 'ngx-spinner';
import { CommanService } from 'src/app/shared/comman.service';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { DriverService } from '../services/driver.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { POSCategoryService } from '../services/poscategory.service';
@Component({
  templateUrl: 'view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
  } = {
      page: 0,
      count: 25,
      search: '',
      sortBy: 'createdAt desc',
      date: new Date().getTime().toString(),
    };
  public addEditDelete: boolean = false;
  categoryList: any = [];
  totalItems: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: POSCategoryService,
    private _commanService: CommanService,
    private _activateRouter: ActivatedRoute,
    private posCategoryService: POSCategoryService,
    private spinner: NgxSpinnerService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getSubCategories();
    // this.getAllMainCategories();
  }

  getSubCategories() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.posCategoryService
      .getMainCategoryWithSubcgy(this.filters)
      .subscribe(
        (response) => {
          if (!response.success) {
            this.categoryList = [];
            // this.isLoading = false;
          } else {
            this.totalItems = response.data.total;
            this.categoryList = response['data'].data.map((data) => {
              return {
                id: data.id,
                name: data.name,
                subcategories: data.subCategories,
                date: data.createdAt,
              };
            });
            // this.isLoading = false;

          }
          this.spinner.hide();
        },
        (error) => {
          this.spinner.hide();
        }
      );
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset;
    Object.assign(this.filters, { page: this.page });
    // let route = '/poscategory/view/' + page;
    this.getSubCategories();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getSubCategories();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getSubCategories();
  }
}
