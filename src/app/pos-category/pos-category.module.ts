import { NgxSpinnerModule } from 'ngx-spinner';
import { POSCategoryService } from './services/poscategory.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { NG2DataTableModule } from "angular2-datatable-pagination";
// import { DriverService } from './services/driver.service';
// import { SharedModule } from '../shared/shared.module';
// import { CustomFormsModule } from 'ng2-validation';
// import { FlashMessagesModule } from 'ngx-flash-messages';
import { PosCategoryRoutingModule } from './pos-category-routing.module';

import { ListComponent } from './list-component/list.component';
import { ViewComponent } from './view-component/view.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
    CommonModule,
    PosCategoryRoutingModule,
    //  NG2DataTableModule,
    //  CustomFormsModule,
    //  FlashMessagesModule,
    //  SharedModule,
    NgxSpinnerModule,
    NgxDatatableModule,
    ImageUploadModule
  ],
  providers: [POSCategoryService],
  declarations: [ListComponent, ViewComponent],
})
export class PosCategoryModule {}
