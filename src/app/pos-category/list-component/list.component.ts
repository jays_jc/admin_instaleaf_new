import { NgxSpinnerService } from 'ngx-spinner';
import { CommanService } from 'src/app/shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
// import { DriverService } from '../services/driver.service';
// import { CommanService } from '../../shared/services/comman.service';
// import { CookieService } from 'ngx-cookie';
// import { Angular2Csv } from 'angular2-csv/Angular2-csv';
// import { FlashMessagesService } from 'ngx-flash-messages';

// import tsMessages  = require('../../tsmessage');
import { ColumnMode } from '@swimlane/ngx-datatable';
import { POSCategoryService } from '../services/poscategory.service';
import { ToastrService } from 'ngx-toastr';
declare let jsPDF;

@Component({
  selector: 'app-driver',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
  } = {
      page: 0,
      count: 100,
      search: '',
      sortBy: 'createdAt desc'
    };
  filtersMainCategory: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
  } = {
      page: 1,
      count: 2000,
      search: '',
      sortBy: 'createdAt desc',
      date: new Date().getTime().toString(),
    };
  categoryList: any = [];
  public addEditDelete: boolean = false;
  public response: any;
  mainCategories: any = [];
  posCategories: any = [];
  totalItems: any;

  public constructor(
    private _router: Router,
    private _commanService: CommanService,
    private _activateRouter: ActivatedRoute,
    private posCategoryService: POSCategoryService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategories();
    this.getAllMainCategories();
  }

  getCategories() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.posCategoryService
      .getAllCategories(this.filters)
      .subscribe(
        (response) => {
          if (!response.success) {
            this.categoryList = [];
            // this.isLoading = false;
          } else {
            this.totalItems = response.data.total;
            this.categoryList = response['data'].itemCategory.map((data) => {
              return {
                id: data.id,
                name: data.name,
                master: data.isMaster,
                // store: data.dispensary_id.name,
                date: data.createdAt,
                instaleaf_categoryId: data.instaleaf_categoryId
                  ? data.instaleaf_categoryId
                  : '',
              };
            });
            // this.isLoading = false;
            // this.spinner.hide();
          }
          this.spinner.hide();
        },
        (error) => {
          this.spinner.hide();
        }
      );
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/poscategory/list/' + page;
    this.getCategories();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategories();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategories();
  }

  getAllMainCategories(): void {
    this.posCategoryService
      .getAllMainCategory(this.filtersMainCategory)
      .subscribe(
        (res) => {
          if (res.success) {
            this.mainCategories = res.data.masterCategories;
            // this.itemsTotal    = res.data.total;
            // this.showAlert();
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          // this.isLoading     = false;
          // this.isPageLoading = false;
          // this._commanService.checkAccessToken(err);
        }
      );
  }

  changeMainCtegory(id, mainId) {
    if (id && mainId) {
      let data = {
        id: id,
        cat_id: mainId,
      };
      this.posCategoryService.updateMainCategory(data).subscribe(
        (res) => {
          this.isLoading = false;
          if (res.success) {
            this.toastr.success(res.message);
            // this._commanService.showAlert(res.message, 'alert-success');
            this.getCategories();
            // this._commanService.back();
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.checkAccessToken(res.error);
            // this._commanService.showAlert(res.error.message, 'alert-danger');
          }
        },
        (err) => {
          this.toastr.error(
            'There is Some Problem please try after some time.',
            'alert-danger'
          );
          // this._commanService.showAlert(
          //   'There is Some Problem please try after some time.',
          //   'alert-danger'
          // );
          // this.isLoading = false;
        }
      );
    }
  }

  view(userID) {
    let route = '/orders/list/' + userID;
    this._router.navigate([route]);
  }

  // changeStatus(user) {

  //     if(confirm("Do you want to Mark as deliver ?")) {
  //         this.isLoading = true;
  //         user['order_status'] = "Delivered"
  //         this._driverService.update(user).subscribe(res => {
  //             if(res.success) {
  //                 this.response  = res;
  //                 let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
  //                 this.itemsTotal = this.itemsTotal - 1;

  //                 if( ! (this.itemsTotal >= start) ){
  //                    this.activePage = this.activePage -1
  //                 }
  //                 this._commanService.showAlert(res.data.message,'alert-success');
  //                 /* reload page. */
  //                 this.getUsers();
  //             } else {
  //                 this.isLoading    = false;
  //                 this._commanService.showAlert(res.error.message,'alert-danger');
  //                 this._commanService.checkAccessToken(res.error);
  //             }
  //         },err => {
  //             this.isLoading = false;
  //         });
  //     }
  // }

  // sendInvoice(order) {
  //     if(confirm("Do you want to send invoice to supplier ?")) {
  //         this.isLoading = true;
  //         //user['isInvoice'] = "Sent"
  //         this._driverService.sendingInvoice(order).subscribe(res => {
  //             if(res.success) {
  //                 this.response  = res;
  //                 this._commanService.showAlert(res.data.message,'alert-success');
  //                 /* reload page. */
  //                 this.getUsers();
  //             } else {
  //                 this.isLoading    = false;
  //                 this._commanService.showAlert(res.error.message,'alert-danger');
  //                 this._commanService.checkAccessToken(res.error);
  //             }
  //         },err => {
  //             this.isLoading = false;
  //         });
  //     }
  // }

  /*Get all Users */
  // getUsers(): void {
  //     this._driverService.getAllUsers( this.rowsOnPage, this.activePage, this.sortTrem,  this.searchTerm, this.roles ).subscribe(res => {
  //         this.isLoading     = false;
  //         this.isPageLoading = false;
  //         if(res.success) {
  //             this.data          = res.data.data;
  //             this.itemsTotal    = res.data.total;
  //             if(res.data.users && res.data.users.length == 1) this.activePage = 1;
  //         } else {
  //             this._commanService.checkAccessToken(res.error);
  //         }
  //     },err => {
  //         this.isLoading     = false;
  //         this.isPageLoading = false;
  //    });
  // }

  // public onPageChange(event) {
  //     this.isLoading     = true;
  //     this.rowsOnPage = event.rowsOnPage;
  //     this.activePage = event.activePage;
  //     this.getUsers();
  // }

  // public onRowsChange( event ): void {
  //     this.isLoading  = true;
  //     this.rowsOnPage = this.itemsOnPage;
  //     this.activePage = 1;
  //     this.getUsers();
  // }

  // public onSortOrder(event) {
  //     this.sortTrem = this.sortBy+' '+this.sortOrder;
  //     this.isLoading  = true;
  //     this.getUsers();
  // }

  // public search( event, element = 'input' ) {
  //     if( element == 'input' ) {
  //         if(event.keyCode == 13 || this.searchTerm == '') {
  //             this.searchTerm = this.searchTerm.trim();
  //             this.isLoading  = true;
  //             this.getUsers();
  //             this.activePage = 1;
  //             this.getUsers();
  //         }
  //     }else{
  //         this.searchTerm = this.searchTerm.trim();
  //         this.isLoading  = true;
  //         this.getUsers();
  //         this.activePage = 1;
  //         this.getUsers();
  //     }
  // }
}
