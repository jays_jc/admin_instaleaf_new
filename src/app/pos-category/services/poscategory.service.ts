import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { CommanService } from 'src/app/shared/comman.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
// import {
//   Http,
//   Response,
//   Headers,
//   RequestOptions,
//   URLSearchParams,
// } from '@angular/http';
// import { CommanService } from '../../shared/services/comman.service';
// import tsConstants = require('../../tsconstant');

@Injectable()
export class POSCategoryService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService
  ) {}

  /*Use to fetch all Inputs*/
  getAllCategories(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'itemCategoryList', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let date = new Date().getTime().toString();
    // let url =
    //   this._host +
    //   '/itemCategoryList?count=' +
    //   rowsOnPage +
    //   '&page=' +
    //   activePage +
    //   '&search=' +
    //   search;

    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  /*Use to fetch all Inputs*/
  getAllMainCategory(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'masterCategoriesList', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  // /*Use to add new Input*/
  // add(category) {
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this._http
  //     .post(this._host + '/category', category, { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  // /*Use to get category with id*/
  // get(ID) {
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this._http
  //     .get(this._host + '/category/' + ID, { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  // /*Use to update category*/
  // update(category) {
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this._http
  //     .put(this._host + '/category/' + category.id, category, {
  //       headers: headers,
  //     })
  //     .map((res: Response) => res.json());
  // }

  // getCategoryByType(type) {
  //   let headers = this._commanService.getAuthorizationHeader();
  //   let url = this._host + '/categorybytype/' + type;
  //   console.log('news are', url);
  //   return this._http
  //     .get(url, { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  // updateNewsCategory(newsinfo) {
  //   let obj = {
  //     id: newsinfo.id,
  //     categoryId: newsinfo.categoryId,
  //   };
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this._http
  //     .put(this._host + '/addnewscategory', newsinfo, { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  // /*Use to Delete category with category id */
  // delete(Id) {
  //   let headers = this._commanService.getAuthorizationHeader();
  //   return this._http
  //     .delete(this._host + '/deletecat/' + Id, { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  updateMainCategory(obj) {
    return this._http
      .put(
        this._host +
          'mergePosCategory?pos_catId=' +
          obj.id +
          '&main_catId=' +
          obj.cat_id,
        {}
      )
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .put(
    //     this._host +
    //       '/mergePosCategory?pos_catId=' +
    //       obj.id +
    //       '&main_catId=' +
    //       obj.cat_id,
    //     {},
    //     { headers: headers }
    //   )
    //   .map((res: Response) => res.json());
  }

  getMainCategoryWithSubcgy(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'mainCategoryWithSubcategory', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let date = new Date().getTime().toString();
    // let url =
    //   this._host +
    //   '/mainCategoryWithSubcategory?count=' +
    //   rowsOnPage +
    //   '&page=' +
    //   activePage +
    //   '&sortBy=' +
    //   sortTrem +
    //   '&search=' +
    //   search +
    //   '&date=' +
    //   date;

    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
