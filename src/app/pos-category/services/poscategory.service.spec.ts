import { TestBed, inject } from '@angular/core/testing';

import { POSCategoryService } from './poscategory.service';

describe('CategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [POSCategoryService],
    });
  });

  it('should ...', inject(
    [POSCategoryService],
    (service: POSCategoryService) => {
      expect(service).toBeTruthy();
    }
  ));
});
