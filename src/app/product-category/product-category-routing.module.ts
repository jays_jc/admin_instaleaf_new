import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { ViewCategoryComponent } from './view-component/view-category.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Product Category',
    },
    children: [
      {
        path: '',
        component: ListCategoryComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'list',
        component: ListCategoryComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'add',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Add Product Category',
        },
      },
      {
        path: 'view',
        component: ViewCategoryComponent,
        data: {
          title: 'View',
        },
      },
      {
        path: 'edit/:id',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Edit Product Category',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), FormsModule],
  exports: [RouterModule, FormsModule],
})
export class ProductCategoryRoutingModule {}
