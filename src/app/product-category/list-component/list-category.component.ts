import { CommanService } from 'src/app/shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
// import { CategoryService } from '../services/category.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { CategoryService } from 'src/app/category/services/category.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare let jsPDF;

@Component({
  selector: 'app-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
})
export class ListCategoryComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      date: new Date().getTime().toString(),
    };
  categoryList: any = [];
  public addEditDelete: boolean = false;
  totalItems: any;
  public constructor(
    private _router: Router,
    private _commanService: CommanService,
    private _categoryService: CategoryService,
    private spinner: NgxSpinnerService,
    private _activateRouter: ActivatedRoute
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getProductCategories();
  }

  getProductCategories() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._categoryService
      .getAllProductCategories(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.categoryList = [];
          // this.isLoading = false;
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].category.map((data) => {
            return {
              id: data.id,
              name: data.name,
              Subcategory: data.subcategoryname,
              // type: data.type,
              date: data.createdAt,
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  // remove(ID) {
  //   if (confirm('Do you want to delete?')) {
  //     this.isLoading = true;
  //     this._categoryService.delete(ID).subscribe(
  //       (res) => {
  //         this.response = res;
  //         this.isLoading = false;
  //         // let start = this.activePage * this.rowsOnPage - this.rowsOnPage + 1;
  //         // this.itemsTotal = this.itemsTotal - 1;

  //         // if (!(this.itemsTotal >= start)) {
  //         //   this.activePage = this.activePage - 1;
  //         // }
  //         this._cookieService.put('categoryAlert', res.result.sucess.Message);
  //         /* reload page. */
  //         this.getCategories();
  //       },
  //       (err) => {
  //         this.isLoading = false;
  //       }
  //     );
  //   }
  // }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/category/list/' + page;
    this.getProductCategories();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getProductCategories();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getProductCategories();
  }

  edit(ID) {
    let route = '/category/edit/' + ID;
    this._router.navigate([route]);
  }


  
  
}
