import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { ViewCategoryComponent } from './view-component/view-category.component';
import { ProductCategoryRoutingModule } from './product-category-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CategoryService } from '../category/services/category.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [ProductCategoryRoutingModule, CommonModule, NgxDatatableModule,NgxSpinnerModule, ImageUploadModule,],
  declarations: [
    ListCategoryComponent,
    AddUpdateCategoryComponent,
    ViewCategoryComponent,
    
  ],
  providers: [CategoryService],
})
export class ProductCategoryModule {}
