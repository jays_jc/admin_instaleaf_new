import { environment } from './../../../environments/environment';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
// import { CategoryService } from '../services/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ImageResult } from 'ng2-imageupload';
import { CommanService } from 'src/app/shared/comman.service';
import { CookieService } from 'ngx-cookie';
import { CategoryService } from 'src/app/category/services/category.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate-category.component.scss'],
})
export class AddUpdateCategoryComponent {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;

  public category = {
    name: '',
    subcategoryname: '',
    image: '',
  };

  private _host = environment.config.HOST;
  public isLoading = false;
  public isPageLoading = true;
  public categoryID: any;
  public oBj = { vname: '' };
  public response: any;
  public type;
  public errMessage = '';
  public ParentCategories = ['Recreational', 'Medical', 'Conditions'];

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    // private _flashMessagesService: FlashMessagesService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    this.categoryID = _activateRouter.snapshot.params['id'];

    if (this.categoryID) {
      this._catgService.getProductCategoryDetail(this.categoryID).subscribe(
        (res) => {
          this.isPageLoading = false;
          if (res.success) {
            this.category = res.data[0];
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.isPageLoading = false;
          // this._commanService.checkAccessToken(err);
        }
      );
    } else {
      this.isPageLoading = false;
    }
  }

  ngOnInit(): void {
    // this.showDangerAlert();
  }

  /*If categoryID exist then will update existing category otherwise will add new category*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.category));
    if (this.categoryID) {
      this._catgService.updateProductCategory(data).subscribe(
        (res) => {
          // this.isLoading = false;
          if (res.success) {
            this.response = res;
            this._cookieService.put('categoryAlert', res.data.message);
            this.toastr.success('Updated Successfully!');
            this._router.navigate(['/product-category/list']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
          this.spinner.hide()
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    } else {
      this._catgService.addProductCategory(data).subscribe(
        (res) => {
          // this.isLoading = false;
          if (res.success) {
            this.response = res;
            this._cookieService.put('categoryAlert', res.data.message);
            this.toastr.success(res.message);
            this._router.navigate(['/product-category/list']);
          } else {
            this.toastr.error(res.error.message);
            this._cookieService.put('categoryExistAlert', res.error.message);
            // this.showDangerAlert();
          }
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  // showDangerAlert(): void {

  //     let alertMessage = this._cookieService.get('categoryExistAlert');
  //     if( alertMessage ) {
  //         this._flashMessagesService.show( alertMessage, {
  //             classes: ['alert', 'alert-danger'],
  //             timeout: 3000,
  //         });
  //         this._cookieService.remove('categoryExistAlert');
  //     }
  // }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'dispensary',
    };
   this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(
      (res) => {
        // this.isLoading = false;
        if (res.success) {
          this.category.image = res.data.fullPath;
          this.myInputVariable.nativeElement.value = '';
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  trim(key) {
    if (this.category[key] && this.category[key][0] == ' ')
      this.category[key] = this.category[key].trim();
  }
}
