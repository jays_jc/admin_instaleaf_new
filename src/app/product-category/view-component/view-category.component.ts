import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/category/services/category.service';
// import { CategoryService } from '../services/category.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  templateUrl: 'view-category.component.html',
  styleUrls: ['./view-category.component.scss'],
})
export class ViewCategoryComponent {
  public ID = '';
  public category = { type: '' };
  public isLoading: boolean = true;
  public addEditDelete: boolean = false;

  constructor(
    private _router: Router,
    private _activatedRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _categoryService: CategoryService
  ) {
    this._activatedRouter.queryParams.subscribe((params) => {
      this.ID = params['id'];
      this.getCategoryDetail();
    });
  }

  getCategoryDetail() {
    this.spinner.show();
    if (this.ID) {
      this._categoryService.getDetailProductCategory(this.ID).subscribe(
        (res) => {
          this.isLoading = false;
          this.spinner.hide();
          if (res.success) {
            this.category = res.data[0];
            /*console.log(res.data.type);
                    this.category.type = this.firstUpper(this.category.type);*/
          } else {
            this.spinner.hide();
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.isLoading = false;
          this.spinner.hide();
          // this._commanService.checkAccessToken(err);
        }
      );
    }
  }

  route(ID, path) {
    let route = '/product-category/' + path + '/' + ID;
    this._router.navigate([route]);
  } 
}
