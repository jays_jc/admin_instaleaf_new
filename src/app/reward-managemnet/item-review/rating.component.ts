import { CommanService } from './../../shared/comman.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { RewardService } from '../services/reward.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-item-review-list',
  templateUrl: './rating.component.html'
})
export class ItemRatingComponent implements OnInit {

  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    city: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      city: '',
    };
  reviewList: any = [];
  private _path: any;
  ID: any;
  public addEditDelete: boolean = true;
  totalItems: any;

  public constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _dispensaryService: RewardService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService
  ) {
    this._path = this._route.snapshot["_urlSegment"].segments[0].path;

    let actions = this._commanService.getActions();

    this.ID = _route.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.filters.page = this._route.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getList();

  }

  /*Get all Users */
  getList(): void {
    this.spinner.show();

    this._subscriberData = this._dispensaryService
      .getAllReviewList(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.reviewList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.reviewList = response['data'].items.map((data) => {
            return {
              id: data.id,
              itemId: data._id.item_id,
              fullName: data.fullName,
              storeRating: data.store ? data.store : '-',
              rating: data.rating ? data.rating : '-',
              storeLayoutRating: data.storeLayoutRating ? data.storeLayoutRating : '-',
              staffRating: data.staffRating ? data.staffRating : '-',
              reviews: data.detail ? data.detail : '-',
              createdAt: data.createdAt,
              // addedBy: data.addedBy._id,
              // productDetail: data.productDetail._id
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },

        error => {
          this.spinner.hide();
        });
  }

  route(productID, path) {
    let route = '/products/' + path + '/' + productID;
    this._router.navigate([route]);
  }

  routeitem(reviewId, path) {
    let route = '/dispensary/' + path + '/' + reviewId;
    this._router.navigate([route]);
  }

  viewUser(userID, path) {
    let route = path + '/' + userID;
    this._router.navigate([route]);
  }

  remove(ID) {
    if (confirm("Do you want to delete ?")) {
      this.spinner.show();
      // this.isLoading = true;
      this._dispensaryService.deleteItemReviews(ID).subscribe(res => {
        if (res.success) {
          this.spinner.hide();
          // this.isLoading = false;
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1;
          //    if(this.activePage == 0) this.activePage = 1;
          // }
          /* reload page data */
          this.getList();
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
        } else {
          this.spinner.hide();
          this.toastr.error(res.error.message);
          // this.isLoading = false;
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }

  revoke(ID) {
    if (confirm("Do you want to Revoke Reward ?")) {
      this.spinner.show();
      // this.isLoading = true;
      this._dispensaryService.RevokeReviews(ID, "reviews").subscribe(res => {
        console.log("res is ", res);
        if (res.success) {
          this.spinner.hide();
          // this.isLoading = false;
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1;
          //    if(this.activePage == 0) this.activePage = 1;
          // }
          /* reload page data */
          this.getList();
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
        } else {
          this.spinner.hide();
          this.toastr.error(res.error.message);
          // this.isLoading = false;
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/reward/list/' + page;
    this.getList();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getList();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getList();
  }


}
