import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { RewardService } from '../services/reward.service';
@Component({
  selector: 'app-order-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    city: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      city: '',
    };
  rewardsList: any = [];
  totalItems: any;


  constructor(
    private _router: Router,
    private RewardService: RewardService,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService

  ) { }

  ngOnInit() {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();
  }

  /*Get all Users */
  getCategory(): void {
    this.spinner.show();

    this._subscriberData = this.RewardService
      .getAllList(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.rewardsList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.rewardsList = response['data'].products.map((data) => {
            return {
              order_id: data.order_id,
              username1: data.addedBy.firstName ? data.addedBy.firstName : data.addedBy.username1,
              name: data.productDetail?data.productDetail.name:'',
              address: data.destination_location ? data.destination_location : '-',
              type: data.reward_type,
              status: data.status,
              rewardPoint: data.point ? data.point : '-',
              createdAt: data.createdAt,
              addedBy: data.addedBy._id,
              productDetail: data.productDetail?data.productDetail._id:''
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },

        error => {
          this.spinner.hide();
        });
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/reward/list/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  route(ID, path) {
    let route = '/product/' + path + '/' + ID;
    this._router.navigate([route]);
  }

  userRoute(ID, path) {
    let route = '/user/' + path + '/' + ID;
    this._router.navigate([route]);
  }

  /* Function use to approve dispensary with dispensary id*/
  changeStatus(ID, status) {
    if (confirm("Do you want to " + status + " this Order?")) {
      this.spinner.show();
      // this.isLoading = true;
      let obj = {
        id: ID,
        status: status
      }
      this.RewardService.updateOrder(obj).subscribe(res => {
        if (res.success) {
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1
          // }
          // if(this.activePage == 0) this.activePage = 1;
          /* reload page. */
          this.getCategory();
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
        } else {
          this.spinner.hide();
          this.toastr.error(res.error.message);
          // this.isLoading = false;
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }
}
