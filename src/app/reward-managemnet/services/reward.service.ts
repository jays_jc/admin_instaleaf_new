import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators/map';
import { throwError } from 'rxjs';

@Injectable()
export class RewardService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService
  ) { }

  /*Use to fetch all Inputs*/
  getAllList(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'rewardallorder', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  add(obj) {
    return this._http.post(this._host + 'product', obj).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.post(this._host +'/product', obj, { headers: headers }).map((res:Response) => res.json())
  }

  updateOrder(obj) {
    return this._http.put(this._host + 'rewardorder', obj).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/rewardorder', obj, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to deletion of reviews*/
  deleteReviews(id) {
    let body = { isDeleted: true };
    return this._http
      .put(this._host + 'itemReviewDeletion?id=' + id, body)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // console.log("id in service is ",id)
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/itemReviewDeletion?id='+id;
    // let body = {isDeleted:true}
    // //return this._http.delete(url, { headers: headers }).map((res:Response) => res.json())
    // return this._http.put(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to deletion of reviews*/
  deleteItemReviews(id) {
    let body = { isDeleted: true };
    return this._http.put(this._host + 'deleteitemReviews?id=' + id, body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // console.log("id in service is ",id)
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/deleteitemReviews?id='+id;
    // let body = {isDeleted:true}
    // //return this._http.delete(url, { headers: headers }).map((res:Response) => res.json())
    // return this._http.put(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to deletion of reviews*/
  deleteProductReviews(id) {
    let body = { isDeleted: true };
    return this._http.put(this._host + 'reviewDeletion?id=' + id, body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/reviewDeletion?id='+id;
    // let body = {isDeleted:true}
    // return this._http.put(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  getAllReviewList(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'itemProductReview', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let date =  new Date().getTime().toString();
    // let url = this._host +'/itemProductReview?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&product='+search+'&id='+ID;
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  getAllProdReviewList(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'ProductReview', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let date =  new Date().getTime().toString();
    // let url = this._host +'/ProductReview?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&product='+search+'&id='+ID;
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  RevokeReviews(id, type) {
    let body = {
      id: id,
      isRevoked: true,
      type: type,
    };
    return this._http.put(this._host + 'revokedReview', body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/revokedReview';
    // let body = {
    //     "id": id,
    //     "isRevoked": true,
    //     "type":type
    // }
    // return this._http.put(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
