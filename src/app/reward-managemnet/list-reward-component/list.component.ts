import { CommanService } from './../../shared/comman.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { BrandService } from '../../products/services/brand.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
@Component({
  selector: 'app-list-reward',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [BrandService]
})
export class ListRewardComponent implements OnInit {

  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    city: string,
    type: string,
    product: string
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      city: '',
      type: 'reward',
      product: ''
    };
  cities: any = [];
  rewardsList: any = [];
  public addEditDelete: boolean = true;
  private _path: any;
  totalItems: any;
  public constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _brandService: BrandService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private _commanService: CommanService
  ) {
    this._path = this._route.snapshot["_urlSegment"].segments[0].path;

    let actions = this._commanService.getActions();

    this.spinner.show();

    this.allCities();
  }

  ngOnInit(): void {
    this.filters.page = this._route.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getList();
  }

  allCities() {
    this._commanService.allCities().subscribe(res => {
      if (res.success) {
        let data = res.data.data
        for (var i = 0; i < data.length; ++i) {
          // code...
          this.cities.push(data[i].name);
        }
      }
    }, err => {
      this.isLoading = false;
    });
  }

  route(cropID, path) {
    let route = '/product/' + path + '/' + cropID;
    this._router.navigate([route]);
  }

  viewUser(userID, path) {
    let route = path + '/' + userID;
    this._router.navigate([route]);
  }

  remove(ID) {
    if (confirm("Do you want to delete ?")) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.deleteRecord(ID, 'product').subscribe(res => {
        if (res.success) {
          this.spinner.hide();
          // this.isLoading = false;
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1;
          //    if(this.activePage == 0) this.activePage = 1;
          // }
          /* reload page data */
          this.getList();
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
        } else {
          this.spinner.hide();
          this.toastr.error(res.error.message);
          // this.isLoading = false;
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }

  /*Get all Users */
  getList(): void {
    this.spinner.show();

    this._subscriberData = this._brandService
      .getAllProducts(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.rewardsList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.rewardsList = response['data'].products.map((data) => {
            return {
              id: data.id,
              username: data.addedBy.firstName ? data.addedBy.firstName : data.addedBy.username1,
              name: data.name,
              city: data.cities,
              type: data.category ? data.category : '-',
              userVisits: data.user_visits ? data.user_visits : '-',
              reviews: data.reviews ? data.reviews : '-',
              rating: data.rating ? data.rating : '-',
              createdAt: data.createdAt,
              // addedBy: data.addedBy._id,
              // productDetail: data.productDetail._id
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },

        error => {
          this.spinner.hide();
        });
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/reward/list/' + page;
    this.getList();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search, city: this.filters.city });
    this.getList();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getList();
  }


}
