import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list-component/list.component';
import { TabsComponent } from './tabs/tabs.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Reward'
    },
    children: [
      {
        path: '',
        component: TabsComponent,
        data: {
          title: 'Reward Management'
        }
      },
      {
        path: 'list',
        component: TabsComponent,
        data: {
          title: 'Reward Management'
        }
      }  
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RewardManagemnetRoutingModule { }
