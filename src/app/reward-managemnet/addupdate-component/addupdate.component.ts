import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { RewardService } from '../services/reward.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';
import { ImageResult } from 'ng2-imageupload';

@Component({
  selector: 'app-addupdate-reward',
  templateUrl: 'addupdate.component.html',
  styleUrls: ['./addupdate.component.scss']
})
export class AddUpdateComponent {

  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  @ViewChild('myInput1', { static: true })
  myInputVariable1: any;
  private _host = environment.config.HOST;

  public object = {
    name: '',
    city: '',
    cities: [],
    category_id: '',
    thc: null,
    cbd: null,
    user_visits: 1,
    image: '',
    detail: '',
    isFeatured: false,
    product_category: [],
    product_subcategory: [],
    terpene_profile: [],
    second_name: '',
    producer: '',
    type: 'reward',
    reward_type: 'product',
    reward_point: null,
    meta_name: '',
    meta_desc: ''
  };

  public terpene_profile = [];

  /*    public city = '';*/
  public errMessage: any;

  public isLoading = false;
  public isPageLoading = true;
  public sellers = [];
  public categories = [];
  public cities = [];
  public subCategories = [];
  public subcategoryIDs = [];
  public productSubcat = '';
  public ID: any;
  ProducerArray = [];
  Recreational = [];
  Medical = [];
  Conditions = [];

  constructor(private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _rewardService: RewardService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private location: Location
  ) {

    this.allCities();
    this.allCategories();
  }

  /*If id exist then will update existing brand otherwise will add new brand*/
  save() {
    this.spinner.show();
    // this.isLoading         = true;
    let data = JSON.parse(JSON.stringify(this.object));
    this._rewardService.add(data).subscribe(res => {
      this.spinner.hide();
      // this.isLoading = false;
      if (res.success) {
        this.toastr.success(res.dta.message);
        // this._commanService.showAlert(res.data.message,'alert-success');
        this.object = {
          name: '   ',
          city: '',
          cities: [],
          category_id: data.category_id,
          thc: null,
          cbd: null,
          user_visits: 1,
          image: '',
          detail: '',
          isFeatured: false,
          product_category: [],
          product_subcategory: [],
          terpene_profile: [],
          second_name: '',
          producer: '',
          type: 'reward',
          reward_type: 'product',
          reward_point: 0,
          meta_name: '',
          meta_desc: ''
        };
      } else {
        this.toastr.error(res.error.message);
        // this._commanService.checkAccessToken(res.error);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  allCities() {
    this._commanService.allCities().subscribe(res => {
      if (res.success) {
        let data = res.data.data
        for (var i = 0; i < data.length; ++i) {
          this.cities.push(data[i].name);
        }
      } else {
        this.toastr.error(res.error.message);
        // this._commanService.checkAccessToken(res.error);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  allCategories() {
    this._commanService.getAllCategories().subscribe(res => {
      this.spinner.hide();
      // this.isLoading = false;
      if (res.success) {
        this.categories = res.data.category;
      } else {
        this.toastr.error(res.error.message);
        // this._commanService.checkAccessToken(res.error);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  trim(key) {
    if (this.object[key] && this.object[key][0] == ' ') this.object[key] = this.object[key].trim();
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'product'
    }
    
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(res => {
     
      // this.isLoading = false;
      if (res.success) {
        this.object.image = res.data.fullPath;
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message);
        // this._commanService.showAlert(res.error.message,'alert-danger');
      }
      this.myInputVariable.nativeElement.value = "";
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      // this.isLoading = false;
    });
  }

  back() {
    this.location.back();
  }


}
