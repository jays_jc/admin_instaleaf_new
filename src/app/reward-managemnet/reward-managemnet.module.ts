import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RewardManagemnetRoutingModule } from './reward-managemnet-routing.module';
import { RewardService } from './services/reward.service';
import { FormsModule } from '@angular/forms';
import { TabsComponent } from './tabs/tabs.component';
import { ListComponent } from './list-component/list.component';
import { ListRewardComponent } from './list-reward-component/list.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ItemRatingComponent } from './item-review/rating.component';
import { RatingComponent } from './product-review/rating.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
    CommonModule,
    RewardManagemnetRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxSpinnerModule,
    ImageUploadModule
  ],
  providers: [
    RewardService,
  ],
  declarations: [
    TabsComponent,
    ListComponent,
    ListRewardComponent,
    AddUpdateComponent,
    ItemRatingComponent,
    RatingComponent
  ]
})
export class RewardManagemnetModule { }
