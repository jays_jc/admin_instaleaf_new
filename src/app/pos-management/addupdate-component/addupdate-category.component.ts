import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate.component.scss']
})
export class AddUpdateCategoryComponent {

  public category = {
    name: ''
  };

  public isLoading = false;
  public isPageLoading = true;
  public categoryID: any;
  public response: any;
  public type;
  public errMessage = '';
  public ParentCategories = [];



  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    private spinner: NgxSpinnerService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private toastr: ToastrService) {

    this.categoryID = _activateRouter.snapshot.params['id'];
    this.spinner.show();
    if (this.categoryID) {
      this._catgService.get(this.categoryID).subscribe(res => {
        this.spinner.hide();
        // this.isPageLoading = false;
        if (res.success) {
          this.category = res.data[0];
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      }, err => {
        this.spinner.hide();
        // this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      });
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }


  }

  ngOnInit(): void {
    // this.showDangerAlert();
  }

  /*If categoryID exist then will update existing category otherwise will add new category*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.category));
    if (this.categoryID) {
      this._catgService.update(data).subscribe(res => {
        this.spinner.hide();
        // this.isLoading         = false;
        if (res.success) {
          this.response = res;

          this.toastr.success('Pos updated successfully')
        //  this._cookieService.put('categoryAlert', res.data.message);
          this._router.navigate(['/pos-management']);
        } else {
          this._cookieService.put('categoryExistAlert', res.error.message);
          this.toastr.error(res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
        // this.isLoading = false;
      })
    } else {
      this._catgService.add(data).subscribe(res => {
        this.spinner.hide();
        // this.isLoading         = false;
        if (res.success) {
          this.response = res;
          this.toastr.success('Pos added successfully')

          this._cookieService.put('categoryAlert', res.data.message);
          this._router.navigate(['/pos-management']);
        } else {
          this._cookieService.put('categoryExistAlert', res.error.message);
          this.toastr.error(res.error.message);
          // this.showDangerAlert();
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
        // this.isLoading = false;
      });

    }
  }


  trim(key) {
    if (this.category[key] && this.category[key][0] == ' ') this.category[key] = this.category[key].trim();
  }



}
