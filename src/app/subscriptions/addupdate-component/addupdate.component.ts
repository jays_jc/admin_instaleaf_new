import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsService } from '../services/news.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: './addupdate.component.html',
  styleUrls: ['./addupdate.component.scss'],
})
export class AddupdateComponent implements OnInit {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  public business_types = ['All', 'Dispensary', 'Brand'];
  private _host = environment.config.HOST;

  public news = {
    name: '',
    type: '',
    detail: '',
    price: null,
    count: null,
    productStatus: false,
    featureDispensary: false,
    analyticDashboard: false,
    reviewStatus: false,
    renewquarter: false,
    brandonfeaturedpage: false,
    bannerattop: false,
    introemail: false,
    businessType: 'Dispensary',
    status: 'active',
    planType: 'Paid',
    validateDay: null,
    dispensaries: [],
    brands: [],
    trial_period: false,
    trial_period_days: '',
    GSTPrice: null,
    reservedAhead: false,
    linkedInLiveMenus: false,
    featuredFrontpageWebApps: false,
    accessDashboard: false,
    editStoreDetails: false,
  };

  public isLoading = false;
  public isPageLoading = true;
  public descriptionError = false;
  public titleError = false;
  public subtitleError = false;
  public errMessage = '';
  public newsID: any;
  public dispensaries = [];
  public brands = [];
  public types = [];
  public names = [];

  public option: Object = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: false,
  };

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _newsService: NewsService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.newsID = _activateRouter.snapshot.params['id'];
    this.dispensariesUser();
    this.brandsUser();

    if (this.newsID) {
      this.spinner.show();
      this._newsService.get(this.newsID).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isPageLoading = false;
          if (res.success) {
            this.news = res.data[0];
            console.log(this.news);
            this.news.name = res.data[0].name['id'];
            this.news.GSTPrice = this.news.price;
            if (res.data[0].type) this.news.type = res.data[0].type['id'];
            this.setDispensaries();
            this.setBrands();
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isPageLoading = false;
        }
      );
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }
  }

  ngOnInit(): void {
    this.getNameType('name');
    this.getNameType('type');
  }

  dispensariesUser() {
    this._newsService.getDispensariesUser().subscribe(
      (res) => {
        if (res.success) this.dispensaries = res.data.users;
      },
      (err) => { }
    );
  }

  brandsUser() {
    this._newsService.getBrandsUser().subscribe(
      (res) => {
        if (res.success) this.brands = res.data.users;
      },
      (err) => { }
    );
  }

  getNameType(key) {
    this._newsService.getNameType(key).subscribe(
      (res) => {
        this[key + 's'] = res.data;
      },
      (err) => { }
    );
  }

  calculatePricewithGST(e) {
    var gstPrice = (e * 5) / 100;
    this.news.price = gstPrice + e;
  }

  save() {
    this.spinner.show();
    // this.isLoading = true;
    this.news.GSTPrice = '';
    let data = JSON.parse(JSON.stringify(this.news));
    if (data.dispensaries != '') {
      data.dispensaries = [];
      for (var i = 0; i < this.news['dispensaries'].length; i++) {
        for (var j = 0; j < this.dispensaries.length; j++) {
          if (
            this.dispensaries[j].email &&
            this.news['dispensaries'][i] == this.dispensaries[j].email
          ) {
            data.dispensaries.push(this.dispensaries[j].id);
          }
        }
      }
    } else {
      data.brands = [];
      for (var k = 0; k < this.news['brands'].length; k++) {
        for (var l = 0; l < this.brands.length; l++) {
          if (
            this.brands[l].email &&
            this.news['brands'][k] == this.brands[l].email
          ) {
            data.brands.push(this.brands[l].id);
          }
        }
      }
    }
    if (this.newsID) {
      this._newsService.update(data).subscribe(
        (res) => {
          this.spinner.hide();
          this.toastr.success('Record Updated successfully.');
          // this.isLoading = false;
          this._cookieService.put('newsAlert', 'Record Updated successfully.');
          this._router.navigate(['/subscrptions']);
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    } else {
      this._newsService.add(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success('Record Added successfully.');
            this._cookieService.put('newsAlert', 'Record Added successfully.');
            this._router.navigate(['/subscrptions']);
          } else {
            window.scrollTo(0, 0);
            this.errMessage = res.error.message;
            this.toastr.error(res.error.message);
            // this.showAlert();
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  setDispensaries() {
    if (!this.news.dispensaries) this.news.dispensaries = [];
    for (var i = 0; i < this.news.dispensaries.length; i++) {
      for (var j = 0; j < this.dispensaries.length; j++) {
        if (this.news.dispensaries[i] == this.dispensaries[j].id) {
          this.news.dispensaries.splice(i, 1, this.dispensaries[j].email);
        }
      }
    }
  }

  setBrands() {
    if (!this.news.brands) this.news.brands = [];
    for (var m = 0; m < this.news.brands.length; m++) {
      for (var n = 0; n < this.brands.length; n++) {
        if (this.news.brands[m] == this.brands[n].id) {
          this.news.brands.splice(m, 1, this.brands[n].email);
        }
      }
    }
  }

  // showAlert(): void {

  //         this._flashMessagesService.show( this.errMessage, {
  //             classes: ['alert', 'alert-danger'],
  //             timeout: 3000,
  //         });
  // }
}
