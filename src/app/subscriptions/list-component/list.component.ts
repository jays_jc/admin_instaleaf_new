import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-list-component',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      date: new Date().getTime().toString(),
    };
  categoryList: any = [];
  public addEditDelete: boolean = false;
  response: any;
  totalItems: any;

  public constructor(
    private _router: Router,
    private _newsService: NewsService,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService,
    private _cookieService: CookieService
  ) {
    let actions = this._commanService.getActions();
    if(actions["type"] == 'SA' || actions['news']['addEditDelete']) this.addEditDelete = true;
   }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();
  }
  /*Get all Users */
  getCategory(): void {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._newsService
      .getAllCategory(this.filters)
      .subscribe((response) => {
        console.log('data', response.data);
        if (!response.success) {
          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].subscribepackage.map((data) => {
            return {
              id: data.id,
              name: data.name,
              Type: data.planType,
              Renew: data.type ? data.type.name : '-',
              price: data.price,
              validateDay: data.validateDay,
              count: data.count,
              createdAt: data.createdAt,
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  /* Function use to remove News with news id*/
  removeNews(newsID) {
    if (confirm('Do you want to delete?')) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.deleteRecord(newsID, 'subscribepackages').subscribe(
        (res) => {
          this.response = res;
          this.spinner.hide();
          // this.isLoading = false;
          // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
          // this.itemsTotal = this.itemsTotal - 1;

          // if( ! (this.itemsTotal >= start) ){
          //    this.activePage = this.activePage -1
          //    if(this.activePage == 0) this.activePage = 1;
          // }
          this._cookieService.put('newsAlert', 'Deleted successfully.');
          /* reload page. */
          this.getCategory();
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/subscrptions/list/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  viewNews(newsID) {
    let route = '/subscrptions/list/' + newsID;
    this._router.navigate([route]);
  }

  editNews(newsID) {
    let route = '/subscrptions/edit/' + newsID;
    this._router.navigate([route]);
  }
}
