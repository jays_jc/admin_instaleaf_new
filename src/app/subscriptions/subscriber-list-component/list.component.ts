import { CommanService } from 'src/app/shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
@Component({
  selector: 'app-subscriber-list-component',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class SubscriberListComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      date: new Date().getTime().toString(),
    };
  categoryList: any = [];
  public addEditDelete: boolean = false;
  totalItems: any;

  public constructor(
    private _router: Router,
    private _newsService: NewsService,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['news']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getTransactionList();
  }

  getTransactionList(): void {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._newsService
      .getAllplanTransactions(this.filters)
      .subscribe((response) => {
        console.log('data', response.data);
        if (!response.success) {
          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].data.map((data) => {
            return {
              id: data.id,
              name: data.transaction_id,
              Type: data.plan_type,
              user: data.addedBy,
              price: data.price,
              createdAt: data.createdAt,
            };
          });
          // this.isLoading = false;
          // this.spinner.hide();
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/subscrptions/list/' + page;
    this.getTransactionList();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getTransactionList();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getTransactionList();
  }
}
