import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListComponent } from './list-component/list.component';
import { SubscriberListComponent } from './subscriber-list-component/list.component';
import { AddupdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Subscriptions'
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'List Subscriptions'
        }
      },
      {
        path: 'list',
        component: ListComponent,
        data: {
          title: 'List Subscriptions'
        }
      },
      {
        path: 'add',
        component: AddupdateComponent,
        data: {
          title: 'Add Subscriptions'
        }
      },
      {
        path: 'list/:id',
        component: ViewComponent,
        data: {
          title: 'View Subscriptions'
        }
      },

      {
        path: 'edit/:id',
        component: AddupdateComponent,
        data: {
          title: 'Edit Subscriptions'
        }
      },
      {
        path: 'subscribers',
        component: SubscriberListComponent,
        data: {
          title: 'List Transactions'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
  	RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
  	RouterModule,
    FormsModule,
  ]
})
export class SubscriptionsRoutingModule { }
