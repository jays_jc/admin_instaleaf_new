import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AddupdateComponent } from './addupdate-component/addupdate.component';
import { SubscriptionsRoutingModule } from './subscriptions-routing.module';
import { ListComponent } from './list-component/list.component';
import { ViewComponent } from './view-component/view.component';
import { SubscriberListComponent } from './subscriber-list-component/list.component';
import { NewsService } from './services/news.service';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  	imports: [
	    CommonModule,
	    SubscriptionsRoutingModule,
	    FormsModule,
		NgxDatatableModule,
		NgxSpinnerModule
  	],
  	exports: [
		RouterModule,
		FormsModule
	],
  	providers: [
  		NewsService
  	],
  	declarations: [
  		ListComponent,
  		AddupdateComponent,
		ViewComponent,
		SubscriberListComponent
  	],
  	entryComponents: [
  	], 
})
export class SubscriptionsModule { }
