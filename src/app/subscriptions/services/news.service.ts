import { CookieService } from 'ngx-cookie';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators/map';
import { throwError } from 'rxjs';
@Injectable()
export class NewsService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService,
    private _cookieService: CookieService
  ) {}

  /*Use to fetch all Inputs*/
  getAllCategory(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'subscribepackage', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  /*Use to fetch all news*/
  getAllplanTransactions(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'alltransactions', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );

    //     let date =  new Date().getTime().toString();
    //     let url = this._host +'/alltransactions?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&search='+search+'&date='+date;

    //     let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to add new news*/
  add(news) {
    return this._http.post(this._host + 'subscribepackage', news).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.post(this._host +'/subscribepackage', news, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to get news with crop id*/
  get(newsID) {
    return this._http.get(this._host + 'subscribepackage/' + newsID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/subscribepackage/'+ newsID, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to update news*/
  update(news) {
    return this._http.put(this._host + 'subscribepackage', news).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/subscribepackage', news, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to Delete news with news id */
  delete(newsID) {
    return this._http.delete(this._host + 'news/' + newsID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.delete(this._host +'/news/'+ newsID,  { headers: headers }).map((res:Response) => res.json());
  }

  getUserID() {
    let userData = this._cookieService.getObject('userData');
    if (userData) {
      return userData['id'];
    }
  }

  getNameType(key) {
    return this._http.get(this._host + 'subscribe' + key).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/subscribe'+key, { headers: headers }).map((res:Response) => res.json())
  }

  getDispensariesUser() {
    return this._http.get(this._host + 'user?roles=D').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/user?roles=D', { headers: headers }).map((res:Response) => res.json())
  }

  getBrandsUser() {
    return this._http.get(this._host + 'user?roles=B').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/user?roles=B', { headers: headers }).map((res:Response) => res.json())
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
