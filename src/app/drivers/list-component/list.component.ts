import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { DriverService } from '../services/driver.service';
import { CommanService } from './../../shared/comman.service';
import { CookieService } from 'ngx-cookie';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-driver',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string; roles: string; } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
    roles: 'DRIVER',
  };
  categoryList: any = [];
  public response: any;
  public addEditDelete: boolean = false;
  public exportData = [];
  private _path;
  totalItems: any;
  public constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _DriverService: DriverService,
    private _commanService: CommanService,
    private toastr:ToastrService
  ) {

    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();

  }
  remove(ID) {
  }
  /*Get all Users */
  getCategory() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._DriverService
      .getAllUsers(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {

          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].users.map((data) => {
            return {
              id: data.id,
              username1: data.username1,
              email: data.email ? data.email : data.username,
              mobile: data.mobile ? data.mobile : '-',
              rating: data.rating ? data.rating : '-',
              totalOrders: data.totalOrders ? data.totalOrders : '-',
              createdAt: data.createdAt,
              status: data.status
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => { this.spinner.hide(); }
      );
  }
  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/driver/' + page;
    this.getCategory();
    // this._router.navigate([route]);
  }

  viewUser(userID) {
    let route = '/drivers/view/' + userID;
    this._router.navigate([route]);
  }

  edit(ID) {
    let route = '/drivers/edit/' + ID;
    this._router.navigate([route]);
  }
  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }

  changeStatus(data, ID) {
    let status = '';
    let message = 'Do you want to activate this banner ?';
    if (data == 'active') {
      status = "deactive";
      message = 'Do you want to deactivate this banner ?';
    } else {
      status = "active";
    }

    if (confirm(message)) {
      this.spinner.show();
      this._commanService.changeStatus(ID,'user',status).subscribe(res => {
        if(res.success) {
            this.response  = res;
           this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
            /* reload page. */
            this.getCategory();
        } else {
          this.toastr.error(res.error.message);
            // this.isLoading    = false;
            // this._commanService.showAlert(res.error.message,'alert-danger');
            // this._commanService.checkAccessToken(res.error);
        }
        this.spinner.hide();
    },err => {
      this.spinner.hide();
        // this.isLoading = false;
    }); 
    }
  }




  //    /*Get all Crops fro export*/
  //   export(key) {   
  //     this.spinner.show();
  //     // this.isLoading         = true;
  //     this._DriverService.getAllUsers(this.filters).subscribe(res => {
  //       this.spinner.hide();
  //         // this.isLoading     = false;
  //         // this.isPageLoading = false;
  //         if(res.success) {
  //             this.exportData    = res.data.users;
  //             if(key == 'CSV') this.downloadCSV();
  //             if(key == 'PDF') this.downloadPDF();
  //         } else {
  //             // this._commanService.checkAccessToken(res.error);   
  //         }
  //     }, err => {
  //         this.spinner.hide();
  //         // this.isLoading     = false;
  //         // this.isPageLoading = false;
  //    });
  // }

  // downloadCSV(): void {
  //     let i;
  //     let filteredData = [];

  //     let header = {
  //         name:"Username",
  //         Email:'Email',
  //         mobile:'Mobile',
  //         rating:'Rating',
  //         totalOrders:"Total Orders",
  //         registeredOn:'Registered On',
  //         status:'Status'
  //     }

  //     filteredData.push(header);

  //     for ( i = 0; i < this.exportData.length ; i++ ) { 
  //         let date = new Date(this.exportData[i].createdAt);
  //         let registeredOn = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
  //         let temp = {
  //             name: this.exportData[i].username1,
  //             email: this.exportData[i].email ? this.exportData[i].email : this.exportData[i].username,
  //             mobile: this.exportData[i].mobile ? this.exportData[i].mobile :'-',
  //             rating: this.exportData[i].rating ? this.exportData[i].rating : '-',
  //             totalOrders: this.exportData[i].totalOrders ? this.exportData[i].totalOrders : '-',
  //             registeredOn: registeredOn,
  //             status:this.exportData[i].status =='active' ? 'Active' : 'Deactive'
  //         };

  //         filteredData.push(temp);
  //     }       

  //     let fileName = "DriverReport-"+Math.floor(Date.now() / 1000); 
  //     this._commanService.downloadCSV(fileName,filteredData)
  // }

  // downloadPDF() {

  //     let i;
  //     let filteredData = [];

  //     let header = [
  //         "Username",
  //         "Email",
  //         "Mobile",
  //         "Rating",
  //         "Total Orders",
  //         "Registered On",
  //         "Status"
  //     ]  

  //     for ( i = 0; i < this.exportData.length ; i++ ) { 
  //         let date = new Date(this.exportData[i].createdAt);
  //         let registeredOn = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

  //         let temp = [
  //             this.exportData[i].username1,                
  //             this.exportData[i].email ? this.exportData[i].email : this.exportData[i].username,
  //             this.exportData[i].mobile,
  //             this.exportData[i].rating ? this.exportData[i].rating : '-',
  //             this.exportData[i].totalOrders ? this.exportData[i].totalOrders : '-',
  //             registeredOn,
  //             this.exportData[i].status =='active' ? 'Active' : 'Deactive'
  //         ];

  //         filteredData.push(temp);
  //     }       

  //     let fileName = "DriverReport-"+Math.floor(Date.now() / 1000); 
  //     this._commanService.downloadPDF(fileName,header,filteredData);

  // }

}