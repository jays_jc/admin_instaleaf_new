import { environment } from './../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DriverService } from '../services/driver.service';
import { CommanService } from 'src/app/shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  templateUrl: 'view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent {
  public ID = '';
  public user = {};
  public isLoading: boolean = true;
  public addEditDelete: boolean = false;

  private _host = environment.config.HOST;

  public userID = '';
  public isPageLoading = true;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private DriverService: DriverService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {

    let actions = this._commanService.getActions();
    if (actions["type"] == 'SA' || actions['users']['addEditDelete']) this.addEditDelete = true;
    this.spinner.show();
    this.userID = _route.snapshot.params['id'];
    if (this.userID) {
      this.DriverService.get(this.userID).subscribe(res => {
        if (res.success) {
          this.user = res.data;
          this.spinner.hide();
          //  this.isPageLoading = false;
        } else {
          //  this._commanService.checkAccessToken(res.error); 
        }
      }, err => {
        this.spinner.hide();
        //  this.isPageLoading = false
      });
    }
  }

  editUser(userid) {
    let route = '/drivers/edit/' + userid;
    this._router.navigate([route]);
  }

  changeStatus(user) {
    let status = '';
    let message = 'Do you want to Activate Driver?';
    if (user.status == 'active') {
      status = "deactive";
      message = 'Do you want to Deactivate Driver?';
    } else {
      status = "active";
    }

    if (confirm(message)) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.changeStatus(user.id, 'users', status).subscribe(res => {
        this.spinner.hide();
        // this.isLoading    = false;
        if (res.success) {
          this.user["status"] = status;
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
          /* reload page. */
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      }, err => {
        this.spinner.hide();
        // this.isLoading = false;
      });
    }
  }




}