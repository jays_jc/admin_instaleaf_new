import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriversRoutingModule } from './drivers-routing.module';

import { ListComponent } from './list-component/list.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DriverService } from './services/driver.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
    CommonModule,
    DriversRoutingModule,
    NgxDatatableModule,
    NgxSpinnerModule,
    ImageUploadModule
  ],
  providers: [
    DriverService
  ],
  declarations: [
    ListComponent,
    AddUpdateComponent,
    ViewComponent
  ]
})
export class DriversModule { }
