import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DispensaryService } from '../services/dispensary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';
import { ImageResult } from 'ng2-imageupload';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss'],
})
export class EditProductComponent implements OnInit {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  private _host = environment.config.HOST;
  public object = {
    name: '',
    pre_roll: '',
    grams: '',
    Eighth: '',
    quarter: '',
    half: '',
    ounce: '',
    category_id: '',
    details: '',
    thc: '',
    image: '',
    isDeleted: false,
    cbd: '',
    status: 'active',
    brand_name: '',
    product_id: '',
    isForDelivery: false,
  };
  public categories = [];
  public isLoading: boolean = false;
  public ID: any;
  public isPageLoading: boolean = false;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _dispensaryService: DispensaryService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.ID = _activateRouter.snapshot.params['id'];
  }

  ngOnInit() {
    this.getAllCategories();
    this.fetch();
  }

  fetch() {
    this._dispensaryService.getProduct(this.ID).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.object = res.data.key[0];
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  /*If id exist then will update existing dispensary otherwise will add new dispensary*/
  save() {
    this.spinner.show();
    // this.isLoading         = true;
    let data = JSON.parse(JSON.stringify(this.object));
    this._dispensaryService.editProducts(this.ID, data).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
          this.location.back();
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  getAllCategories() {
    this.spinner.show();
    this._commanService.getAllCategories().subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          let data = res.data.category;
          for (let index = 0; index < data.length; index++) {
            if (data[index].type == 'other') this.categories.push(data[index]);
          }
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'product',
    };
   
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(
      (res) => {
        
        // this.isLoading = false;
        if (res.success) {
          this.object['image'] = res.data.fullPath;
        } else {
          window.scrollTo(0, 0);
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
        this.spinner.hide();
        this.myInputVariable.nativeElement.value = '';
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }
}
