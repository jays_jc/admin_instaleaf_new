import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListComponent } from './list-component/list.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';
import { AddProductsComponent } from './add-products/add-products.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { RatingComponent } from './rating-component/rating.component';
import { EditRatingComponent } from './editrating-component/editrating.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Items'
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'list',
        component: ListComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'add',
        component: AddUpdateComponent,
        data: {
          title: 'Add Items'
        }
      },
      {
        path: 'list/:id',
        component: ViewComponent,
        data: {
          title: 'View Items'
        }
      },
      {
        path: 'edit/:id',
        component: AddUpdateComponent,
        data: {
          title: 'Edit Items'
        }
      },
      {
        path: 'addproducts/:id',
        component: AddProductsComponent,
        data: {
          title: 'Add Product'
        }
      },
      {
        path: 'editproduct/:id',
        component: EditProductComponent,
        data: {
          title: 'Edit Product'
        }
      },
      {
        path: 'rating/:id',
        component: RatingComponent,
        data: {
          title: 'Rating Dispensary'
        }
      },
      {
        path: 'editrating/:id',
        component: EditRatingComponent,
        data: {
          title: 'Edit Rating'
        }
      },
      {
        path: 'bulkupload',
        component: BulkUploadComponent,
        data: {
          title: 'Bulk Upload'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,

  ],
  exports: [
    RouterModule,
    FormsModule,

  ]
})
export class DispensaryRoutingModule {}
