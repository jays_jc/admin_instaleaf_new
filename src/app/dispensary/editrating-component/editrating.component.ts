import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DispensaryService } from '../services/dispensary.service';
// import { CommanService } from '../../shared/services/comman.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  selector: 'app-editrating',
  templateUrl: './editrating.component.html',
  styleUrls: ['./editrating.component.scss'],
})
export class EditRatingComponent implements OnInit {
  private _host = environment.config.HOST;
  public object = {
    rating: '',
    details: '',
  };
  public ID: any;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _dispensaryService: DispensaryService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.ID = _activateRouter.snapshot.params['id'];
  }

  ngOnInit() {
    this.fetch();
  }

  fetch() {
    this.spinner.show();
    // this.isLoading = true;
    this._dispensaryService.getReview(this.ID).subscribe(
      (res) => {
        this.spinner.hide();
       
        if (res.success) {
          if(res.data.message=='No product found.'){
            this.spinner.hide();
            this.toastr.error(res.data.message);
          }else{
            this.object = res.data.key;
          }
         
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  /*If id exist then will update existing reviews and rating*/
  save() {
    this.spinner.show();
    // this.isLoading         = true;
    let data = JSON.parse(JSON.stringify(this.object));
    this._dispensaryService.editReviews(this.ID, data).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
          this.location.back();
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  trim(key) {
    if (this.object[key] && this.object[key][0] == ' ')
      this.object[key] = this.object[key].trim();
  }
}
