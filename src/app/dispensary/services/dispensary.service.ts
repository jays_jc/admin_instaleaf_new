import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { CommanService } from 'src/app/shared/comman.service';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
// import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
// import { CommanService } from '../../shared/services/comman.service';
// import tsConstants = require('./../../tsconstant');
@Injectable()
export class DispensaryService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService
  ) {}

  /*Use to fetch all dispensarys*/
  getAllList(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'dispensary_list', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let url =
    //   this._host +
    //   '/dispensary_list?count=' +
    //   rowsOnPage +
    //   '&page=' +
    //   activePage +
    //   '&sortBy=' +
    //   sortTrem +
    //   '&item=' +
    //   search +
    //   '&city=' +
    //   city;
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  /*Use to add new dispensary*/
  add(dispensary) {
    return this._http.post(this._host + 'add_dispensary', dispensary).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .post(this._host + '/add_dispensary', dispensary, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  /*Use to get dispensary with dispensary id*/
  get(dispensaryID) {
    return this._http
      .get(this._host + 'AllProductsByItem?id=' + dispensaryID)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .get(this._host + '/AllProductsByItem?id=' + dispensaryID, {
    //     headers: headers,
    //   })
    //   .map((res: Response) => res.json());
  }

  /*Use to update dispensary*/
  update(dispensary) {
    return this._http
      .put(this._host + `edit_dispensary/` + dispensary.id, dispensary)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .put(this._host + '/edit_dispensary/' + dispensary.id, dispensary, {
    //     headers: headers,
    //   })
    //   .map((res: Response) => res.json());
  }

  /*Use to verify, approve or expire dispensary*/
  delete(dispensaryID) {
    let body = { isDeleted: true };
    return this._http
      .post(this._host + 'delete_dispensary/' + dispensaryID, body)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/delete_dispensary/' + dispensaryID;
    // let body = { isDeleted: true };
    // return this._http
    //   .post(url, body, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  /*Use to verify, approve or expire dispensary*/
  changeStatus(dispensaryID, action) {
    return this._http
      .get(this._host + 'dispensarys/' + action + '/' + dispensaryID)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/dispensarys/' + action + '/' + dispensaryID;
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  addProducts(array = []) {
    let body = { data: array };
    return this._http.post(this._host + 'add_item_product/', body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/add_item_product';
    // let body = { data: array };
    // return this._http
    //   .post(url, body, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getProduct(id) {
    return this._http.get(this._host + 'getItemProduct?id=' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/getItemProduct?id=' + id;
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  editProducts(ID, body) {
    return this._http
      .put(
        this._host +
          `editItemProduct?id=` +
          ID +
          '&product_id' +
          body.product_id,
        body
      )
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url =
    //   this._host +
    //   '/editItemProduct?=id' +
    //   ID +
    //   '&product_id' +
    //   body.product_id;
    // return this._http
    //   .put(url, body, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getAllReviewList(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'itemreviews', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let date = new Date().getTime().toString();
    // let url =
    //   this._host +
    //   '/itemreviews?count=' +
    //   rowsOnPage +
    //   '&page=' +
    //   activePage +
    //   '&sortBy=' +
    //   sortTrem +
    //   '&product=' +
    //   search +
    //   '&id=' +
    //   ID;
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getReview(id) {
    return this._http.get(this._host + 'reviewdetail?id=' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/reviewdetail?id=' + id;
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  editReviews(ID, body) {
    return this._http.put(this._host + `edititemreviews?id=` + ID, body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/edititemreviews?id=' + ID;
    // return this._http
    //   .put(url, body, { headers: headers })
    //   .map((res: Response) => res.json());
  }
  /*Use to deletion of reviews*/
  deleteReviews(id) {
    let body = { isDeleted: true };
    return this._http
      .put(this._host + `itemReviewDeletion?id=` + id, body)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // console.log('id in service is ', id);
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/itemReviewDeletion?id=' + id;
    // let body = { isDeleted: true };
    // //return this._http.delete(url, { headers: headers }).map((res:Response) => res.json())
    // return this._http
    //   .put(url, body, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  fetchMaster() {
    return this._http.get(this._host + 'allmasterdispensary').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/allmasterdispensary';
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getAllCategory() {
    return this._http.get(this._host + 'allmasterdispensary').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to update dispensary*/
  updateMaster(dispensary) {
    let obj = {
      id: dispensary.id,
      master_id: dispensary.master_id,
    };
    return this._http.put(this._host + `addsubmaster`, dispensary).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .put(this._host + '/addsubmaster', dispensary, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  updateMainCategory(obj) {
    return this._http
      .put(
        this._host + `meregeCategory?id=` + obj.id + '&cat_id=' + obj.cat_id,
        {}
      )
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .put(
    //     this._host + '/meregeCategory?id=' + obj.id + '&cat_id=' + obj.cat_id,
    //     {},
    //     { headers: headers }
    //   )
    //   .map((res: Response) => res.json());
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
