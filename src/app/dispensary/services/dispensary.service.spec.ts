import { TestBed, inject } from '@angular/core/testing';

import { DispensaryService } from './dispensary.service';

describe('DispensaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DispensaryService]
    });
  });

  it('should ...', inject([DispensaryService], (service: DispensaryService) => {
    expect(service).toBeTruthy();
  }));
});
