import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { DispensaryService } from '../services/dispensary.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './rating.component.html',
})
export class RatingComponent implements OnInit {
  private _subscriberData: any;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    product: string;
    id: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      product: '',
      id: '',
    };
  public addEditDelete: boolean = true;
  ratingList: any = [];
  totalItems: any;

  public constructor(
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private _router: Router,
    private _dispensaryService: DispensaryService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.filters.id = this._route.snapshot.params['id'];
    this.filters.page = this._route.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getList();
  }

  routeitem(reviewId, path) {
    let route = '/dispensary/' + path + '/' + reviewId;
    this._router.navigate([route]);
  }

  viewUser(userID, path) {
    let route = path + '/' + userID;
    this._router.navigate([route]);
  }

  getList() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._dispensaryService
      .getAllReviewList(this.filters)
      .subscribe((response) => {
        if (response['data'].products.length == 0) {
          this.ratingList = [];
          this.spinner.hide();
          // this.isLoading = false;
        } else {
          this.totalItems = response.data.total;
          this.ratingList = response['data'].products.map((data) => {
            return {
              id: data.id,
              name: data.addedby,
              reviews: data.detail ? data.detail : '-',
              rating: data.rating ? data.rating : '-',
              storeLayoutRating: data.storeLayoutRating
                ? data.storeLayoutRating
                : '-',
              staffRating: data.staffRating ? data.staffRating : '-',
              date: data.createdAt,
              verified: data.verified,
            };
          });
          // this.isLoading = false;
          this.spinner.hide();
        }
      });
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/dispensary/rating/' + page;
    this.getList();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, {
      page: 1,
      search: this.filters.search,
    });
    this.getList();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getList();
  }

  remove(ID) {
    if (confirm('Do you want to delete ?')) {
      this.spinner.show();
      // this.isLoading = true;
      this._dispensaryService.deleteReviews(ID).subscribe(
        (res) => {
          if (res.success) {
            this.spinner.hide();
            // this.isLoading = false;
            // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
            // this.itemsTotal = this.itemsTotal - 1;

            // if( ! (this.itemsTotal >= start) ){
            //    this.activePage = this.activePage -1;
            //    if(this.activePage == 0) this.activePage = 1;
            // }
            /* reload page data */
            this.getList();
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
          } else {
            this.spinner.hide();
            this.toastr.error(res.error.message);
            // this.isLoading = false;
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }
}
