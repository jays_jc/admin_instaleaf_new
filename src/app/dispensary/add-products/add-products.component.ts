import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../environments/environment';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DispensaryService } from '../services/dispensary.service';
import { ImageResult } from 'ng2-imageupload';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  selector: 'app-add-products',
  templateUrl: './add-products.component.html',
  styleUrls: ['./add-products.component.scss'],
})
export class AddProductsComponent implements OnInit {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  private _host = environment.config.HOST;
  public products = [];
  public categories = [];
  public _index = [];
  private _selectedVehicleIndex = 0;
  public isLoading: boolean = false;
  public ID: any;
  public isPageLoading: boolean = false;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _dispensaryService: DispensaryService,
    private _commanService: CommanService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    this.ID = _activateRouter.snapshot.params['id'];
  }

  ngOnInit() {
    this.getAllCategories();
  }

  /*If id exist then will update existing dispensary otherwise will add new dispensary*/
  save() {
    this.spinner.show();
    // this.isLoading         = true;
    let data = JSON.parse(JSON.stringify(this.products));
    data.forEach((obj) => {
      obj.dispensary_id = this.ID;
    });
    this._dispensaryService.addProducts(data).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.toastr.success(res.data.message);
          // this._commanService.showAlert(res.data.message,'alert-success');
          // if(res.data.key.business_type == 'brand' || res.data.key.business_type == 'dispensary') {
          let route = '/dispensary/list';
          this._router.navigate([route]);
          // } else {
          // this._commanService.back();
          // }
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  getAllCategories() {
    this.spinner.show();
    this._commanService.getAllCategories().subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          let data = res.data.category;
          for (let index = 0; index < data.length; index++) {
            if (data[index].type == 'other') this.categories.push(data[index]);
          }
          this.addObject();
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  addObject() {
    let obj = {
      name: '',
      pre_roll: '',
      grams: '',
      Eighth: 0,
      quarter: '',
      half: '',
      ounce: '',
      category_id: this.categories[0]['id'],
      details: '',
      thc: '',
      image: '',
      isDeleted: false,
      cbd: '',
      status: 'active',
      brand_name: '',
      product_id: '',
      isForDelivery: false,
      meta_name: '',
      meta_desc: '',
    };
    if (this._index.length == 0) {
      this._index.push(1);
    } else {
      this._index.push(this._index[this._index.length - 1] + 1);
    }
    this.products.push(obj);
  }

  remove(index) {
    this._index.splice(index, 1);
    this.products.splice(index, 1);
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'product',
    };
    this.myInputVariable.nativeElement.value = '';
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.products[this._selectedVehicleIndex].image = res.data.fullPath;
        } else {
          window.scrollTo(0, 0);
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }
}
