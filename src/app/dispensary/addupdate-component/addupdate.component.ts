import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../environments/environment';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { DispensaryService } from '../services/dispensary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';
import { Location } from '@angular/common';
import { ImageResult } from 'ng2-imageupload';

@Component({
  templateUrl: 'addupdate.component.html',
  styleUrls: ['./addupdate.component.scss'],
})
export class AddUpdateComponent {
  @ViewChild('myInput', { static: false })
  myInputVariable: any;
  @ViewChild('myInput1', { static: true })
  myInputVariable1: any;
  private _host = environment.config.HOST;

  public object = {
    username: '',
    name: '',
    address: '',
    city: '',
    postal_code: null,
    email: '',
    mobile: null,
    website: '',
    about_us: '',
    image: '',
    logo: '',
    lat: 1,
    businessType: 'doctor',
    medical: false,
    recreation: false,
    lng: 1,
    isFeatured: false,
    scheduler: [],
    meta_name: '',
    meta_desc: '',
    isMaster: false,
    fbUrl: '',
    instagramUrl: '',
    twitterUrl: '',
    gabUrl: '',
  };

  public isLoading = false;
  public isPageLoading = true;
  public sellers = [];
  public ID: any;
  public business_types = ['brand', 'doctor', 'dispensary', 'producer'];
  public cities = [];
  public days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _dispensaryService: DispensaryService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private location: Location
  ) {
    /*Use to get all users*/
    // this.allCities();
  }

  ngOnInit(): void {
    this.ID = this._activateRouter.snapshot.params['id'];
    this.getDetail();
    this.getAllUsers();
    this.getAllCities();
  }

  getDetail() {
    this.spinner.show();
    if (this.ID) {
      this._dispensaryService.get(this.ID).subscribe(
        (res) => {
          if (res.success) {
            this.object = res.data.item[0];
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
          this.spinner.hide();
          // this.isPageLoading = false;
        },
        (err) => {
          this.spinner.hide();
          // this.isPageLoading = false;
        }
      );
    } else {
      this.days.forEach((key) => {
        let obj = {
          day: key,
          startTime: '9:00AM',
          closeTime: '5:00PM',
        };
        this.object.scheduler.push(obj);
      });
      this.spinner.hide();
      // this.isPageLoading = false;
    }
  }

  getAllUsers() {
    this._commanService.getAllUsers('U').subscribe(
      (res) => {
        if (res.success) {
          this.sellers = res.data.users;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {}
    );
  }

  getAllCities() {
    this._commanService.allCities().subscribe(
      (res) => {
        if (res.success) {
          let data = res.data.data;
          for (var i = 0; i < data.length; ++i) {
            // code...
            this.cities.push(data[i].name);
          }
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }
  /*If id exist then will update existing dispensary otherwise will add new dispensary*/
  save() {
    let data = JSON.parse(JSON.stringify(this.object));
    if (data.businessType != 'producer') {
      if (!data.isMaster && !data.postal_code) {
        this.toastr.error('Postal Code is Required.');
        // this._commanService.showAlert(
        //   'Postal Code is Required.',
        //   'alert-danger'
        // );
        return;
      }
    }
    this.spinner.show();
    // this.isLoading = true;
    if (this.ID) {
      this._dispensaryService.update(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message, 'alert-success');
            this.location.back();
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.checkAccessToken(res.error);
            // this._commanService.showAlert(res.error.message, 'alert-danger');
          }
        },
        (err) => {
          this.toastr.error(
            'There is Some Problem please try after some time.'
          );
          // this._commanService.showAlert(
          //   'There is Some Problem please try after some time.',
          //   'alert-danger'
          // );
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    } else {
      this._dispensaryService.add(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message, 'alert-success');
            if (
              res.data.key.businessType == 'brand' ||
              res.data.key.businessType == 'dispensary'
            ) {
              let route = '/items/addproducts/' + res.data.key.id;
              this._router.navigate([route]);
            } else {
              this.location.back();
            }
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.checkAccessToken(res.error);
            // this._commanService.showAlert(res.error.message, 'alert-danger');
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  trim(key) {
    if (this.object[key] && this.object[key][0] == ' ')
      this.object[key] = this.object[key].trim();
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'dispensary',
    };
   
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.object.image = res.data.fullPath;
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
        this.myInputVariable.nativeElement.value = '';
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  removeImage(image) {
    this.object.image = '';
  }

  // Use to View Image Prompt
  viewImage(imageUrl) {
    // this._dialogService.addDialog(ViewCropImageComponent, {
    //   imageUrl:imageUrl
    // }).subscribe((res)=>{ });
  }

  onDateChanged(event): void {
    // date selected
  }
}
