import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { DispensaryService } from '../services/dispensary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ImageResult } from 'ng2-imageupload';
import { CommanService } from 'src/app/shared/comman.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-bulk-upload',
  templateUrl: './bulk-upload.component.html',
  styleUrls: ['./bulk-upload.component.scss'],
})
export class BulkUploadComponent {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;

  public isLoading = false;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _dispensaryService: DispensaryService,
    public _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private location: Location
  ) {}

  ngOnInit() {}

  // uploadCSV(event) {
  //     let object = {
  //         data:event
  //     }
  //     this.myInputVariable.nativeElement.value = "";
  //     this.isLoading = true;
  //     this._commanService.uploadCSV(object).subscribe( res => {
  //         this.isLoading = false;
  //         if(res.success) {

  //         } else {
  //             this._commanService.showAlert(res.error.message,'alert-danger');
  //         }
  //     },err => { this.isLoading = false; });
  // }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
    };
   
    if (confirm('Are you sure you want to upload ?')) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.uploadCSV(object).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success('Bulk Upload Completed Successfully.');
            // this._commanService.showAlert("Bulk Upload Completed Successfully.",'alert-success');
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
          this.myInputVariable.nativeElement.value = '';
        },
        (err) => {
          this.isLoading = false;
        }
      );
    }
  }

  back() {
    this.location.back();
  }
}
