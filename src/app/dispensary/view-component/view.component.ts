import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { CategoryService } from './../../category/services/category.service';
import { environment } from './../../../environments/environment';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DispensaryService } from '../services/dispensary.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { CommanService } from 'src/app/shared/comman.service';
@Component({
  templateUrl: 'view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent {
  private _host = environment.config.HOST;
  // private _dateFormat = tsConstants.DATE_FORMAT;

  public cropID = '';
  public object = {};
  public isPageLoading = true;
  public isLoading = false;
  public addEditDelete: boolean = false;
  public products = [];
  mainCategories: any = [];
  public activePage = 1;
  public searchTerm = '';
  public sortTrem = '';
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
  } = {
    page: 1,
    count: 2000,
    search: '',
    sortBy: 'createdAt desc',
    date: new Date().getTime().toString(),
  };

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _dispensaryService: DispensaryService,
    private _commanService: CommanService,
    private CategoryService: CategoryService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    let actions = this._commanService.getActions();
    this.addEditDelete = true;

    this.cropID = _route.snapshot.params['id'];
    this.spinner.show();
    this._dispensaryService.get(this.cropID).subscribe(
      (res) => {
        if (res.success) {
         
          // this.isPageLoading = false;
          this.object = res.data.item[0];
          this.products = res.data.key;
          this.products.forEach((x) => {
            if (!x.instaleaf_categoryId) {
              x.instaleaf_categoryId = '';
            }
          });
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        // this.isPageLoading = false;
      }
    );
    this.getAllCategories();
  }

  route(cropid, path) {
    let route = '/dispensary/' + path + '/' + cropid;
    this._router.navigate([route]);
  }

  getAllCategories(): void {
    this.CategoryService.getAllCategories(this.filters).subscribe(
      (res) => {
        // this.isLoading     = false;
        // this.isPageLoading = false;
        console.log(res);
        if (res.success) {
          this.mainCategories = res.data.category;
          // this.itemsTotal    = res.data.total;
          // this.showAlert();
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {
        // this.isLoading     = false;
        // this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      }
    );
  }

  changeMainCtegory(id, mainId) {
    if (id && mainId) {
      let data = {
        id: id,
        cat_id: mainId,
      };
      this.spinner.show();
      this._dispensaryService.updateMainCategory(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success(res.message);
            // this._commanService.showAlert(res.message, 'alert-success');
            // this._commanService.back();
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.checkAccessToken(res.error);
            // this._commanService.showAlert(res.error.message, 'alert-danger');
          }
        },
        (err) => {
          this.toastr.error(
            'There is Some Problem please try after some time.'
          );
          this.spinner.hide();
          // this._commanService.showAlert(
          //   'There is Some Problem please try after some time.',
          //   'alert-danger'
          // );
          // this.isLoading = false;
        }
      );
    }
  }
}
