import { NgxSpinnerModule } from 'ngx-spinner';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list-component/list.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';
import { DispensaryRoutingModule } from './dispensary-routing.module';
import { DispensaryService } from './services/dispensary.service';
import { FormsModule } from '@angular/forms';
import { AddProductsComponent } from './add-products/add-products.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { EditRatingComponent } from './editrating-component/editrating.component';
import { RatingComponent } from './rating-component/rating.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CategoryService } from '../category/services/category.service';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
    DispensaryRoutingModule,
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    NgxSpinnerModule,
    ImageUploadModule
  ],
  providers: [DispensaryService, CategoryService],
  declarations: [
    ListComponent,
    AddUpdateComponent,
    ViewComponent,
    AddProductsComponent,
    EditProductComponent,
    RatingComponent,
    EditRatingComponent,
    BulkUploadComponent,
  ],
  //Don't forget add component to entryComponents section
  entryComponents: [],
})
export class DispensaryModule {}
