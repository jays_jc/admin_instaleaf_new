import { CommanService } from 'src/app/shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { DispensaryService } from '../services/dispensary.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
@Component({
  selector: 'app-crops',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    item: string;
    sortBy: string;
    city: string;
  } = {
      page: 0,
      count: 25,
      item: '',
      sortBy: 'createdAt desc',
      city: '',
    };
  cities: any = [];
  masterDispensaries: any = [];
  itemsList: any = [];
  private _path: any;
  public addEditDelete: boolean = true;
  totalItems: any;

  public constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _dispensaryService: DispensaryService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService
  ) {
    this._path = this._route.snapshot['_urlSegment'].segments[0].path;

    let actions = this._commanService.getActions();

    // this.isPageLoading = true;
  }

  ngOnInit(): void {
    this.filters.page = this._route.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getItemsList();
    this.allCities();
    this.fetchMaster();
  }

  getItemsList() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._dispensaryService
      .getAllList(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.itemsList = [];
          // this.spinner.hide();
          // this.isLoading = false;
        } else {
          this.totalItems = response.data.total;
          response.data.data.forEach((obj,i)=>{
            if(!obj.master_id)response.data.data[i]['master_id'] = ""
        })
          this.itemsList = response['data'].data.map((data) => {
            return {
              id: data.id,
              name: data.name,
              address: data.address
                ? data.address + ', ' + data.postal_code
                : '-',
              businesstype: data.businesstype,
              city: data.city,
              reviews: data.reviews ? data.reviews : '-',
              rating: data.rating ? data.rating : '-',
              storeLayoutRating: data.storeLayoutRating
                ? data.storeLayoutRating
                : '-',
              staffRating: data.staffRating ? data.staffRating : '-',
              date: data.createdAt,
              verified: data.verified,
              isMaster:data.isMaster
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  setPage(e) {
    // console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/products/list/' + page;
    this.getItemsList();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, {
      page: 1,
      item: this.filters.item,
      city: this.filters.city,
    });
    this.getItemsList();
  }

  clearValue() {
    this.page = 0;
    this.filters.item = '';
    Object.assign(this.filters, { page: 1, item: this.filters.item });
    this.getItemsList();
  }

  route(cropID, path) {
    let route = '/dispensary/' + path + '/' + cropID;
    this._router.navigate([route]);
  }

  viewUser(userID, path) {
    let route = path + '/' + userID;
    this._router.navigate([route]);
  }

  remove(ID) {
    if (confirm('Do you want to delete?')) {
      this.spinner.show();
      // this.isLoading = true;
      this._dispensaryService.delete(ID).subscribe(
        (res) => {
          if (res.success) {
            // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
            // this.itemsTotal = this.itemsTotal - 1;

            // if( ! (this.itemsTotal >= start) ){
            //    this.activePage = this.activePage -1;
            //    if(this.activePage == 0) this.activePage = 1;
            // }
            /* reload page data */
            this.getItemsList();
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
          } else {
            this.spinner.hide();
            this.toastr.error(res.error.message);
            // this.isLoading = false;
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  fetchMaster() {
    this._dispensaryService.fetchMaster().subscribe(
      (res) => {
        if (res.success) {
          this.masterDispensaries = res.data.dispensaryList;
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  /* Function use to approve dispensary with dispensary id*/
  changeStatus(ID, action) {
    if (confirm('Do you want to Approve Dispensary?')) {
      this.spinner.show();
      // this.isLoading = true;
      this._dispensaryService.changeStatus(ID, action).subscribe(
        (res) => {
          if (res.success) {
            // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
            // this.itemsTotal = this.itemsTotal - 1;

            // if( ! (this.itemsTotal >= start) ){
            //    this.activePage = this.activePage -1
            // }
            // if(this.activePage == 0) this.activePage = 1;
            /* reload page. */
            this.getItemsList();
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
          } else {
            this.spinner.hide();
            // this.isLoading = false;
            this.toastr.error(res.error.message);
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  allCities() {
    this._commanService.allCities().subscribe(
      (res) => {
        if (res.success) {
          let data = res.data.data;
          for (var i = 0; i < data.length; ++i) {
            // code...
            this.cities.push(data[i].name);
          }
        } else {
          this.toastr.error(res.error);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  changeMasterDispensary(obj) {
    if (obj.master_id) {
      this.spinner.show();
      // this.isLoading = true;
      this._dispensaryService.updateMaster(obj).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success('Updated Successfully.');
            // this._commanService.showAlert("Updated Successfully.",'alert-success');
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.checkAccessToken(res.error);
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
        },
        (err) => {
          this.toastr.error(
            'There is Some Problem please try after some time.'
          );
          // this._commanService.showAlert('There is Some Problem please try after some time.','alert-danger');
          // this.isLoading = false;
          this.spinner.hide();
        }
      );
    }
  }
}
