import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


// import { NG2DataTableModule } from "angular2-datatable-pagination";
import { DriverService } from './services/driver.service';
// import { SharedModule } from '../shared/shared.module';
// import { CustomFormsModule } from 'ng2-validation';
// import { FlashMessagesModule } from 'ngx-flash-messages';
import { OrdersRoutingModule } from './orders-routing.module';

import { ListComponent } from './list-component/list.component';
import { ViewComponent } from './view-component/view.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  imports: [
    CommonModule,
    OrdersRoutingModule,
    //  NG2DataTableModule,
    //  CustomFormsModule,
    //  FlashMessagesModule,
    //  SharedModule,
    //  ImageUploadModule
    NgxSpinnerModule,
    NgxDatatableModule
  ],
  providers: [
    DriverService
  ],
  declarations: [
    ListComponent,
    ViewComponent
  ]
})
export class OrdersModule { }
