import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
// import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
// import { CommanService } from '../../shared/services/comman.service';
// import tsConstants = require('./../../tsconstant');

@Injectable()
export class DriverService {
  private _host = environment.config.HOST;

  constructor(private _http: HttpClient) {}

  getAllOrders(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'order', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  get(id) {
    return this._http.get(this._host + 'order/' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to get all Users*/
  // getAllUsers(rowsOnPage, activePage, sortTrem, search = '', roles = 'U') {
  //     let date =  new Date().getTime().toString();
  //     let url = this._host +'/order?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&roles='+roles+'&search='+search+'&date='+date;

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  // }

  /*User to get user detail with ID*/
  // get(userid) {

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.get(this._host +'/order/'+ userid, { headers: headers }).map((res:Response) => res.json())
  // }

  /*Use to update user detail with there ID*/
  // update(user) {

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.put(this._host +'/order', user, { headers: headers }).map((res:Response) => res.json())
  // }
  /*Use to update user detail with there ID*/
  order_update_supplier(user) {
    return this._http.get(this._host + 'order_update_supplier/' + user).pipe(
      map((response: any) => {
        return response.json();
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/order_update_supplier', user, { headers: headers }).map((res:Response) => res.json())
  }

  sendingInvoice(order) {
    return this._http.get(this._host + 'sendinvoice/' + order).pipe(
      map((response: any) => {
        return response.json();
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/sendinvoice/'+ order, { headers: headers }).map((res:Response) => res.json())
  }

  sendingInvoiceToSupplier(order, order_id, gst, delivery) {
    return this._http
      .get(
        this._host +
          'sendinvoicetosupplier/' +
          order +
          '/' +
          order_id +
          '/' +
          gst +
          '/' +
          delivery
      )
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/sendinvoicetosupplier/'+ order+'/'+order_id+'/'+gst+'/'+delivery, { headers: headers }).map((res:Response) => res.json())
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
