import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { DriverService } from '../services/driver.service';
// import { CommanService } from '../../shared/services/comman.service';
// import { CookieService } from 'ngx-cookie';
// import { Angular2Csv } from 'angular2-csv/Angular2-csv';
// import { FlashMessagesService } from 'ngx-flash-messages';
import { NgxSpinnerService } from 'ngx-spinner';
// import tsMessages  = require('../../tsmessage');
import { ColumnMode } from '@swimlane/ngx-datatable';
import { CommanService } from 'src/app/shared/comman.service';
declare let jsPDF;

@Component({
  selector: 'app-driver',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    roles: string;
    date: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      roles: 'DRIVER',
      date: new Date().getTime().toString(),
    };
  orderList: any = [];
  totalItems: any;

  public constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _driverService: DriverService,
    private _commanService: CommanService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // if (this._activateRouter.snapshot.params['page']) {
    //   this.page = this.filters.page - 1;
    // }

    Object.assign(this.filters, { page: this.filters.page });
    this.getOrders();
  }

  getOrders() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._driverService
      .getAllOrders(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.orderList = [];
          this._commanService.checkAccessToken(response.error);
          this.toastr.error(response.error.message);
          if (response.error.code == 401) {
            this._router.navigate(['/auth/login', { data: true }]);
          }

          // this.isLoading = false;
        } else {
          // console.log(response);
          this.totalItems = response.data.total;
          this.orderList = response['data'].data.map((data) => {
            return {
              id: data.id,
              orderId: data.order_number,
              price: data.totalprice,
              user: data.addedBy,
              date: data.createdAt,
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          this._commanService.checkAccessToken(error);
          this.toastr.error(error.message);
          if (error.code == 401) {
            this._router.navigate(['/auth/login', { data: true }]);
          }
        }
      );
  }

  view(userID) {
    let route = '/orders/list/' + userID;
    this._router.navigate([route]);
  }

  // changeStatus(user) {

  //     if(confirm("Do you want to Mark as deliver ?")) {
  //         this.isLoading = true;
  //         user['order_status'] = "Delivered"
  //         this._driverService.update(user).subscribe(res => {
  //             if(res.success) {
  //                 this.response  = res;
  //                 let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
  //                 this.itemsTotal = this.itemsTotal - 1;

  //                 if( ! (this.itemsTotal >= start) ){
  //                    this.activePage = this.activePage -1
  //                 }
  //                 this._commanService.showAlert(res.data.message,'alert-success');
  //                 /* reload page. */
  //                 this.getUsers();
  //             } else {
  //                 this.isLoading    = false;
  //                 this._commanService.showAlert(res.error.message,'alert-danger');
  //                 this._commanService.checkAccessToken(res.error);
  //             }
  //         },err => {
  //             this.isLoading = false;
  //         });
  //     }
  // }

  // sendInvoice(order) {
  //     if(confirm("Do you want to send invoice to supplier ?")) {
  //         this.isLoading = true;
  //         //user['isInvoice'] = "Sent"
  //         this._driverService.sendingInvoice(order).subscribe(res => {
  //             if(res.success) {
  //                 this.response  = res;
  //                 this._commanService.showAlert(res.data.message,'alert-success');
  //                 /* reload page. */
  //                 this.getUsers();
  //             } else {
  //                 this.isLoading    = false;
  //                 this._commanService.showAlert(res.error.message,'alert-danger');
  //                 this._commanService.checkAccessToken(res.error);
  //             }
  //         },err => {
  //             this.isLoading = false;
  //         });
  //     }
  // }

  /*Get all Users */
  // getUsers(): void {
  //     this._driverService.getAllUsers( this.rowsOnPage, this.activePage, this.sortTrem,  this.searchTerm, this.roles ).subscribe(res => {
  //         this.isLoading     = false;
  //         this.isPageLoading = false;
  //         if(res.success) {
  //             this.data          = res.data.data;
  //             this.itemsTotal    = res.data.total;
  //             if(res.data.users && res.data.users.length == 1) this.activePage = 1;
  //         } else {
  //             this._commanService.checkAccessToken(res.error);
  //         }
  //     },err => {
  //         this.isLoading     = false;
  //         this.isPageLoading = false;
  //    });
  // }

  // public onPageChange(event) {
  //     this.isLoading     = true;
  //     this.rowsOnPage = event.rowsOnPage;
  //     this.activePage = event.activePage;
  //     this.getUsers();
  // }

  // public onRowsChange( event ): void {
  //     this.isLoading  = true;
  //     this.rowsOnPage = this.itemsOnPage;
  //     this.activePage = 1;
  //     this.getUsers();
  // }

  // public onSortOrder(event) {
  //     this.sortTrem = this.sortBy+' '+this.sortOrder;
  //     this.isLoading  = true;
  //     this.getUsers();
  // }

  // public search( event, element = 'input' ) {
  //     if( element == 'input' ) {
  //         if(event.keyCode == 13 || this.searchTerm == '') {
  //             this.searchTerm = this.searchTerm.trim();
  //             this.isLoading  = true;
  //             this.getUsers();
  //             this.activePage = 1;
  //             this.getUsers();
  //         }
  //     }else{
  //         this.searchTerm = this.searchTerm.trim();
  //         this.isLoading  = true;
  //         this.getUsers();
  //         this.activePage = 1;
  //         this.getUsers();
  //     }
  // }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/order/list/' + this.page;
    this.getOrders();
    // this.router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getOrders();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getOrders();
  }
}
