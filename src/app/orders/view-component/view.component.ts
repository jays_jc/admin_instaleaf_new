import { environment } from './../../../environments/environment';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DriverService } from '../services/driver.service';
import { CommanService } from 'src/app/shared/comman.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  templateUrl: 'view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent {
  private _host = environment.config.HOST;

  public gst;
  public delivery_charge;
  public userID = '';
  public order: any = {
    shippingDetail: {},
    billingDetail: {},
  };
  public isLoading = false;
  public isPageLoading = true;
  public addEditDelete = false;
  public response: any;
  public groupByObject: any = [];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: DriverService,
    private _commanService: CommanService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
  ) {
    this._commanService.getSettings().subscribe(
      (res) => {
        if (res.success) {
          this.gst = res.data[0].gst;
          this.delivery_charge = res.data[0].delivery_charge;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {
        this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      }
    );

    // let actions = this._commanService.getActions();
    // if(actions["type"] == 'SA' || actions['users']['addEditDelete']) this.addEditDelete = true;
    this.route.queryParams.subscribe((params) => {
      this.userID = params['id'];
      this.getDetail();
    });
  }

  getDetail() {
    this.spinner.show();
    this._userService.get(this.userID).subscribe(
      (res) => {
        if (res.success) {
          this.order = res.data[0];
          this.groupByObject = this.groupBy(this.order['order_detail']);
          this.groupByObject = Object.keys(this.groupByObject).map(
            (key) => this.groupByObject[key]
          );
          this.isPageLoading = false;
          this.spinner.hide();
        } else {
          //    this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {
        this.isPageLoading = false;
        this.spinner.hide();
      }
    );
  }

  sendInvoice(order, gst, delivery) {
    if (confirm('Do you want to send invoice to supplier ?')) {
      this.spinner.show();
      // this.isLoading = true;
      //user['isInvoice'] = "Sent"
      gst = gst ? gst : false;
      delivery = delivery ? delivery : false;
      this._userService
        .sendingInvoiceToSupplier(order, this.userID, gst, delivery)
        .subscribe(
          (res) => {
            if (res.success) {
              this.isLoading = false;
              this.response = res;
              this.toastr.success(res.data.message);
              //   this._commanService.showAlert(res.data.message,'alert-success');
              /* reload page. */
              //this.getUsers();
            } else {
              this.isLoading = false;
              this.toastr.error(res.error.message);
              //   this._commanService.showAlert(res.error.message,'alert-danger');
              //   this._commanService.checkAccessToken(res.error);
            }
            this.spinner.hide();
          },
          (err) => {
            this.spinner.hide();
            this.toastr.error(err);
            // this.isLoading = false;
          }
        );
    }
  }

  groupBy(arr) {
    let group = arr.reduce((r, a) => {
      r[a.product_id.supplier_id] = [...(r[a.product_id.supplier_id] || []), a];
      return r;
    }, {});

    return group;
  }

  viewInvoice(orders) {
    let message;
    let grandTotal;
    let counter = 0;
    message = '<!DOCTYPE html>';
    message += '<html>';
    message += '<body> ';
    message +=
      '<div style="font-family: "Roboto", sans-serif;  background: white;padding: 1rem 0px;width: 100%;margin: auto;display: block;" id="invoice">';
    // message += '<div class="invoice overflow-auto">';
    // message += '<div style="min-width: 600px">';
    // message += '<h2 style="color: #000000f2; text-align: center;"><strong>Congrats you got a new order!</strong></h2>';
    // message += '<h2 style="color: #000000f2; text-align: center;"><strong>Please Click link below to accept order and agree to arrange necessary delivery of products:</strong></h2>';
    // message += '<div style="background: #adecc8 !important;width: 100%;padding: 10px;text-align: center;margin: auto !important; display: block;max-width: 50%;"><a href="' + this._host + "/acceptorder/" + orders.id + '" target="_blank" ><b>Accept Order</b></a></div>';
    message +=
      '<header style="padding: 10px 0;margin-bottom: 20px;border-bottom: 1px solid #285941;width: 100%;float: left;">';
    message += '<div class="row">';
    message += '<div class="col" style="width:20%;float: left;">';
    message +=
      '<a target="_blank" href="https://instaleaf.ca/"><img src="https://instaleaf.ca/assets/img/logo.png" data-holder-rendered="true" style="width: 160px;"/></a></div>';
    message +=
      '<div class="col company-details" style="width:80%;float: right;text-align: right;"><h2 style="color: #0000007d;"><strong>Tax Invoice/Bill of Supply/Cash Memo</strong></h2><p>(Original for Recipient)</p></div></div></header>';
    message +=
      '<main><div class="row contacts" style="width:100%;margin-bottom: 25px;overflow: hidden;clear: both;">';

    message +=
      '<div class="col invoice-to" style="width:50%;float: left;text-align: left;">';
    message +=
      '<div style="font-size: 16px;font-weight: bold;" class="invoi-title">Sold By:</div>';
    message +=
      '<p style="width: 215px;font-size: 16px;line-height: 25px;">Instaleaf.ca<br> Address- 1803 Bowness Rd, nw, calgary, Alberta <br> Postal Code -T2N-3K5<br> Phone- (587) 434-4794</p></div>';

    message +=
      '<div class="col invoice-details" style="width:50%;float: left;text-align: right;">';
    message +=
      '<div style="font-size: 16px;font-weight: bold;" class="invoi-title">Billing Address:</div>';
    message +=
      '<p style="font-size: 16px;line-height: 25px;width: 215px;float: right;">' +
      this.order.billingDetail.fullName +
      '<br>' +
      this.order.billingDetail.address2 +
      '<br>' +
      this.order.billingDetail.city +
      ',' +
      this.order.billingDetail.state +
      '<br>' +
      this.order.billingDetail.pincode +
      '</p></div></div>';
    message +=
      '<div class="col invoice-to" style="text-align: left;width:50%;float: left;"><span style="font-size: 16px;font-weight: bold;" class="invoi-title">PAN No:</span><span> AALCA0171E</span><br></div>';
    message +=
      '<div class="row contacts"style="width:100%;margin-bottom: 25px;overflow: hidden;clear: both;">';

    message +=
      '<div class="col invoice-details" style="text-align: right;width:100%;float: left;">';
    message +=
      '<div style="font-size: 16px;font-weight: bold;" class="invoi-title">Shipping Address:</div><p style="font-size: 16px;line-height: 25px;width: 215px;float: right;">' +
      this.order.shippingDetail.fullName +
      '<br>' +
      this.order.shippingDetail.address2 +
      '<br>' +
      this.order.shippingDetail.city +
      ',' +
      this.order.shippingDetail.state +
      '<br>' +
      this.order.shippingDetail.pincode +
      '</p></div></div>';
    message +=
      '<span style="font-size: 16px;font-weight: bold;" class="invoi-title">Order Number:</span><span>' +
      this.order.order_number +
      '</span><br/><span style="font-size: 16px;font-weight: bold;">Order Date:</span><span>' +
      this.order.order_date +
      '</span></div></div>';
    message +=
      '<div class="row contacts"style="width:100%;"><div class="col invoice-to" style="width:100%;float: left;">';

    message +=
      '<table style="width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;display: block;" border="0" cellspacing="0" cellpadding="0"><thead>';
    message +=
      '<tr><th style="width:10%;padding: 0px;background: #eee;border-bottom: 1px solid #fff;white-space: nowrap;font-weight: 400;font-size: 14px;">SL No.</th>';
    message +=
      '<th style="width:20%;padding: 5px;background: #eee;border-bottom: 1px solid #fff;white-space: nowrap;font-weight: 400;font-size: 14px;width:50%;">Description</th>';
    message +=
      '<th style="width:10%;padding: 5px;background: #eee;border-bottom: 1px solid #fff;white-space: nowrap;font-weight: 400;font-size: 14px;">Total Price </th>';
    message +=
      '<th style="width:10%;padding: 5px;background: #eee;border-bottom: 1px solid #fff;white-space: nowrap;font-weight: 400;font-size: 14px;max-width: 95px;">Reg Margin (' +
      orders[0].product_id.supplier_commission +
      ')</th>';
    //message += '<th style="width:10%;padding: 5px;background: #eee;border-bottom: 1px solid #fff;white-space: nowrap;font-weight: 400;padding: 16px;font-size: 14px; max-width: 8px !important;font-size: 14px;">Sale (5%)</th>';
    message +=
      '<th style="width:0%; padding: 5px;background: #eee;border-bottom: 1px solid #fff;white-space: nowrap;font-weight: 400;font-size: 14px;">Qty</th>';
    message +=
      '<th style="padding: 5px;background: #eee;border-bottom: 1px solid #fff;white-space: nowrap;font-weight: 400;font-size: 14px;width:10%;">Total</th></tr></thead>';

    let subSum = 0;
    let j = 0;

    for (var i = 0; i < orders.length; i++) {
      j += 1;
      var newQty =
        parseInt(orders[i].product_id.quantity) - parseInt(orders[i].quantity);
      //Marketplaceproduct.update({ id: orders[i].product_id.id }, { quantity: newQty }).then(function (result) { })
      message +=
        '<tr><td style="color: #fff;font-size: 1.6em;text-align: center;background: #285941;border-bottom: 1px solid #fff;" class="no">' +
        j +
        '</td>';
      message +=
        '<td style="padding: 10px;font-size: 12px;background: #eee;border-bottom: 1px solid #fff;" class="text-left"><h3>' +
        orders[i].product_id.name +
        '</h3></td>';
      message +=
        '<td style="border-bottom: 1px solid #fff;background: #ddd;text-align: center;font-size: 12px;" class="unit">CAD $' +
        orders[i].product_id.price.toFixed(2) +
        '</td>';
      message +=
        '<td style="padding: 10px;text-align: center;font-size: 12px;background: #eee;border-bottom: 1px solid #fff;">CAD $' +
        (
          orders[i].product_id.price *
          (orders[i].product_id.supplier_commission / 100)
        ).toFixed(2) +
        '</td>';
      //message += '<td style="padding: 10px;text-align: center;font-size: 12px;background: #eee;    border-bottom: 1px solid #fff;">$26.6</td>';
      message +=
        '<td style="padding: 10px;text-align: center;font-size: 12px;background: #eee;border-bottom: 1px solid #fff;">' +
        orders[i].quantity +
        '</td>';

      // message += '<td align="center">$ ' + orders[i].product_id.price + '</td><td align="center">$ ' + (parseFloat(orders[i].quantity) * parseFloat(orders[i].product_id.price)) + '</td></tr>';
      // message += '<td align="center">$ ' + (parseFloat(orders[i].quantity) * (orders[i].product_id.price *(70/100))) + '</td></tr>';
      grandTotal =
        parseFloat(orders[i].quantity) *
        (orders[i].product_id.price *
          (orders[i].product_id.supplier_commission / 100));
      message +=
        '<td style="background: #285941;color: #fff;border-bottom: 1px solid #fff;" class="total">';
      message +=
        '<p style="text-align: center;background: white;color: #616161; width: 100%;max-width: 70%; display: block;margin: auto;padding: 3px;" class="bg">CAD $' +
        grandTotal.toFixed(2) +
        '</p></td></tr></tbody>';
      if (orders.length > 1) {
        // console.log("in if")
        subSum += parseFloat(grandTotal);
      } else {
        // console.log("in ese")
        subSum = parseFloat(grandTotal);
      }
      // console.log('subSum',subSum,grandTotal, j)
      if (orders.length == j) {
        // message += '<td style="background: #285941;color: #fff;border-bottom: 1px solid #fff;" class="total">';
        // message += '<p style="text-align: center;background: white;color: #616161; width: 100%;max-width: 70%; display: block;margin: auto;padding: 3px;" class="bg">CAD $'+grandTotal+'</p></td></tr></tbody>';
        //message += '<tr><td colspan="3" align="center">Grant Total : </td><td align="center">' + grandTotal + '</td></tr>';

        message +=
          '<tfoot><tr><td style="background: 0 0;border: none;white-space: nowrap;text-align: right;padding: 10px 20px;font-size: 1.2em;border-top: 1px solid #aaa;" colspan="3"></td>';
        message +=
          '<td style="background: 0 0;border: none;white-space: nowrap;text-align: right;padding: 10px 20px;font-size: 1.2em;" colspan="2">SUBTOTAL</td>';
        message +=
          '<td style="background: 0 0;border: none;white-space: nowrap;text-align: right;padding: 10px 20px;font-size: 1.2em;">CAD $' +
          subSum.toFixed(2) +
          '</td></tr>';

        if (orders.delivery) {
          // message += '<td style="background: 0 0;border-bottom: none;white-space: nowrap; text-align: right;padding: 10px 20px;font-size: 1.2em;border-top: 1px solid #aaa;" colspan="2">Delivery Charges</td>';
          message +=
            '<tr><td style="background: 0 0;border: none;white-space:nowrap;text-align: right;padding: 10px 20px;font-size: 1.2em;border: none;" colspan="5">Delivery Charges</td>';
          message +=
            '<td style="background: 0 0;border-bottom: none;white-space: nowrap; text-align: right;padding: 10px 20px;font-size: 1.2em;border-top: 1px solid #aaa;">CAD $' +
            this.delivery_charge.toFixed(2) +
            '</td></tr>';
        }

        // message += '<tr><td style="background: 0 0;border: none;white-space:nowrap;text-align: right;padding: 10px 20px;font-size: 1.2em;border: none;" colspan="3"></td>';
        if (orders.gst) {
          message +=
            '<tr><td style="background: 0 0;border-bottom: none;white-space: nowrap; text-align: right;padding: 10px 20px;font-size: 1.2em;border: none;" colspan="5">GST(' +
            this.gst +
            '%)</td>';
          message +=
            '<td style="background: 0 0;border-bottom: none;white-space: nowrap; text-align: right;padding: 10px 20px;font-size: 1.2em;border-top: 1px solid #aaa;">CAD $' +
            ((subSum * this.gst) / 100).toFixed(2) +
            '</td></tr>';
        }

        message +=
          '<tr><td style="color: #3989c6;background: 0 0;border-bottom: none;   white-space: nowrap;text-align: right;padding: 10px 20px;font-size: 1.4em;" colspan="3"></td>';
        message +=
          '<td style="color: #3989c6;background: 0 0;border-bottom: none;   white-space: nowrap;text-align: right;padding: 10px 20px;font-size: 1.4em;border-top: 1px solid #3989c6;" colspan="2">GRAND TOTAL</td>';
        message +=
          '<td style="color: #3989c6;font-size: 1.4em;background: 0 0;   border-bottom: none;white-space: nowrap;text-align: right;padding: 10px 20px;border-top: 1px solid #3989c6;">CAD $' +
          (
            parseFloat(subSum + (orders.delivery ? this.delivery_charge : 0)) +
            (orders.gst ? (subSum * this.gst) / 100 : 0)
          ).toFixed(2) +
          '</td></tr></tfoot>';
        message += '</table>';

        message +=
          '<div style="margin-top: -100px;font-size: 2em;margin-bottom: 50px;" class="thanks pl-1">Thank you!</div></main>';
        message +=
          '<footer style="width: 100%;text-align: center;color: #777;background: #adecc8 !important;border-top: 1px solid #aaa;padding: 8px 0;" class="lightgreen">Invoice was created on a computer and is valid without the signature and seal.</footer>';
        message +=
          "<p>Don't hesitate to each out to the contact details below if you have any questions regarding order.</p>";
        message +=
          '<p style="margin: 0px;">All the Best</p><p style="margin: 0px;"><b>Instaleaf Support</b></p><p style="margin: 0px;">Phone-(587)434-4794</p><p style="margin: 0px;">Email-Shopping@instaleaf.ca</p><a target="_blank" href="instaleaf.ca">instaleaf.ca</a><br /><a target="_blank" href="https://instaleaf.ca/"><img src="https://instaleaf.ca/assets/img/logo.png" data-holder-rendered="true" style="width: 160px; margin-top:10px;"/></a>';
        message += '</div><div></div></div></div></body></html>';
      }
    }
    let printContents, popupWin;
    printContents = message;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(printContents);
    popupWin.document.close();
  }

  changeStatus(id) {
    let obj = {};
    obj['order_id'] = this.userID;
    obj['id'] = id;
    if (confirm('Do you want to Mark as deliver ?')) {
      this.isLoading = true;
      this._userService.order_update_supplier(obj).subscribe(
        (res) => {
          if (res.success) {
            this.response = res;
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
            /* reload page. */
            this.isLoading = false;
            this.getDetail();
          } else {
            this.isLoading = false;
            this.toastr.success(res.error.message);
            // this._commanService.showAlert(res.error.message,'alert-danger');
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.isLoading = false;
        }
      );
    }
  }
}
