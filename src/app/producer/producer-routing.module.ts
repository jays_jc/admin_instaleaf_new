import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Producer'
    },
    children: [
     {
        path: '',
        component: ListCategoryComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'list',
        component: ListCategoryComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'add',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Add Producer'
        }
      },
      {
        path: 'edit/:id',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Edit Producer'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
    RouterModule,
    FormsModule,
  ]
})
export class ProducerRoutingModule {}
