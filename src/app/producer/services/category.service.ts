import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators/map';
import { throwError } from 'rxjs';

@Injectable()
export class CategoryService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService
  ) {}

  /*Use to fetch all Inputs*/
  getAllCategory(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'producerAll', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to add new Input*/
  add(category) {
    return this._http.post(this._host + 'producer', category).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.post(this._host +'/terpenprofile', category, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to get category with id*/
  get(ID) {
    return this._http.get(this._host + 'producer/' + ID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/terpenprofileDetail/'+ ID, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to update category*/
  update(category) {
    return this._http.put(this._host + 'producer/' + category.id, category).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/terpenprofile', category, { headers: headers }).map((res:Response) => res.json())
  }
  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
