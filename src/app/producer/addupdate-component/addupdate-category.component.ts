import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate-category.component.scss'],
})
export class AddUpdateCategoryComponent {
  public category = {
    name: '',
    city: '',
  };

  public isLoading = false;
  public isPageLoading = true;
  cities = [];
  categoryID: any;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.categoryID = _activateRouter.snapshot.params['id'];
    this.spinner.show();
    if (this.categoryID) {
      this._catgService.get(this.categoryID).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isPageLoading = false;
          if (res.success) {
            this.category = res.data;
            this.category.city = res.data.city.id;
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isPageLoading = false;
          // this._commanService.checkAccessToken(err);
        }
      );
    } else {
      this.spinner.hide();
      // this.isPageLoading = false;
    }

    this.allCities();
  }

  ngOnInit(): void {
    // this.showDangerAlert();
  }

  /*If categoryID exist then will update existing category otherwise will add new category*/
  save() {
    this.spinner.show();
    // this.isLoading = true;
    let data = JSON.parse(JSON.stringify(this.category));
    if (this.categoryID) {
      this._catgService.update(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading         = false;
          if (res.success) {
            this.toastr.success('Producer updated successfully!');
            this._cookieService.put('categoryAlert', res.data.message);
            this._router.navigate(['/producer/list']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    } else {
      this._catgService.add(data).subscribe(
        (res) => {
          this.spinner.hide();
          // this.isLoading         = false;
          if (res.success) {
            this.toastr.success('Producer addedd successfully!');
            this._cookieService.put('categoryAlert', res.data.message);
            this._router.navigate(['/producer/list']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  allCities() {
    this._commanService.allCities().subscribe(
      (res) => {
        if (res.success) {
          this.cities = res.data.data;
          //for (var i = 0; i < data.length; ++i) {
          //   this.cities.push(data[i].name);
          //}
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }
}
