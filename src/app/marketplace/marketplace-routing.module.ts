import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ListComponent } from './list-component/list.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Marketplace'
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'list',
        component: ListComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'add',
        component: AddUpdateComponent,
        data: {
          title: 'Add Marketplace Product'
        }
      },
      {
        path: 'list/:id',
        component: ViewComponent,
        data: {
          title: 'View Marketplace Product'
        }
      },
      {
        path: 'edit/:id',
        component: AddUpdateComponent,
        data: {
          title: 'Edit Marketplace Product'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
    RouterModule,
    FormsModule,
  ]
})
export class MarketplaceRoutingModule {}
