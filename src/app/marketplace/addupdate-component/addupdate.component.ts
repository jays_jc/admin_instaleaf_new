import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../environments/environment';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BrandService } from '../services/brand.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from 'src/app/shared/comman.service';
import { ImageResult } from 'ng2-imageupload';

@Component({
  templateUrl: 'addupdate.component.html',
  styleUrls: ['./addupdate.component.scss'],
})
export class AddUpdateComponent {
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  @ViewChild('myInput1', { static: true })
  myInputVariable1: any;
  private _host = environment.config.HOST;

  public object = {
    name: '',
    quantity: null,
    category_id: '',
    supplier_id: '',
    price: null,
    supplier_commission: '',
    metaName: '',
    metaDesc: '',
    description: '',
    image: '',
    images: [],
    isFeatured: false,
    //isAloha:false,
    status: 'active',
  };

  public errMessage: any;
  public descriptionError = false;
  public isLoading = false;
  public isPageLoading = true;
  public categories = [];
  public suppliers = [];
  public supplier_commission;
  public ID: any;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _brandService: BrandService,
    private _commanService: CommanService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private location: Location
  ) {
    this.spinner.show();
    this.ID = _activateRouter.snapshot.params['id'];
    if (this.ID) {
      this._brandService.get(this.ID).subscribe(
        (res) => {
          if (res.success) {
            this.object = res.data.key;
            if (res.data.key.category_id && res.data.key.category_id.id) {
              this.object.category_id = res.data.key.category_id.id;
              this.object.supplier_commission =
                res.data.key.supplier_commission;
            }
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
          this.spinner.hide();
          // this.isPageLoading = false;
        },
        (err) => {
          this.spinner.hide();
          // this.isPageLoading = false;
        }
      );
    } else {
      this.spinner.hide();
      // this.isPageLoading      = false;
    }

    this.allCategories();
    this.allSuppliers();
  }

  /*If id exist then will update existing brand otherwise will add new brand*/
  save() {
   
    // this.isLoading         = true;
    if (!this.object.description) {
      this.descriptionError = true;
      return;
    }
    this.spinner.show();
    let data = JSON.parse(JSON.stringify(this.object));

    if (this.ID) {
      this._brandService.update(data).subscribe(
        (res) => {
        
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
            this.location.back();
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.checkAccessToken(res.error);
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
          this.spinner.hide();
        },
        (err) => {
          this.toastr.error(
            'There is Some Problem please try after some time.'
          );
          // this._commanService.showAlert('There is Some Problem please try after some time.','alert-danger');
          // this.isLoading = false;
          this.spinner.hide();
        }
      );
    } else {
      this._brandService.add(data).subscribe(
        (res) => {
          // this.spinner.hide();
          // this.isLoading = false;
          if (res.success) {
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
            this.location.back();
          } else {
            this.toastr.error(res.error.message);
            // this._commanService.checkAccessToken(res.error);
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  allCategories() {
    this._commanService.getAllCategories().subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.categories = res.data.category;
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  allSuppliers() {
    this._commanService.getAllSuppliers().subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.suppliers = res.data.suppliers;
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }
  getcommission(value) {
    this._commanService.getSupplierCommission(value).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isLoading = false;
        if (res.success) {
          this.object.supplier_commission = res.commission.supplier_commission;
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  trim(key) {
    if (this.object[key] && this.object[key][0] == ' ')
      this.object[key] = this.object[key].trim();
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'product',
    };
   
    this.spinner.show();
    // this.isLoading = true;
    this._commanService.uploadImage(object).subscribe(
      (res) => {
       
        // this.isLoading = false;
        if (res.success) {
          this.object.images.push(res.data.fullPath);
        } else {
          window.scrollTo(0, 0);
          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
        this.spinner.hide();
        this.myInputVariable.nativeElement.value = '';
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }

  removeImage(image) {
    let index = this.object.images.indexOf(image);
    if (index > -1) this.object.images.splice(index, 1);
  }
}
