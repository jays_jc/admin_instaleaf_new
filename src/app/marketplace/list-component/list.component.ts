import { CommanService } from 'src/app/shared/comman.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { BrandService } from '../services/brand.service';
@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    product: string;
    city: string;
    type: string;
  } = {
      page: 0,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      product: '',
      city: '',
      type: 'other',
    };
  public addEditDelete: boolean = true;
  productList: any = [];
  totalItems: any;

  public constructor(
    private _router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private _commanService: CommanService,
    private _activateRouter: ActivatedRoute,
    private brandService: BrandService
  ) {
    let actions = this._commanService.getActions();
    this.spinner.show();
    // this.isPageLoading = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getList();
  }

  route(cropID, path) {
    let route = '/market/' + path + '/' + cropID;
    this._router.navigate([route]);
  }

  viewUser(userID, path) {
    let route = path + '/' + userID;
    this._router.navigate([route]);
  }

  remove(ID) {
    if (confirm('Do you want to delete?')) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService.deleteRecord(ID, 'marketplaceproduct').subscribe(
        (res) => {
          if (res.success) {
            // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
            // this.itemsTotal = this.itemsTotal - 1;

            // if( ! (this.itemsTotal >= start) ){
            //    this.activePage = this.activePage -1;
            //    if(this.activePage == 0) this.activePage = 1;
            // }
            /* reload page data */
            this.getList();
            this.toastr.success(res.data.message);
            // this._commanService.showAlert(res.data.message,'alert-success');
          } else {
            this.spinner.hide();
            this.toastr.error(res.error.message);
            // this.isLoading = false;
            // this._commanService.showAlert(res.error.message,'alert-danger');
          }
        },
        (err) => {
          this.spinner.hide();
          // this.isLoading = false;
        }
      );
    }
  }

  changeStatus(user) {
    let status = '';
    let message = 'Do You wana active ?';
    if (user.status == 'active') {
      status = 'deactive';
      message = 'Do you wana deactive ?';
    } else {
      status = 'active';
    }

    if (confirm(message)) {
      this.spinner.show();
      // this.isLoading = true;
      this._commanService
        .changeStatus(user.id, 'marketplaceproduct', status)
        .subscribe(
          (res) => {
            if (res.success) {
              // let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
              // this.itemsTotal = this.itemsTotal - 1;

              // if( ! (this.itemsTotal >= start) ){
              //    this.activePage = this.activePage -1
              // }
              this.toastr.success(res.data.message);
              // this._commanService.showAlert(res.data.message,'alert-success');
              /* reload page. */
              this.getList();
            } else {
              this.spinner.hide();
              this.toastr.error(res.error.message);
              // this.isLoading    = false;
              // this._commanService.showAlert(res.error.message,'alert-danger');
              // this._commanService.checkAccessToken(res.error);
            }
          },
          (err) => {
            this.spinner.hide();
            // this.isLoading = false;
          }
        );
    }
  }

  getList() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this.brandService
      .getAllList(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.productList = [];
          // this.spinner.hide();
          // this.isLoading = false;
        } else {
          this.totalItems = response.data.total;
          this.productList = response['data'].products.map((data) => {
            return {
              id: data.id,
              name: data.name,
              category: data.category ? data.category : '-',
              status: data.status,
              date: data.createdAt,
            };
          });
          // this.isLoading = false;
          // this.spinner.hide();
        }
        this.spinner.hide();
      },
        error => { this.spinner.hide(); }
      );
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/order/list/' + this.page;
    this.getList();
    // this.router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getList();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getList();
  }
}
