import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../environments/environment';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrandService } from '../services/brand.service';
import { CommanService } from 'src/app/shared/comman.service';

@Component({
  templateUrl: 'view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent {
  private _host = environment.config.HOST;
  // private _dateFormat    = tsConstants.DATE_FORMAT;

  public ID = '';
  public object = {};
  public isPageLoading = true;
  public isLoading = false;
  public addEditDelete: boolean = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _brandService: BrandService,
    private _commanService: CommanService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['crops']['addEditDelete'])
      this.addEditDelete = true;
    this.spinner.show();
    this.ID = _route.snapshot.params['id'];
    this._brandService.get(this.ID).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isPageLoading = false;
        if (res.success) {
          this.object = res.data.key;
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isPageLoading = false;
      }
    );
  }

  route(ID, path) {
    let route = '/market/' + path + '/' + ID;
    this._router.navigate([route]);
  }
}
