import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { CommanService } from 'src/app/shared/comman.service';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class BrandService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _commanService: CommanService
  ) {}

  /*Use to fetch all brand*/
  getAllList(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'marketplaceproducts', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let date =  new Date().getTime().toString();
    // let url = this._host +'/marketplaceproducts?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&product='+search+'&city='+city+'&type='+type;
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to add new brand*/
  add(obj) {
    return this._http.post(this._host + 'add_marketplace_product', obj).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.post(this._host +'/add_marketplace_product', obj, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to get brand with brand id*/
  get(ID) {
    return this._http.get(this._host + 'marketplaceproductdetail/' + ID).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/marketplaceproductdetail/'+ ID, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to get brand with brand id*/
  getProducer() {
    return this._http.get(this._host + 'producer').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/producer', { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to update obj*/
  update(obj) {
    return this._http
      .put(this._host + 'edit_marketplace_product/' + obj.id, obj)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.put(this._host +'/edit_marketplace_product/'+ obj.id, obj, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to verify, approve or expire brand*/
  delete(id) {
    return this._http.delete(this._host + 'product/' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/product/' + id;
    // let body = {isDeleted:true}
    // return this._http.post(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  getAllReviewList(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'productreviews', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let date =  new Date().getTime().toString();
    // let url = this._host +'/productreviews?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&product='+search+'&id='+ID;
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  getReview(id) {
    return this._http.get(this._host + 'productreviewdetail/' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // //let url = this._host + '/reviewdetail?id='+id;
    // let url = this._host + '/productreviewdetail/'+id;
    // return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  }

  editReviews(ID, body) {
    return this._http.put(this._host + 'editproductreview', body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/editproductreview';  //editreviews
    // return this._http.put(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  /*Use to deletion of reviews*/
  deleteReviews(id) {
    return this._http.delete(this._host + 'reviewDeletion?id=' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/reviewDeletion?id='+id;
    // let body = {isDeleted:true}
    // return this._http.put(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  RevokeReviews(id) {
    let body = {
      id: id,
      isRevoked: true,
      type: 'product',
    };
    return this._http.put(this._host + 'revokedReview', body).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/revokedReview';
    // let body = {
    //     "id": id,
    //     "isRevoked": true,
    //     "type":"product"
    // }
    // return this._http.put(url,body, { headers: headers }).map((res:Response) => res.json())
  }

  getSubCat(name) {
    return this._http.get(this._host + 'get_productcategory/' + name).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/get_productcategory/'+ name, { headers: headers }).map((res:Response) => res.json())
  }

  productReviewTag(name) {
    return this._http.get(this._host + 'get_productreviewtag/' + name).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/get_productreviewtag/'+ name, { headers: headers }).map((res:Response) => res.json())
  }

  getTerpenProfile() {
    return this._http.get(this._host + 'terpenprofile').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/terpenprofile', { headers: headers }).map((res:Response) => res.json())
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
