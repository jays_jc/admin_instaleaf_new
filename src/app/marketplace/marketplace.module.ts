import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list-component/list.component';
import { AddUpdateComponent } from './addupdate-component/addupdate.component';
import { ViewComponent } from './view-component/view.component';
import { BrandService } from './services/brand.service';
import { FormsModule } from '@angular/forms';
import { MarketplaceRoutingModule } from './marketplace-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CKEditorModule } from 'ng2-ckeditor';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
    MarketplaceRoutingModule,
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    NgxSpinnerModule,
    CKEditorModule,
    ImageUploadModule
  ],
  providers: [BrandService],
  declarations: [ListComponent, AddUpdateComponent, ViewComponent],
})
export class MarketplaceModule { }
