import { CommanService } from 'src/app/shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
declare let jsPDF;

@Component({
  selector: 'app-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
})
export class ListCategoryComponent implements OnInit {
  private _host = environment.config.HOST;
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; count: number; search: string; sortBy: string } = {
    page: 0,
    count: 5,
    search: '',
    sortBy: 'createdAt desc',
  };
  public addEditDelete: boolean = false;
  categoryList: any = [];
  response: any;
  totalItems: any;
  public constructor(
    private router: Router,
    private _categoryService: CategoryService,
    private _activateRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService,
    private toastr: ToastrService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategory();
  }
  edit(ID) {
    let route = '/affiliate/edit/' + ID;
    this.router.navigate([route]);
  }
  remove(ID) {
    if (confirm('Do you want to delete?')) {
      this.spinner.show();
      //this.isLoading = true;
      this._commanService.deleteRecord(ID, 'affiliate').subscribe(
        (res) => {
          this.response = res;
          //this.spinner.hide();
          if (res.success) {

            //let start       = (this.activePage * this.rowsOnPage - this.rowsOnPage + 1);
            //this.itemsTotal = this.itemsTotal - 1;
//
            //if( ! (this.itemsTotal >= start) ){
            //   this.activePage = this.activePage -1;
            //   if(this.activePage == 0) this.activePage = 1;
            //}
            /* reload page data */
            this.toastr.success(res.data.message);

            this.getCategory();
            // this._commanService.showAlert(res.data.message,'alert-success');
          } else {
             //  this.isLoading = false;
           this.toastr.error(res.error.message);
           // this._commanService.showAlert(res.error.message,'alert-danger');
          }
        },
        (err) => {
          this.spinner.hide();

         // this.isLoading = false;
          this.toastr.error(err);
        }
      );
    }
  }
  /*Get all Users */
  getCategory(): void {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._categoryService
      .getAllCategory(this.filters)
      .subscribe((response) => {
        // console.log('data', response.data);
        if (!response.success) {
          this._commanService.checkAccessToken(response.error);
          this.toastr.error(response.error.message);
          if (response.error.code == 401) {
            this.router.navigate(['/auth/login', { data: true }]);
          }
          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].affiliateData.map((data) => {
            return {
              id: data.id,
              name: data.name,
              createdBy: data.createdAt,
            };
          });
          // this.isLoading = false;
          this.spinner.hide();
        }
      },
        error => {
          this.spinner.hide();
          this._commanService.checkAccessToken(error);
          this.toastr.error(error.message);
          if (error.code == 401) {
            this.router.navigate(['/auth/login', { data: true }]);
          }
        }
      );
  }
  setPage(e) {
    this.page = e.offset + 1;
    // console.log(e);
    // let page = e.offset;
    Object.assign(this.filters, { page: this.page });
    // let route = '/affiliate/list/' + page;
    this.getCategory();
    // this.router.navigate([route]);
  }
  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }
  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategory();
  }
}
