import { CommanService } from 'src/app/shared/comman.service';
import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ImageResult } from 'ng2-imageupload';
@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate-category.component.scss'],
})
export class AddUpdateCategoryComponent {
  @ViewChild('myInput', { static: true }) myInputVariable: any;
  private _host = environment.config.HOST;
  public affiliateForm: FormGroup;
  _affiliateObservable: any;
  submitted = false;
  affiliate: any = [];
  affiliateId: any;
  categoryID: any;
  conditionalForm: boolean = false;
  headerTitle: string;
  // myInputVariable: any;
  public types: any = [];
  object: any;

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _commanService: CommanService
  ) {
    this.createForm();
  }
  createForm() {
    this.affiliateForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', [Validators.required, Validators.min(1000000000), Validators.max(999999999999), Validators.pattern]],
      link: ['', Validators.required],
      image: ['']
    });
  }
  get f() {
    return this.affiliateForm.controls;
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.categoryID = params['id'];
      if (params['view'] == 'true' && this.categoryID) {
        this.headerTitle = 'View Affiliate';
        this.getAffiliateData();
        // this.addAbbrevationForm.patchValue({
        //   clientName: params['clientName'],
        //   abbreviation: params['abbreviation'],
        // })
      } else if (this.categoryID) {
        this.headerTitle = 'Update Affiliate';
        // this.conditionalForm = false;
        this.getAffiliateData();
        // this.addAbbrevationForm.patchValue({
        //   clientName: params['clientName'],
        //   abbreviation: params['abbreviation'],
        // })
      } else {
        this.headerTitle = 'Add Affiliate';
        // this.conditionalForm = true;
      }
    });
  }
  getAffiliateData() {
    this.spinner.show();
    this._catgService.get(this.categoryID).subscribe(
      (res) => {
        this.spinner.hide();
        // this.isPageLoading = false;
        if (res.success) {
          this.affiliateForm.patchValue({
            name: res.data[0].name,
            email: res.data[0].email,
            mobile: res.data[0].mobile,
            link: res.data[0].link,
            image: res.data[0].image
          });
          this.object = res.data[0];
        } else {
          // this._commanService.checkAccessToken(res.error);
        }
      },
      (err) => {
        this.spinner.hide();
        // this.isPageLoading = false;
        // this._commanService.checkAccessToken(err);
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    // if (this.affiliateForm.controls.image.value == '') {
    //   this.toastr.warning('Please upload affiliate image');
    // }

    if (!this.affiliateForm.invalid) {
      this.spinner.show();

      if (this.categoryID) {
        let data = JSON.parse(JSON.stringify(this.object));
        data['name'] = this.affiliateForm.value.name;
        data['email'] = this.affiliateForm.value.email;
        data['mobile'] = this.affiliateForm.value.mobile;
        data['link'] = this.affiliateForm.value.link;
        data['image'] = this.affiliateForm.value.image;
        this._affiliateObservable = this._catgService
          .update(data)
          .subscribe(
            (res) => {
              let url;
              if (res.success) {
                this.toastr.success('Affiliate updated Successfully!');
                url = '/affiliate';
                this.router.navigate([url]);
              } else {
                this.toastr.error(res.error.message, 'Error');
              }
              this.spinner.hide();
            },
            (error) => {
              this.spinner.hide();
              this.toastr.error(error, 'Error');
            }
          );
      } else {
        this._affiliateObservable = this._catgService
          .add(this.affiliateForm.value)
          .subscribe(
            (res) => {
              let url;
              if (res.code == 200) {
                this.toastr.success('Affiliate saved Successfully!');
                url = '/affiliate';
                this.router.navigate([url]);
              } else {
                this.toastr.error(res.error.message, 'Error');
              }
              this.spinner.hide();
            },
            (error) => {
              this.spinner.hide();
              this.toastr.error(error, 'Error');
            }
          );
      }
    }
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'dispensary',
    };
    
    // this.isLoading = true;
    this.spinner.show();
    this._commanService.uploadImage(object).subscribe(
      (res) => {
       
        // this.isLoading = false;
        if (res.success) {
          this.affiliateForm.patchValue({
            image: res.data.fullPath,
          });
         // this.toastr.success('Affiliate saved Successfully!');

        } else {

          this.toastr.error(res.error.message);
          // this._commanService.showAlert(res.error.message,'alert-danger');
        }
        this.myInputVariable.nativeElement.value = '';
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        // this.isLoading = false;
      }
    );
  }
}
