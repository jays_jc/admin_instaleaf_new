import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryService } from './services/category.service';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { AffiliateRoutingModule } from './affiliate-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
@NgModule({
  imports: [
  	AffiliateRoutingModule,
  	 CommonModule,
     NgxDatatableModule,
     FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    ImageUploadModule
  ],
  providers: [
    CategoryService
  ],
  declarations: [
  	ListCategoryComponent,
  	AddUpdateCategoryComponent
  ]
})
export class AffiliateModule { }