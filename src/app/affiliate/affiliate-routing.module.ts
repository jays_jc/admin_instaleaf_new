import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Affiliate Management',
    },
    children: [
      {
        path: '',
        component: ListCategoryComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'list',
        component: ListCategoryComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'add',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Add Affiliate',
        },
      },
      {
        path: 'edit',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Edit Affiliate',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), FormsModule],
  exports: [RouterModule, FormsModule],
})
export class AffiliateRoutingModule {}
