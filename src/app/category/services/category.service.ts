import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpParams,
  HttpClient,
} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class CategoryService {
  private _host = environment.config.HOST;

  constructor(private _http: HttpClient) { }

  getAllCategories(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'allcategory', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  delete(Id) {
    return this._http.delete(this._host + 'deletecat/' + Id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .delete(this._host + '/deletecat/' + Id, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  updateMasterCategory(obj) {
    return this._http
      .put(
        this._host +
        'updateMasterCategory?id=' +
        obj.id +
        '&isMaster=' +
        obj.isMaster,
        {}
      )
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .put(
    //     this._host +
    //       '/updateMasterCategory?id=' +
    //       obj.id +
    //       '&isMaster=' +
    //       obj.isMaster,
    //     {},
    //     { headers: headers }
    //   )
    //   .map((res: Response) => res.json());
  }

  get(id) {
    return this._http.get(this._host + 'category/' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  getProductCategoryDetail(id) {
    return this._http.get(this._host + 'get_productsubcategorydetail/' + id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  // get_productsubcategorydetail

  update(category) {
    return this._http
      .put(this._host + 'category/' + category.id, category)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  /*Use to add new Input*/
  add(category) {
    return this._http.post(this._host + 'category', category).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  getAllProductCategories(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http
      .get(this._host + 'get_allproductcategory', { params: params })
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  /*Use to get category with id*/
  getDetailProductCategory(ID) {
    return this._http
      .get(this._host + 'get_productsubcategorydetail/' + ID)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http.get(this._host +'/get_productsubcategorydetail/'+ ID, { headers: headers }).map((res:Response) => res.json())
  }

  updateProductCategory(category) {
    return this._http.put(this._host + 'update_productcategory', category).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to add new Input*/
  addProductCategory(category) {
    return this._http.post(this._host + 'set_productcategory', category).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  /*Use to get all Users*/
  // getAllUsers(rowsOnPage, activePage, sortTrem, search = '', roles = 'U') {
  //     let date =  new Date().getTime().toString();
  //     let url = this._host +'/order?count='+rowsOnPage+'&page='+activePage+'&sortBy='+sortTrem+'&roles='+roles+'&search='+search+'&date='+date;

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.get(url, { headers: headers }).map((res:Response) => res.json())
  // }

  /*User to get user detail with ID*/
  // get(userid) {

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.get(this._host +'/order/'+ userid, { headers: headers }).map((res:Response) => res.json())
  // }

  /*Use to update user detail with there ID*/
  // update(user) {

  //     let headers = this._commanService.getAuthorizationHeader();
  //     return this._http.put(this._host +'/order', user, { headers: headers }).map((res:Response) => res.json())
  // }
  /*Use to update user detail with there ID*/
  // order_update_supplier(user) {
  //   return this._http.get(this._host + 'order_update_supplier/' + user).pipe(
  //     map((response: any) => {
  //       return response.json();
  //     }),
  //     catchError(this.handleError)
  //   );
  //   // let headers = this._commanService.getAuthorizationHeader();
  //   // return this._http.put(this._host +'/order_update_supplier', user, { headers: headers }).map((res:Response) => res.json())
  // }

  // sendingInvoice(order) {
  //   return this._http.get(this._host + 'sendinvoice/' + order).pipe(
  //     map((response: any) => {
  //       return response.json();
  //     }),
  //     catchError(this.handleError)
  //   );
  //   // let headers = this._commanService.getAuthorizationHeader();
  //   // return this._http.get(this._host +'/sendinvoice/'+ order, { headers: headers }).map((res:Response) => res.json())
  // }

  // sendingInvoiceToSupplier(order, order_id, gst, delivery) {
  //   return this._http
  //     .get(
  //       this._host +
  //         'sendinvoicetosupplier/' +
  //         order +
  //         '/' +
  //         order_id +
  //         '/' +
  //         gst +
  //         '/' +
  //         delivery
  //     )
  //     .pipe(
  //       map((response: any) => {
  //         return response.json();
  //       }),
  //       catchError(this.handleError)
  //     );
  //   // let headers = this._commanService.getAuthorizationHeader();
  //   // return this._http.get(this._host +'/sendinvoicetosupplier/'+ order+'/'+order_id+'/'+gst+'/'+delivery, { headers: headers }).map((res:Response) => res.json())
  // }

  getCategoryByType(type) {
    return this._http.get(this._host + 'categorybytype/' + type).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // let url = this._host + '/categorybytype/' + type;
    // console.log('news are', url);
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  updateNewsCategory(newsinfo) {
    let obj = {
      id: newsinfo.id,
      categoryId: newsinfo.categoryId,
    };
    return this._http.put(this._host + 'addnewscategory', newsinfo).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this._commanService.getAuthorizationHeader();
    // return this._http
    //   .put(this._host + '/addnewscategory', newsinfo, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
