import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ListCategoryComponent } from './list-component/list-category.component';
import { AddUpdateCategoryComponent } from './addupdate-component/addupdate-category.component';
import { ViewCategoryComponent } from './view-component/view-category.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Category',
    },
    children: [
      {
        path: '',
        component: ListCategoryComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'list',
        component: ListCategoryComponent,
        data: {
          title: 'List',
        },
      },
      {
        path: 'add',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Add Category',
        },
      },
      {
        path: 'view',
        component: ViewCategoryComponent,
        data: {
          title: 'View',
        },
      },
      {
        path: 'edit/:id',
        component: AddUpdateCategoryComponent,
        data: {
          title: 'Edit Category',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), FormsModule],
  exports: [RouterModule, FormsModule],
})
export class CategoryRoutingModule {}
