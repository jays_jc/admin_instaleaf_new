import { Component, ChangeDetectorRef } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { CommanService } from 'src/app/shared/comman.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  templateUrl: 'addupdate-category.component.html',
  styleUrls: ['./addupdate-category.component.scss'],
})
export class AddUpdateCategoryComponent {
  public category = {
    name: '',
    type: '',
  };

  public isLoading = false;
  public isPageLoading = true;
  public categoryID: any;
  public oBj = { vname: '' };
  public response: any;
  public type;
  public errMessage = '';
  public ParentCategories = [];

  constructor(
    private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _catgService: CategoryService,
    // private _flashMessagesService: FlashMessagesService,
    private _cookieService: CookieService,
    private _commanService: CommanService,
    private changeDetectorRef: ChangeDetectorRef,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
  ) {
    this.categoryID = _activateRouter.snapshot.params['id'];

    if (this.categoryID) {

      this._catgService.get(this.categoryID).subscribe(
        (res) => {
          this.spinner.hide();
          this.isPageLoading = false;
          if (res.success) {
            this.category = res.data;
            if (this.category['parentId']) {
              this.category['parentId'] = this.category['parentId']['id'];
            } else {
              this.category['parentId'] = '';
            }
          } else {
            // this._commanService.checkAccessToken(res.error);
          }
        },
        (err) => {
          this.isPageLoading = false;
          this.spinner.hide();
          // this._commanService.checkAccessToken(err);
        }
      );
    } else {
      this.isPageLoading = false;
      this.spinner.hide();
    }
  }

  ngOnInit(): void {
    // this.showDangerAlert();
  }

  /*If categoryID exist then will update existing category otherwise will add new category*/
  save() {
    // this.isLoading = true;
    this.spinner.show();
    let data = JSON.parse(JSON.stringify(this.category));
    if (data.parentId == '' || !data.parentId) {
      data.parentId = null;
      data.variety = [];
    } else {
    }
    if (this.categoryID) {
      this._catgService.update(data).subscribe(
        (res) => {
          // this.isLoading = false;
          this.spinner.hide();
          if (res.success) {
            this.response = res;
            this.toastr.success(res.data.message);
            this._cookieService.put('categoryAlert', res.data.message);
            this._router.navigate(['/category/list']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            // this.showDangerAlert();
            this.toastr.error(res.error.message);
          }
        },
        (err) => {
          this.toastr.error(err);
          // this.isLoading = false;
          this.spinner.hide();
        }
      );
    } else {
      this._catgService.add(data).subscribe(
        (res) => {
          // this.isLoading = false;
          this.spinner.hide();
          if (res.success) {
            this.response = res;
            this.toastr.success(res.data.message);
            this._cookieService.put('categoryAlert', res.data.message);
            this._router.navigate(['/category/list']);
          } else {
            this._cookieService.put('categoryExistAlert', res.error.message);
            this.toastr.error(res.error.message);
            // this.showDangerAlert();
          }
        },
        (err) => {
          this.toastr.error(err);
          // this.isLoading = false;
          this.spinner.hide();
        }
      );
    }
  }

  // showDangerAlert(): void {

  //     let alertMessage = this._cookieService.get('categoryExistAlert');
  //     if( alertMessage ) {
  //         this._flashMessagesService.show( alertMessage, {
  //             classes: ['alert', 'alert-danger'],
  //             timeout: 3000,
  //         });
  //         this._cookieService.remove('categoryExistAlert');
  //     }
  // }

  trim(key) {
    if (this.category[key] && this.category[key][0] == ' ')
      this.category[key] = this.category[key].trim();
  }
}
