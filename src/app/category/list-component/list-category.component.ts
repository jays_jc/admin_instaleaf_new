import { CookieService } from 'ngx-cookie';
import { CommanService } from './../../shared/comman.service';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
declare let jsPDF;

@Component({
  selector: 'app-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
})
export class ListCategoryComponent implements OnInit {
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
  } = {
      page: 0,
      count: 25,
      search: '',
      sortBy: 'createdAt desc',
      date: new Date().getTime().toString(),
    };
  categoryList: any = [];
  public addEditDelete: boolean = false;
  public response: any;
  totalItems: any;

  public constructor(
    private spinner: NgxSpinnerService,
    private _router: Router,
    private _categoryService: CategoryService,
    private _activateRouter: ActivatedRoute,
    private _commanService: CommanService,
    private _cookieService: CookieService,
    private toastr: ToastrService
  ) {
    let actions = this._commanService.getActions();
    if (actions['type'] == 'SA' || actions['category']['addEditDelete'])
      this.addEditDelete = true;
  }

  ngOnInit(): void {
    this.filters.page = this._activateRouter.snapshot.params['page'] || 1;
    // this.page = this.filters.page;
    Object.assign(this.filters, { page: this.filters.page });
    this.getCategories();
  }

  getCategories() {
    // this.isLoading = true;
    this.spinner.show();
    this._subscriberData = this._categoryService
      .getAllCategories(this.filters)
      .subscribe((response) => {
        if (!response.success) {
          this.categoryList = [];
          // this.isLoading = false;
          // this.spinner.hide();
        } else {
          this.totalItems = response.data.total;
          this.categoryList = response['data'].category.map((data) => {
            return {
              id: data.id,
              name: data.name,
              master: data.isMaster,
              type: data.type,
              date: data.createdAt,
            };
          });
          // this.isLoading = false;

        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
        }
      );
  }

  remove(ID) {
    if (confirm('Do you want to delete?')) {
      this.isLoading = true;
      this.spinner.show();
      this._categoryService.delete(ID).subscribe(
        (res) => {
          if(res.Status){
            this.response = res;
            this.spinner.hide();
           // this.categoryList = [];
           // this.getCategories();
           // this.toastr.success(res.result.sucess.Message);
          }
          // this.response = res;
          // this.isLoading = false;
          this.spinner.hide();
          // let start = this.activePage * this.rowsOnPage - this.rowsOnPage + 1;
          // this.itemsTotal = this.itemsTotal - 1;

          // if (!(this.itemsTotal >= start)) {
          //   this.activePage = this.activePage - 1;
          // }
          // this._cookieService.put('categoryAlert', res.result.sucess.Message);
          /* reload page. */
          
        },
        (err) => {
          // this.isLoading = false;
          this.spinner.hide();
        }
      );
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/category/' + page;
    this.getCategories();
    // this._router.navigate([route]);
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategories();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: 1, search: this.filters.search });
    this.getCategories();
  }

  updateCategory(id, e) {
    let data = {
      id: id,
      isMaster: e.target.checked,
    };
    this._categoryService.updateMasterCategory(data).subscribe(
      (res) => {
        this.isLoading = false;
        this.spinner.hide();
        if (res.success) {
          this.toastr.success('Master category updated successfully');
          // this._commanService.showAlert(
          //   'Master category updated successfully',
          //   'alert-success'
          // );
          this.getCategories();
          // this._commanService.back();
        } else {
          this.toastr.error(res.error.message);
          // this._commanService.checkAccessToken(res.error);
          // this._commanService.showAlert(res.error.message, 'alert-danger');
        }
      },
      (err) => {
        this.toastr.error('There is Some Problem please try after some time.');
        // this._commanService.showAlert(
        //   'There is Some Problem please try after some time.',
        //   'alert-danger'
        // );
        this.isLoading = false;
        this.spinner.hide();
      }
    );
    //   }
  }

  edit(ID) {
    let route = '/category/edit/' + ID;
    this._router.navigate([route]);
  }
}
