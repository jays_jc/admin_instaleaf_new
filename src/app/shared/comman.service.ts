import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpErrorResponse, } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie';
import { Router } from '@angular/router';
// import { Http, Response, Headers, ResponseContentType } from '@angular/http';
// import { CookieService } from 'ngx-cookie';
// import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
// import { FlashMessagesService } from 'ngx-flash-messages';
// import { FullLayoutService } from '../../layouts/full-layout.service';
// // import tsConstants = require('./../../tsconstant');
// import tsMessages = require('./../../tsmessage');
// import { Location } from '@angular/common';
// import { Angular2Csv } from 'angular2-csv/Angular2-csv';
declare let jsPDF;

@Injectable()
export class CommanService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _cookieService: CookieService, // private _flashMessagesService: FlashMessagesService, 
    private _router: Router,
    private toastr: ToastrService
    // private _http: Http, // private _fullLayoutService: FullLayoutService, // private _location: Location
  ) { }

  /*This function is use to remove user session if Access token expired. */
  checkAccessToken(err): void {
    console.log(err);
    let code = err.code;
    let message = err.message;

    if (code == 401 && message == 'Authorization') {
      this._cookieService.removeAll();

      // this._router.navigate(['/auth/login', { data: true }]);
    } else {
    }
  }

  // /*This function is use to get access token from cookie. */
  getAccessToken(): string {
    let token = localStorage.getItem('token');
    // let token = this._cookieService.get('token');
    return 'Bearer ' + token;
  }

  // /*This function is use to get header with Authorization or without Authorization. */
  getAuthorizationHeader(access = true) {
    let headers = new Headers();

    if (access) {
      let token = this.getAccessToken();
      headers.append('Authorization', token);
    }

    return headers;
  }

  // /*This function is use to get Roles from cookie. */
  getActions(): object {
    let actions = this._cookieService.getObject('actions');
    return actions;
  }

  /*Use to add new image*/
  uploadImage(object) {
    return this._http.post(this._host + 'upload', object).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // return this._http
    //   .post(this._host + '/upload', object)
    //   .map((res: Response) => res.json());
  }

  uploadCSV(object) {
    return this._http.post(this._host + 'bulkUpload', object).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // return this._http
    //   .post(this._host + '/bulkUpload', object, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  uploadProductCSV(object) {
    return this._http.post(this._host + 'bulkProductUpload', object).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // return this._http
    //   .post(this._host + '/bulkProductUpload', object, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  /*Use to fetch all categories*/
  getAllCategories() {
    return this._http.get(this._host + 'categoryList').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // let url = this._host + '/categoryList';
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getAllCategoriesByType(cattype) {
    return this._http.get(this._host + 'categorybytype/' + cattype).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // let url = this._host + '/categorybytype/' + cattype;
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getAllPosCustom() {
    return this._http.get(this._host + 'customnposcategories').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // let url = this._host + '/customnposcategories';
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getAllSuppliers() {
    return this._http.get(this._host + 'supplierlist').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // let url = this._host + '/supplierlist';
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  getSupplierCommission(supplierid) {
    return this._http
      .get(this._host + 'getcommission?supplierid=' + supplierid)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this.getAuthorizationHeader();
    // let url = this._host + '/getcommission?supplierid=' + supplierid;
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  /*Use to fetch all Users*/
  getAllUsers(role, multipleroles = []) {
    let multipleRoles = '';
    if (!role && multipleroles.length)
      multipleRoles = JSON.stringify(multipleroles);
    return this._http
      .get(
        this._host +
        '/activeuser?roles=' +
        role +
        '&multipleroles=' +
        multipleRoles
      )
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this.getAuthorizationHeader();
    // let url =
    //   this._host +
    //   '/activeuser?roles=' +
    //   role +
    //   '&multipleroles=' +
    //   multipleRoles;
    // return this._http
    //   .get(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  // /*Use to fetch all States*/
  // getStates() {
  //   let headers = this.getAuthorizationHeader();
  //   return this._http
  //     .get(this._host + '/states?sort=stateName', { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  // /*Use to soft delete any Record */
  deleteRecord(id, model) {
    return this._http
      .delete(this._host + `delete?id=` + id + '&model=' + model)
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this.getAuthorizationHeader();
    // let url = this._host + '/delete?id=' + id + '&model=' + model;
    // return this._http
    //   .delete(url, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  // /*Use to get Settings */
  getSettings() {
    return this._http.get(this._host + 'setting').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // // let headers = this.getAuthorizationHeader();
    // return this._http
    //   .get(this._host + '/setting').map((res: Response) => res.json());
  }

  /*Use to soft delete any Record */
  changeStatus(id, model, status) {
    return this._http
      .put(
        this._host +
        'changestatus?id=' +
        id +
        '&model=' +
        model +
        '&status=' +
        status,
        {}
      )
      .pipe(
        map((response: any) => {
          return response;
        }),
        catchError(this.handleError)
      );
    // let headers = this.getAuthorizationHeader();
    // let url =
    //   this._host +
    //   '/changestatus?id=' +
    //   id +
    //   '&model=' +
    //   model +
    //   '&status=' +
    //   status;
    // return this._http
    //   .put(url, {}, { headers: headers })
    //   .map((res: Response) => res.json());
  }

  // /*Use to convert date to date object for date picker module */
  // convertDateToDateObject(date) {
  //   let _date = new Date(date);
  //   if (_date) {
  //     let obj = {
  //       date: {
  //         year: _date.getFullYear(),
  //         month: _date.getMonth() + 1,
  //         day: _date.getDate(),
  //       },
  //       jsdate: date,
  //     };
  //     return obj;
  //   } else {
  //     return {};
  //   }
  // }

  // showAlert(message, alertClass) {
  //   if (message == null || typeof message == 'object')
  //     message = tsMessages.SERVER_ERROR;
  //   this._fullLayoutService.showAlert(
  //     message,
  //     alertClass,
  //     tsConstants.ALERT_TIME
  //   );
  // }

  // //Use to browse back based on location history when click on back buttton.
  // back() {
  //   this._location.back();
  // }

  // getSessionUser() {
  //   let headers = this.getAuthorizationHeader();
  //   return this._http
  //     .get(this._host + '/users/current', { headers: headers })
  //     .map((res: Response) => res.json());
  // }

  // toDateFormate(i, DATE) {
  //   let _date = DATE ? new Date(DATE) : null;
  //   let date = '-';
  //   if (_date) {
  //     date = _date
  //       ? _date.getDate() +
  //         '/' +
  //         (_date.getMonth() + 1) +
  //         '/' +
  //         _date.getFullYear()
  //       : '-';
  //   }

  //   return date;
  // }

  allCities() {
    return this._http.get(this._host + 'allcities').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // return this._http
    //   .get(this._host + '/allcities', { headers: headers })
    //   .map((res: Response) => res.json());
  }

  allProducers() {
    return this._http.get(this._host + 'producerlist').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // return this._http
    //   .get(this._host + '/producerlist', { headers: headers })
    //   .map((res: Response) => res.json());
  }

  allProvinces() {
    return this._http.get(this._host + 'allprovinces').pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
    // let headers = this.getAuthorizationHeader();
    // return this._http
    //   .get(this._host + '/allprovinces', { headers: headers })
    //   .map((res: Response) => res.json());
  }

  // downloadCSV(name, filteredData) {
  //   let fileName = name + '-' + Math.floor(Date.now() / 1000);
  //   new Angular2Csv(filteredData, fileName);
  // }

  // downloadPDF(name, header, filteredData) {
  //   var doc = new jsPDF();
  //   let fileName = name + '-' + Math.floor(Date.now() / 1000);

  //   doc.autoTable(header, filteredData, {
  //     theme: 'grid',
  //     headerStyles: { fillColor: 0 },
  //     startY: 10, // false (indicates margin top value) or a number
  //     margin: { horizontal: 7 }, // a number, array or object
  //     pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  //     tableWidth: 'wrap', // 'auto', 'wrap' or a number,
  //     tableHeight: '1', // 'auto', 'wrap' or a number,
  //     showHeader: 'everyPage',
  //     tableLineColor: 200, // number, array (see color section below)
  //     tableLineWidth: 0,
  //     fontSize: 10,
  //     overflow: 'linebreak',
  //     columnWidth: 'auto',
  //     cellPadding: 2,
  //     cellSpacing: 0,
  //     valign: 'top',
  //     lineHeight: 15,
  //   });

  //   doc.save(fileName);
  // }

  downloadExportCSV(url) {
    // let headers = this.getAuthorizationHeader();
    return this._http.get(this._host + url, {

      responseType: 'blob',
    });
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
