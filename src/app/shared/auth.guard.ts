import { CookieService } from 'ngx-cookie';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class AuthGuard implements CanActivate {

    constructor(
        public router: Router,
        private _cookieService: CookieService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let token = this._cookieService.get('token');
        if (token) {
            return true;
        }
        this.router.navigate(['/']);
        return false;
        // if (!token) {
        //     return true;
        // } else {
        //     this.router.navigate(['/dashboard']);
        // }

    }
}