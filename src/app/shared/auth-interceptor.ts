import { CookieService } from 'ngx-cookie';
import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

// import { Observable, throwError, from } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';

// import {do} from  'rxjs/add/operator/do';

const TOKEN_KEY = 'token';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  protected debug = false;
  private APIToken = null;
  token: any;
  currentUserData: any;

  constructor(
    private authService: AuthService,
    private CookieService: CookieService
  ) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.currentUserData = this.CookieService.get('token');
    console.log(this.currentUserData);
    if (this.currentUserData) {
      let headers = {};
      const token = this.currentUserData;
      if (token) {
        headers = {
          Authorization: `Bearer ${token}`,
        };

        request = request.clone({
          setHeaders: headers,
        });
      }
    }

    return next.handle(request);
  }
}
