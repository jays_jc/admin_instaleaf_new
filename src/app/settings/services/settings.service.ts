import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { CommanService } from 'src/app/shared/comman.service';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable()
export class SettingsService {
  private _host = environment.config.HOST;

  constructor(
    private _http: HttpClient,
    private _CommanService: CommanService
  ) {

  }
  /*Use to fetch all Inputs*/
  getAllCategory(param) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key]);
      }
    }
    return this._http.get(this._host + 'setting', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  update(settings) {
    return this._http.post(this._host + 'setting/add', settings).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
