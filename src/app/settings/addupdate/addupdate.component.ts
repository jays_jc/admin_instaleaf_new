import { ToastrService } from 'ngx-toastr';
import { Component } from '@angular/core';
import { SettingsService } from '../services/settings.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommanService } from '../../shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;

@Component({
    templateUrl: 'addupdate.component.html',
    styleUrls: ['./addupdate.component.scss']
})
export class AddUpdateSettingsComponent {

    public setting = {
        commission: {
            admin: null,
            store: null
        },
        alohacommission: {
            admin: null,
            aloha: null
        },
        reward: {
            dispensary: null,
            product: null
        },
        tax_percentage: null,
        gst: null,
        delivery_charge: null,
        fare_charges: [{
            min: null,
            max: null,
            fare: null
        }]
    };
    public isLoading = false;
    public isPageLoading = true;
    private tab: any;

    constructor(
        private _router: Router,
        private _activateRouter: ActivatedRoute,
        private _commanService: CommanService,
        private spinner: NgxSpinnerService,
        private _settingsService: SettingsService,
        private toastr: ToastrService
    ) {
        this.spinner.show();
        this._commanService.getSettings().subscribe(res => {
            if (res.success) {
                console.log("res.data[0]", res.data[0])
                this.setting = res.data[0];
            } else {
                // this._commanService.checkAccessToken(res.error);
            }
            //   this.isPageLoading = false;
            this.spinner.hide();
            if (this.tab) {
                setTimeout(() => {
                    let tabid = "#" + this.tab + 'TAB'
                    jQuery(tabid)[0].click();
                }, 1);
            }
        }, err => {
            //   this.isPageLoading = false;
            this.spinner.hide();
            // this._commanService.checkAccessToken(err);
        });

    }

    ngOnInit(): void {
        this.tab = this._activateRouter.snapshot.queryParams['tab'];
    }
    save() {

        let data = JSON.parse(JSON.stringify(this.setting));

        let percentage = data.commission.admin + data.commission.store;
        if (percentage <= 100) {
            //   this.isLoading = true;
            this.spinner.show();
            this._settingsService.update(data).subscribe(res => {
                //   this.isLoading         = false;
                this.spinner.hide();
                if (res.success) {
                    this.toastr.success(res.message);
                    // this._commanService.showAlert(res.message,'alert-success');
                    this._router.navigate(['/setting']);
                } else {
                    this.toastr.error(res.error.message);
                    // this._commanService.showAlert(res.error.message,'alert-danger');
                }
            }, err => {
                //   this.isLoading = false;
                this.spinner.hide();
            })
        } else {
            if (percentage > 100)
                this.toastr.error('Commission %age total should be less than or equal to 100');
            //   this._commanService.showAlert('Commission %age total should be less than or equal to 100', 'alert-danger');
        }

    }

    addFareCharges() {
        this.setting.fare_charges.push({
            min: null,
            max: null,
            fare: null
        })
    }
    removeFareCharges(index) {
        this.setting.fare_charges.splice(index, 1);
    }
}
