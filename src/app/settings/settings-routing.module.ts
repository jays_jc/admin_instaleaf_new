import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { AddUpdateSettingsComponent } from './addupdate/addupdate.component';
import { ViewSettingsComponent } from './view-settings/view-settings.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Settings'
    },
    children: [
     {
        path: '',
        component: ViewSettingsComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'list',
        component: ViewSettingsComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'edit',
        component: AddUpdateSettingsComponent,
        data: {
          title: 'Edit Settings'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [
    RouterModule,
    FormsModule
  ]
})
export class SettingsRoutingModule {}
