import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SettingsService } from '../services/settings.service';
import { CommanService } from 'src/app/shared/comman.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: 'view-settings.component.html',
  styleUrls: ['./view-settings.component.scss']
})
export class ViewSettingsComponent {
  public setting: any = {};
  public isLoading: boolean = true;
  public addEditDelete: boolean = false;
  constructor(
    private _router: Router,
    private spinner: NgxSpinnerService,
    private _activatedRouter: ActivatedRoute,
    private _settingsService: SettingsService,
    private _commanService: CommanService,
    private toastr: ToastrService
  ) {

    let actions = this._commanService.getActions();
    if (actions["type"] == 'SA' || actions['settings']['addEditDelete']) this.addEditDelete = true;
    this.spinner.show();
    this._commanService.getSettings().subscribe(res => {
      this.spinner.hide();
      // this.isLoading = false;
      if (res.success) {
        this.setting = res.data[0];
        this.spinner.hide();
      } else {
        // this._commanService.checkAccessToken(res.error);
      }
    }, err => {
      // this.isLoading = false;
      this.spinner.hide();
      // this._commanService.checkAccessToken(err);
    });

  }

  edit(key) {
    let route = '/setting/edit';
    this._router.navigate([route], { queryParams: { tab: key } });
  }
}