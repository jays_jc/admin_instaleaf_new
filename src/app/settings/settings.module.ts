import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsService } from './services/settings.service';
import { AddUpdateSettingsComponent } from './addupdate/addupdate.component';
import { ViewSettingsComponent } from './view-settings/view-settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  imports: [
  	 SettingsRoutingModule,
  	 CommonModule,
     NgxSpinnerModule
  ],
  providers: [
    SettingsService
  ],
  declarations: [
  	AddUpdateSettingsComponent,
  	ViewSettingsComponent
  ]
})
export class SettingsModule { }